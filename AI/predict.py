#%%
from keras.models import load_model
from keras.preprocessing import image
import tensorflow as tf
import numpy as np

# dimensions of our images
img_width, img_height = 160, 160

# load the model we saved
model = load_model('saved_model_500epoch.h5')
# model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
#               optimizer='adam',
#               metrics=['accuracy'])


#%%
# predicting images
img = image.load_img('samples/yes03.jpg', target_size=(img_width, img_height))
print(img)
x = image.img_to_array(img)
# print(x)
# x = np.expand_dims(x, axis=0)

x /= 255.

x = np.array([x])

# (1, 150, 150, 3)
# images = np.vstack([x])
print(x.shape)

classes = model.predict_classes(x, batch_size=1)
print(classes[0][0])

if classes[0][0] == 0:
    print("Oh.. A.I. guesses this is NOT sunrise NOR sunset")
else:
    print("BINGO! A.I. guesses this is sunrise or sunset")



#%%
# predicting multiple images at once
# img = image.load_img('samples/no01.jpg', target_size=(img_width, img_height))
# y = image.img_to_array(img)
# y = np.expand_dims(y, axis=0)

# pass the list of multiple images np.vstack()
# images = np.vstack([x, y])
# classes = model.predict_classes(images, batch_size=128)

# print the classes, the images belong to
# print(classes)
# print(classes[0])
# print(classes[0][0])



# %%
