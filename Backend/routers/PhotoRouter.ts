import express from "express";
import {
  upload,
  photoController,
  isLoggedInRN,
  isOptionalLoggedInRN,
} from "../main";
import { isLoggedInWeb } from "../guards";
import { ROLES } from "../variables";
import fetch from "node-fetch";
import FormData from "form-data";
import fs from "fs";
import { router as commentAndLikeRouter } from "./CommentAndLikeRouter";
import admin from "firebase-admin";
import dotenv from "dotenv";

dotenv.config();

const serviceAccount = require("../service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sunchaserhk-3fccc.firebaseio.com",
});

// Create a list containing up to 500 registration tokens.
// These registration tokens come from the client FCM SDKs.
const registrationTokens = [
  "e6UND7mpTQC-1ovUfw5s-g:APA91bE4YU41EZMibu1ZPz-q0VaZqJnhl7Mp-iP5KuhXXq0vtd2ZjlXePSq0IBcy9W-HTOdoGZ2ueBH48qcLbhmJLA3qyDURQAGgMzykseAkITPJ6CUDUV5FxUa59R9nfPdcfpudtuWt",
];

const irrelevantPhotoMessage = {
  foreground: true, // BOOLEAN: If the notification was received in foreground or not
  userInteraction: false, // BOOLEAN: If the notification was opened by the user from the notification area or not
  message: "A possible irrelevant photo has been detected by A.I.", // STRING: The notification message
  data: {
    title: "SunChaserHK",
    body: "A.I. has detected a possible irrelevant photo",
  }, // OBJECT: The push data
  tokens: registrationTokens,
  title: "SunChaserHK",
  vibration: 300,
};

export const router = express.Router();

router.use(
  "/commentAndLike",
  isLoggedInRN([ROLES.USER, ROLES.ADMIN]),
  commentAndLikeRouter
);

router.get("/", isOptionalLoggedInRN, photoController.getPhotosByUpdatedAt);
router.get("/totalComments", photoController.getPhotosTotalComments);

router.get("/user/:id", photoController.getPhotosByUserId);
router.get("/myTotalComments/:id", photoController.getMyPhotosTotalComments);

router.get(
  "/district/:district",
  isOptionalLoggedInRN,
  photoController.getPhotosByDistrict
);
router.get(
  "/districtTotalComments/:district",
  photoController.getDistrictPhotosTotalComments
);

router.get("/comments/:id", photoController.getCommentsByPhotoId);

router.get("/likes", photoController.getPhotosByLikes);
router.get("/comments", photoController.getPhotosByComments);

router.get("/liked", photoController.getLikedPhotos);

router.delete("/:id", photoController.deletePhoto);

//ts by CHiT
router.post(
  "/addPhoto",
  isLoggedInWeb,
  upload.single("image"),
  photoController.addPhotos
);

router.post(
  "/addPhotoRN",
  isLoggedInRN(["user", "admin"]),
  upload.single("image"),
  async (req, res, next) => {
    try {
      const formData = new FormData();
    const stream = fs.createReadStream(req.file.path);
    formData.append("the_photo", stream, req.file.filename);
    const fetchRes = await fetch("http://127.0.0.1:5000/", {
      method: "POST",
      body: formData,
    });
    const jsonData = await fetchRes.json();
    console.log(jsonData);
    if (jsonData.sunriseOrSunset === "true") {
      req.photo = { status: true };
    } else if (jsonData.sunriseOrSunset === "false") {
     
        admin
        .messaging()
        .sendMulticast(irrelevantPhotoMessage)
        .then((response) => {
          // Response is an object of the form { responses: [] }
          const successes = response.responses.filter(r => r.success === true)
            .length;
          const failures = response.responses.filter(r => r.success === false)
            .length;
          console.log(
            "Notifications sent:",
            `${successes} successful, ${failures} failed`
          );
        });
      req.photo = { status: false };
    
      
    }
    next();
    } catch(err) {
      console.error(err)
    }
    
  },
  photoController.addPhotoRN
);

router.get(
  "/pendingAndDisapprovedPhotos",
  photoController.getPendingAndDisapprovedPhotos
);

router.put("/statusChangedByAdmin", photoController.updatePhotoStatusRN);
