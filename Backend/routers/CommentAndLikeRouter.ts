import express from "express";
import { commentAndLikeController } from "../main";
// import { isLoggedInWeb } from "../guards";

export const router = express.Router();

router.get("/totalLikes", commentAndLikeController.getTotalLikes);
router.post("/RN/like/:id", commentAndLikeController.likePhoto);
router.delete("/RN/unlike/:id", commentAndLikeController.unlikePhotoRN);

router.post("/like/:id", commentAndLikeController.likePhoto);
router.put("/unlike/:id", commentAndLikeController.unlikePhoto);

router.post("/comment/:id", commentAndLikeController.commentPhoto);
router.post("/RN/comment/:id", commentAndLikeController.commentPhotoRN);
