import mockAsyncStorage from '@react-native-community/async-storage/jest/async-storage-mock';

import 'react-native-gesture-handler/jestSetup';

import { NativeModules } from 'react-native';

jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);

jest.mock('react-native-reanimated', () => {
    const Reanimated = require('react-native-reanimated/mock');
  
    // The mock for `call` immediately calls the callback which is incorrect
    // So we override it with a no-op
    Reanimated.default.call = () => {};
  
    return Reanimated;
  });

// Silence the warning: Animated: `useNativeDriver` is not supported because the native animated module is missing
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');


jest.mock('react-native-push-notification', () => {
  return {
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      requestPermissions: jest.fn(),
      configure: jest.fn(),
      getApplicationIconBadgeNumber: jest.fn(),
      getChannels: jest.fn(),
      checkPermissions: jest.fn(),
      createChannel: jest.fn(),
      popInitialNotification: jest.fn(),
  }
});

jest.mock('@react-native-community/google-signin', () => {
  const mockGoogleSignin = require.requireActual('@react-native-community/google-signin');

  mockGoogleSignin.GoogleSignin.hasPlayServices = () => Promise.resolve(true);
  mockGoogleSignin.GoogleSignin.configure = () => Promise.resolve();
  mockGoogleSignin.GoogleSignin.currentUserAsync = () => {
    return Promise.resolve({
      name: 'name',
      email: 'test@example.com',
      // .... other user data
    });
  }
  return mockGoogleSignin;
});

NativeModules.RNGoogleSignin = {
  BUTTON_SIZE_ICON: 0,
  BUTTON_SIZE_STANDARD: 0,
  BUTTON_SIZE_WIDE: 0,
  BUTTON_COLOR_AUTO: 0,
  BUTTON_COLOR_LIGHT: 0,
  BUTTON_COLOR_DARK: 0,
  SIGN_IN_CANCELLED: '0',
  IN_PROGRESS: '1',
  PLAY_SERVICES_NOT_AVAILABLE: '2',
  SIGN_IN_REQUIRED: '3',
  configure: jest.fn(),
  currentUserAsync: jest.fn(),
};

export { NativeModules };


jest.mock('react-native-geolocation-service', () => {
  return {
    getCurrentPosition: jest.fn(),
    watchPosition: jest.fn(),
    requestAuthorization: jest.fn(),
  }
});