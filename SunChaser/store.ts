import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk, {ThunkDispatch as OldThunkDispatch} from 'redux-thunk';
import {AuthActions} from './redux/auth/actions';
import {AuthState, authReducer} from './redux/auth/reducer';
import {MarkersActions} from './redux/markers/actions';
import {MarkersState, markersReducer} from './redux/markers/reducers';
import {SuntimesActions} from './redux/suntimes/actions';
import {suntimesReducer, SuntimesState} from './redux/suntimes/reducers';

import {WeatherConditionsActions} from './redux/weatherConditions/actions';
import {
  weatherConditionsReducer,
  WeatherConditionsState,
} from './redux/weatherConditions/reducers';
import {ProfilePhotoState} from './redux/profilePhotos/state';
import {profilePhotoReducer} from './redux/profilePhotos/reducers';
import {CreateUserActions} from './redux/user/actions';
import {CreateUserState, createUserReducer} from './redux/user/reducer';
import {galleryPhotoReducer} from './redux/galleryPhotos/reducers';
import {GalleryPhotoState} from './redux/galleryPhotos/state';
import {PhotoState} from './redux/photos/state';
import {PhotosActions} from './redux/photos/actions';
import {photoReducer} from './redux/photos/reducers';
import {PendingAndDisapprovedPhotosState} from './redux/adminPhotos/state';
import {PendingAndDisapprovedPhotosActions} from './redux/adminPhotos/actions';
import {pendingAndDisapprovedPhotosReducer} from './redux/adminPhotos/reducers';
import {PhotoStatusState} from './redux/adminPhotoStatus/state';
import {PhotoStatusActions} from './redux/adminPhotoStatus/actions';
import {photoStatusReducer} from './redux/adminPhotoStatus/reducers';
import { districtPhotoReducer } from './redux/districtPhotos/reducers';
import { DistrictPhotoState } from './redux/districtPhotos/state';

export interface RootState {
  auth: AuthState;
  markers: MarkersState;
  suntimes: SuntimesState;
  weatherConditions: WeatherConditionsState;
  galleryPhotos: GalleryPhotoState;
  profilePhotos: ProfilePhotoState;
  districtPhotos: DistrictPhotoState;
  createUser: CreateUserState;
  photos: PhotoState;
  pendingAndDisapprovedPhotos: PendingAndDisapprovedPhotosState;
  photoStatus: PhotoStatusState;
}

export type RootActions =
  | AuthActions
  | MarkersActions
  | SuntimesActions
  | WeatherConditionsActions
  | CreateUserActions
  | PhotosActions
  | PendingAndDisapprovedPhotosActions
  | PhotoStatusActions;

const rootReducer = combineReducers({
  auth: authReducer,
  markers: markersReducer,
  suntimes: suntimesReducer,
  weatherConditions: weatherConditionsReducer,
  galleryPhotos: galleryPhotoReducer,
  profilePhotos: profilePhotoReducer,
  districtPhotos: districtPhotoReducer,
  createUser: createUserReducer,
  photos: photoReducer,
  pendingAndDisapprovedPhotos: pendingAndDisapprovedPhotosReducer,
  photoStatus: photoStatusReducer,
});

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>;

const composeEnhancers = compose;

export const store = createStore<RootState, RootActions, {}, {}>(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk)),
);
