import React, {useEffect} from 'react';
import {Image, Text, View, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Provider, useSelector, useDispatch} from 'react-redux';
import {store, RootState} from './store';
import LoadingScreen from './components/LoadingScreen';
import WelcomeScreen from './components/WelcomeScreen';
import LoginScreen from './components/LoginScreen';
import GalleryScreen from './components/gallery/GalleryScreen';
import MapScreen from './components/MapScreen';
import DistrictsMenuScreen from './components/DistrictsMenuScreen';
import PhotoScreen from './components/PhotoScreen';
import HongKongIslandScreen from './components/HongKongIslandScreen';
import KowloonScreen from './components/KowloonScreen';
import NewTerritoriesScreen from './components/NewTerritoriesScreen';
import DistrictsScreen from './components/districts/DistrictsScreen';
import CreateAccountScreen from './components/CreateAccountScreen';
import ProfileScreen from './components/profile/ProfileScreen';
import ProfileLongScreen from './components/profile/ProfileLongScreen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MapAdminScreen from './components/admin/MapAdminScreen';
import PossibleIrrelevantPhotosScreen from './components/admin/PossibleIrrelevantPhotosScreen';
import PhotoScreenForNonUser from './components/PhotoScreenForNonUser';
import {logout} from './redux/auth/actions';
import ProfileScreenForNonUser from './components/ProfileScreenForNonUser';
import NotifService from './NotifService';
import NotificationHandler from './NotificationHandler';
import handler from './NotificationHandler';
import DistrictLongScreen from './components/districts/DistrictsLongScreen';

const Tab = createBottomTabNavigator();

export function Tabs() {
  const user = useSelector((state: RootState) => state.auth.user);
  const role = useSelector((state: RootState) => state.auth.user?.role);

  const notif = new NotifService();

  useEffect(() => {
    notif.checkPermission((perms: any) => {
      // return console.log('Permissions', JSON.stringify(perms));
    });

    notif.requestPermissions();

    notif.createOrUpdateChannel();

    NotificationHandler.attachRegister(handler);

    NotificationHandler.attachNotification(handler);

    notif.popInitialNotification();
  });

  return (
    <>
      {/* <PushController /> */}
      <Tab.Navigator>
        <Tab.Screen
          name="Map"
          //@ts-ignore
          component={
            (user === null && MapStackScreen) ||
            (role === 'user' && MapStackScreen) ||
            (role === 'admin' && MapAdminStackScreen)
          }
          options={{
            tabBarLabel: ({focused}: any) =>
              focused ? (
                <Text
                  style={{fontSize: 11, fontWeight: 'bold', color: '#E67E22'}}>
                  Map
                </Text>
              ) : (
                <Text style={{fontSize: 11}}>Map</Text>
              ),
            tabBarIcon: ({focused}: any) =>
              focused ? (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/map-icon-focused.png')}
                />
              ) : (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/map-icon.png')}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Districts"
          component={DistrictsStackScreen}
          options={{
            tabBarLabel: ({focused}: any) =>
              focused ? (
                <Text
                  style={{fontSize: 11, fontWeight: 'bold', color: '#E67E22'}}>
                  Districts
                </Text>
              ) : (
                <Text style={{fontSize: 11}}>Districts</Text>
              ),
            tabBarIcon: ({focused}: any) =>
              focused ? (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/districts-icon-focused.png')}
                />
              ) : (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/districts-icon.png')}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Photo"
          //@ts-ignore
          component={
            (user === null && PhotoStackScreenForNonUser) ||
            (role === 'user' && PhotoStackScreen) ||
            (role === 'admin' && PhotoStackScreen)
          }
          options={{
            tabBarVisible: false,
            tabBarLabel: () => <View />,
            tabBarIcon: ({focused}: any) =>
              focused ? (
                <Image
                  style={{width: 40, height: 40}}
                  source={require('./icons/add-image-icon-focused.png')}
                />
              ) : (
                <Image
                  style={{width: 40, height: 40}}
                  source={require('./icons/add-image-icon.png')}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Gallery"
          component={GalleryStackScreen}
          options={{
            tabBarLabel: ({focused}: any) =>
              focused ? (
                <Text
                  style={{fontSize: 11, fontWeight: 'bold', color: '#E67E22'}}>
                  Gallery
                </Text>
              ) : (
                <Text style={{fontSize: 11}}>Gallery</Text>
              ),
            tabBarIcon: ({focused}: any) =>
              focused ? (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/gallery-icon-focused.png')}
                />
              ) : (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./icons/gallery-icon.png')}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Profile"
          //@ts-ignore
          component={
            (user === null && ProfileStackScreenForNonUser) ||
            (role === 'user' && ProfileStackScreen) ||
            (role === 'admin' && ProfileStackScreen)
          }
          //@ts-ignore
          options={
            (user === null && {
              tabBarVisible: false,
              tabBarLabel: ({focused}: any) =>
                focused ? (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: 'bold',
                      color: '#E67E22',
                    }}>
                    Profile
                  </Text>
                ) : (
                  <Text style={{fontSize: 11}}>Profile</Text>
                ),
              tabBarIcon: ({focused}: any) =>
                focused ? (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon-focused.png')}
                  />
                ) : (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon.png')}
                  />
                ),
            }) ||
            (role === 'user' && {
              tabBarVisible: true,
              tabBarLabel: ({focused}: any) =>
                focused ? (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: 'bold',
                      color: '#E67E22',
                    }}>
                    Profile
                  </Text>
                ) : (
                  <Text style={{fontSize: 11}}>Profile</Text>
                ),
              tabBarIcon: ({focused}: any) =>
                focused ? (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon-focused.png')}
                  />
                ) : (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon.png')}
                  />
                ),
            }) ||
            (role === 'admin' && {
              tabBarVisible: true,
              tabBarLabel: ({focused}: any) =>
                focused ? (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: 'bold',
                      color: '#E67E22',
                    }}>
                    Profile
                  </Text>
                ) : (
                  <Text style={{fontSize: 11}}>Profile</Text>
                ),
              tabBarIcon: ({focused}: any) =>
                focused ? (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon-focused.png')}
                  />
                ) : (
                  <Image
                    style={{width: 26, height: 26}}
                    source={require('./icons/profile-icon.png')}
                  />
                ),
            })
          }
        />
      </Tab.Navigator>
    </>
  );
}

const ProfileStack = createStackNavigator();
function ProfileStackScreen(props: any) {
  const dispatch = useDispatch();
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() => {
                dispatch(logout());
                props.navigation.replace('Welcome');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 10}}
                source={require('./icons/sign-out-alt-solid.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <ProfileStack.Screen name="My Photos" component={ProfileLongScreen} />
    </ProfileStack.Navigator>
  );
}

const ProfileForNonUserStack = createStackNavigator();
function ProfileStackScreenForNonUser(props: any) {
  const dispatch = useDispatch();
  return (
    <ProfileForNonUserStack.Navigator>
      <ProfileForNonUserStack.Screen
        name="ProfileScreenForNonUser"
        component={ProfileScreenForNonUser}
        options={{
          headerTitle: 'Profile',
          headerTintColor: 'white',
          headerTransparent: true,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() => {
                dispatch(logout());
                props.navigation.replace('Welcome');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 10}}
                source={require('./icons/sign-out-alt-solid.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <ProfileForNonUserStack.Screen
        name="Sign in"
        component={LoginScreen}
        options={{
          headerShown: true,
          headerTitleStyle: {color: 'white'},
          headerTintColor: 'white',
          headerTransparent: true,
          headerBackTitleVisible: false,
        }}
      />
      <ProfileForNonUserStack.Screen
        name="Create Account"
        component={CreateAccountScreen}
        options={{
          headerShown: true,
          headerTitleStyle: {color: 'white'},
          headerTintColor: 'white',
          headerTransparent: true,
          headerBackTitleVisible: false,
        }}
      />
    </ProfileForNonUserStack.Navigator>
  );
}

const GalleryStack = createStackNavigator();
function GalleryStackScreen(props: any) {
  return (
    <GalleryStack.Navigator>
      <GalleryStack.Screen
        name="Gallery"
        component={GalleryScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
    </GalleryStack.Navigator>
  );
}

const DistrictsStack = createStackNavigator();
function DistrictsStackScreen(props: any) {
  return (
    <DistrictsStack.Navigator>
      <DistrictsStack.Screen
        name="Districts"
        component={DistrictsMenuScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <DistrictsStack.Screen
        name="Hong Kong Island 香港島"
        component={HongKongIslandScreen}
      />
      <DistrictsStack.Screen name="Kowloon 九龍" component={KowloonScreen} />
      <DistrictsStack.Screen
        name="New Territories 新界及離島"
        component={NewTerritoriesScreen}
      />
      <DistrictsStack.Screen
        name="Central and Western 中西區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen name="Eastern 東區" component={DistrictsScreen} />
      <DistrictsStack.Screen
        name="Posts"
        component={DistrictLongScreen}
        options={{headerTitle: ''}}
      />
      <DistrictsStack.Screen
        name="Wan Chai 灣仔區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen name="Southern 南區" component={DistrictsScreen} />
      <DistrictsStack.Screen
        name="Yau Tsim Mong 油尖旺區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Sham Shui Po 深水埗區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Kowloon City 九龍城區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Wong Tai Sin 黃大仙區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Kwun Tong 觀塘區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Kwai Tsing 葵青區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Tsuen Wan 荃灣區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Tuen Mun 屯門區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Yuen Long 元朗區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen name="North 北區" component={DistrictsScreen} />
      <DistrictsStack.Screen name="Tai Po 大埔區" component={DistrictsScreen} />
      <DistrictsStack.Screen
        name="Sha Tin 沙田區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Sai Kung 西貢區"
        component={DistrictsScreen}
      />
      <DistrictsStack.Screen
        name="Islands 離島區"
        component={DistrictsScreen}
      />
    </DistrictsStack.Navigator>
  );
}

const MapStack = createStackNavigator();
function MapStackScreen(props: any) {
  return (
    <MapStack.Navigator>
      <MapStack.Screen
        name="Map"
        component={MapScreen}
        options={{
          headerTitle: 'SUN-CHASER HONG KONG',
          headerTitleStyle: {fontSize: 16},
          headerTintColor: 'white',
          headerTransparent: true,
          headerLeft: () => (
            // <TouchableOpacity
            //   onPress={() => {
            //     props.navigation.navigate('Admin Screen');
            //   }}>
            <Image
              style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
              source={require('./icons/logo_darkorange.png')}
            />
            // </TouchableOpacity>
          ),
        }}
      />
      {/* <MapStack.Screen
        name="Admin Screen"
        component={MapAdminScreen}
        options={{
          headerTintColor: 'white',
          headerTransparent: true,
        }
        }
      />
      <MapStack.Screen
        name="Possible Irrelevant Photos"
        component={PossibleIrrelevantPhotosScreen}
      /> */}
    </MapStack.Navigator>
  );
}

const MapAdminStack = createStackNavigator();
function MapAdminStackScreen(props: any) {
  return (
    <MapAdminStack.Navigator>
      <MapAdminStack.Screen
        name="Admin Screen"
        component={MapAdminScreen}
        options={{
          headerTitleStyle: {fontSize: 18},
          headerTintColor: 'white',
          headerTransparent: true,
          headerLeft: () => (
            <Image
              style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
              source={require('./icons/logo_darkorange.png')}
            />
          ),
        }}
      />
      <MapAdminStack.Screen
        name="Possible Irrelevant Photos"
        component={PossibleIrrelevantPhotosScreen}
        options={{
          headerTitleStyle: {fontSize: 18},
          headerTintColor: 'white',
          headerTransparent: true,
        }}
      />
    </MapAdminStack.Navigator>
  );
}

const PhotoStack = createStackNavigator();
function PhotoStackScreen(props: any) {
  return (
    <PhotoStack.Navigator>
      <PhotoStack.Screen
        name="Add Photo"
        component={PhotoScreen}
        options={{
          headerTintColor: 'white',
          headerTransparent: true,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
    </PhotoStack.Navigator>
  );
}

const PhotoForNonUserStack = createStackNavigator();
function PhotoStackScreenForNonUser(props: any) {
  return (
    <PhotoForNonUserStack.Navigator>
      <PhotoForNonUserStack.Screen
        name="PhotoScreenForNonUser"
        component={PhotoScreenForNonUser}
        options={{
          headerTitle: 'Add Photo',
          headerTintColor: 'white',
          headerTransparent: true,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Map');
              }}>
              <Image
                style={{width: 30, height: 30, marginLeft: 10, marginRight: 0}}
                source={require('./icons/logo_darkorange.png')}
              />
            </TouchableOpacity>
          ),
        }}
      />
      <PhotoForNonUserStack.Screen
        name="Sign in"
        component={LoginScreen}
        options={{
          headerShown: true,
          headerTitleStyle: {color: 'white'},
          headerTintColor: 'white',
          headerTransparent: true,
          headerBackTitleVisible: false,
        }}
      />
      <PhotoForNonUserStack.Screen
        name="Create Account"
        component={CreateAccountScreen}
        options={{
          headerShown: true,
          headerTitleStyle: {color: 'white'},
          headerTintColor: 'white',
          headerTransparent: true,
          headerBackTitleVisible: false,
        }}
      />
    </PhotoForNonUserStack.Navigator>
  );
}

const LoginStack = createStackNavigator();
function LoginStackScreen() {
  return (
    <>
      {/* <PushController /> */}
      <LoginStack.Navigator>
        <LoginStack.Screen
          name="Loading"
          component={LoadingScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        <LoginStack.Screen
          name="Tabs"
          component={Tabs}
          options={{headerShown: false}}
        />
        <LoginStack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{headerShown: false, gestureEnabled: false}}
        />
        <LoginStack.Screen
          name="Create Account"
          component={CreateAccountScreen}
          options={{
            headerShown: true,
            headerTitleStyle: {color: 'white'},
            headerTintColor: 'white',
            headerTransparent: true,
            headerBackTitleVisible: false,
          }}
        />
        <LoginStack.Screen
          name="Sign in"
          component={LoginScreen}
          options={{
            headerShown: true,
            headerTitleStyle: {color: 'white'},
            headerTintColor: 'white',
            headerTransparent: true,
            headerBackTitleVisible: false,
          }}
        />
      </LoginStack.Navigator>
    </>
  );
}

const Stack = createStackNavigator();
const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle={'dark-content'} />
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Root"
            component={LoginStackScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
