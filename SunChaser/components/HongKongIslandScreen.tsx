import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default function HongKongIslandScreen(props: any) {
  return (
    <View style={styles.body}>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Central and Western 中西區', {
            place: 'Central And Western',
          });
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/centralandwestern.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>
              Central and Western 中西區{' '}
            </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Eastern 東區', {place: 'Eastern'});
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/eastern.jpg')}>
          <View style={styles.textContainerWhite}>
            <Text style={styles.sectionTitleBlack}>Eastern 東區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-black.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Wan Chai 灣仔區', {place: 'Wan Chai'});
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/wanchai.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>Wan Chai 灣仔區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Southern 南區', {place: 'Southern'});
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/southern.jpg')}>
          <View style={styles.textContainerWhite}>
            <Text style={styles.sectionTitleBlack}>Southern 南區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-black.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  sectionContainer: {
    marginTop: 0,
    padding: 0,
    marginBottom: 0,
    flex: 1,
  },
  imageBackground: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  textContainerBlack: {
    backgroundColor: '#16161680',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleWhite: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  arrowIcon: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 5,
  },
  textContainerWhite: {
    backgroundColor: '#ffffff80',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
});
