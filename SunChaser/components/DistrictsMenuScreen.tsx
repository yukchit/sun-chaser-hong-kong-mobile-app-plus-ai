import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {useNavigation} from '@react-navigation/native';

export default function DistrictsMenuScreen() {
  const navigation = useNavigation();
  return (
    <View style={styles.body}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Hong Kong Island 香港島');
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/hongkongisland.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>
              Hong Kong Island 香港島{' '}
            </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Kowloon 九龍');
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/kowloon.jpg')}>
          <View style={styles.textContainerWhite}>
            <Text style={styles.sectionTitleBlack}>Kowloon 九龍 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-black.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('New Territories 新界及離島');
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/newterritories.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>
              New Territories 新界及離島{' '}
            </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  sectionContainer: {
    marginTop: 0,
    padding: 0,
    marginBottom: 0,
    flex: 1,
  },
  sectionTitleWhite: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  imageBackground: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  textContainerBlack: {
    backgroundColor: '#16161680',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  arrowIcon: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 5,
  },
  textContainerWhite: {
    backgroundColor: '#ffffff80',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
});
