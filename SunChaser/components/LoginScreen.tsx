import React, {useState, useEffect} from 'react';
import {
  Text,
  TextInput,
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {login, loginGoogle} from '../redux/auth/actions';
import {RootState} from '../store';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {TouchableHighlight, ScrollView} from 'react-native-gesture-handler';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

export default function LoginScreen() {
  const dispatch = useDispatch();
  const navigation = useNavigation<StackNavigationProp<any>>();
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  useEffect(() => {
    if (isAuthenticated) {
      navigation.replace('Tabs');
    }
  }, [dispatch, isAuthenticated]);

  type FormData = {
    email: string;
    password: string;
  };

  const {control, handleSubmit, errors, setError} = useForm<FormData>();

  const onSubmit = ({email, password}: FormData) => {
    dispatch(login(email, password));
    console.log(email);
    console.log(password);

    setError('email', {
      type: 'manual',
      message: 'Login Failed. Please try again',
    });

    // dispatch(login(data.email, data.password));
  };

  GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/userinfo.email'],
    // scopes: ['https://www.googleapis.com/auth/drive.readonly'],
    // iosClientId:
    // '162928042758-qtq5pup3iavqu13gdbn4q6s6172bj6dr.apps.googleusercontent.com',
    webClientId:
      // '162928042758-qtq5pup3iavqu13gdbn4q6s6172bj6dr.apps.googleusercontent.com',
      '711585022928-v89ujd3vsqinh0a326n9tp6or2hssbdp.apps.googleusercontent.com',
  });

  const googleLogin = async () => {
    try {
      console.log('TRY');
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if (userInfo.idToken) {
        dispatch(loginGoogle(userInfo.idToken));
      }
      console.log('user', userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log("user cancelled the login flow", error)
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log("operation (e.g. sign in) is in progress already", error)
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log("play services not available or outdated", error)
      } else {
        // some other error happened
        console.log(error)
      }
    }
  };

  return (
    <>
      <View style={styles.body}>
        <ImageBackground
          style={styles.background}
          source={require('../images/backgrounds/sunrise.jpg')}>
          <SafeAreaView style={styles.container}>
            <ScrollView>
              <View style={styles.logoContainer}>
                <Image
                  style={styles.logo}
                  source={require('../icons/logo_lightorange.png')}
                />
              </View>
              <KeyboardAvoidingView style={styles.formContainer}>
                <Text style={styles.label}>E-mail Address:</Text>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Enter e-mail'}
                      textContentType={'emailAddress'}
                      keyboardType={'email-address'}
                      keyboardAppearance={'dark'}
                      autoCompleteType={'email'}
                    />
                  )}
                  name="email"
                  rules={{
                    required: true,
                    pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  }}
                  defaultValue=""
                />
                {errors.email?.type === 'required' && (
                  <Text style={styles.errorMsg}>Your e-mail is required.</Text>
                )}
                {errors.email?.type === 'pattern' && (
                  <Text style={styles.errorMsg}>
                    Please enter a valid Email Address.
                  </Text>
                )}

                <Text style={styles.label}>Password:</Text>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Enter password'}
                      secureTextEntry={true}
                      textContentType={'password'}
                      keyboardAppearance={'dark'}
                    />
                  )}
                  name="password"
                  rules={{required: true}}
                  defaultValue=""
                />
                {errors.password?.type === 'required' && (
                  <Text style={styles.errorMsg}>
                    Your password is required.
                  </Text>
                )}

                <View style={styles.loginButtonContainer}>
                  <TouchableHighlight
                    style={styles.loginButton}
                    underlayColor="#0fa9bdc5"
                    onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.loginButtonText}>LOGIN</Text>
                  </TouchableHighlight>
                </View>
                {errors.password?.type === 'required' && (
                  <Text style={styles.errorMsg}>
                    Your password is required.
                  </Text>
                )}

                <View style={styles.horizontalLineContainer}>
                  <Image
                    style={styles.horizontalLine}
                    source={require('../icons/horizontal-line-96-white.png')}
                  />
                  <Text style={styles.or}>Or</Text>
                  <Image
                    style={styles.horizontalLine}
                    source={require('../icons/horizontal-line-96-white.png')}
                  />
                </View>

                <View style={styles.loginGoogleButtonContainer}>
                  <GoogleSigninButton
                    style={{width: '100%', height: 48, justifyContent: 'center', alignItems:'center'}}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Light}
                    onPress={googleLogin}
                  />
                </View>
              </KeyboardAvoidingView>
            </ScrollView>
          </SafeAreaView>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
  },
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    padding: 8,
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  logo: {
    width: 60,
    height: 60,
    marginBottom: 5,
    marginTop: 40,
  },
  formContainer: {
    flex: 4,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  label: {
    marginTop: 5,
    marginBottom: 0,
    marginLeft: 20,
    color: 'white',
    fontSize: 13,
    fontWeight: 'bold',
    width: '95%',
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    padding: 10,
    borderRadius: 4,
    marginTop: 2,
    marginLeft: 20,
    marginRight: 20,
    width: '90%',
  },
  errorMsg: {
    fontSize: 10,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    fontWeight: 'bold',
    backgroundColor: '#ff00007a',
    borderRadius: 2,
    padding: 2,
    width: '90%',
  },
  loginButtonContainer: {
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    width: '90%',
  },
  loginButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1bb9ce',
    width: '100%',
    height: 38,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  loginButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    display: 'flex',
  },
  horizontalLineContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  horizontalLine: {
    width: '49%',
    height: 1,
  },
  or: {
    fontSize: 15,
    fontWeight: '600',
    color: 'white',
  },
  loginGoogleButtonContainer: {
    padding: 0,
    marginTop: 5,
    width: '92%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginGoogleButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
    height: 38,
    borderRadius: 4,
    paddingLeft: 5,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  googleIcon: {
    height: 18,
    width: 18,
    marginLeft: 10,
  },
  loginGoogleButtonText: {
    color: '#666666',
    fontWeight: 'bold',
    fontSize: 13,
    height: '80%',
    display: 'flex',
  },
});
