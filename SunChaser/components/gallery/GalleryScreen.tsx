import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import {loadPhotosThunk} from '../../redux/photos/thunks';
import {RootState} from '../../store';
import {useSelector, useDispatch} from 'react-redux';
import GalleryPhotoDetails from './GalleryPhotoDetails';
import {perPage} from '../../redux/galleryPhotos/state';
import IPhoto from '../../redux/photos/state';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function GalleryScreen(props: {photos: IPhoto[]}) {
  const galleryPhotoIds = useSelector(
    (state: RootState) => state.galleryPhotos.photoIds,
  );
  const galleryPhotos: IPhoto[] = useSelector((state: RootState) =>
    galleryPhotoIds.map((id) => state.galleryPhotos.photoById[id]),
  );
  const [page, setPage] = useState(1);
  const total = useSelector(
    (state: RootState) => state.galleryPhotos.galleryPhotoCount,
  );
  const token = useSelector((state: RootState) => state.auth.token);
  const addPhotoCount = useSelector(
    (state: RootState) => state.photos.addPhotoCount,
  );

  const [refreshing, setRefreshing] = React.useState(false);

  const likeCount = useSelector(
    (state: RootState) => state.galleryPhotos.likeCount,
  );
  const commentCount = useSelector(
    (state: RootState) => state.galleryPhotos.commentCount,
  );

  const wait = (timeout: any) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(loadPhotosThunk(page));
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadPhotosThunk(page));
  }, [dispatch, page, likeCount, commentCount, addPhotoCount]);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <SafeAreaView
        style={{
          width: '100%',
        }}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {galleryPhotos
            .slice((page - 1) * perPage, page * perPage)
            .map((photo) => (
              <GalleryPhotoDetails photo={photo} key={photo.id} />
            ))}

          <View style={styles.pagination}>
            {(page > 1 && (
              <TouchableOpacity onPress={() => setPage(page - 1)}>
                <Image
                  style={styles.paginationButtonPrevPage}
                  source={require('../../icons/arrow-left-solid.png')}
                />
              </TouchableOpacity>
            )) ||
              (page === 1 && <Text> </Text>)}

            <Text style={styles.page}>Page: {page}</Text>
            {(page < Math.ceil(total / perPage) && (
              <TouchableOpacity onPress={() => setPage(page + 1)}>
                <Image
                  style={styles.paginationButtonNextPage}
                  source={require('../../icons/arrow-right-solid.png')}
                />
              </TouchableOpacity>
            )) ||
              (page === Math.ceil(total / perPage) && <Text> </Text>)}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  pagination: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  paginationButtonPrevPage: {
    width: 27,
    height: 27,
  },
  paginationButtonNextPage: {
    width: 27,
    height: 27,
  },
  page: {
    // paddingBottom: 50
  },
});
