import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default function NewTerritoriesScreen(props: any) {
  return (
    <View style={styles.body}>
      <ScrollView style={styles.scrollView}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Kwai Tsing 葵青區', {
              place: 'Kwai Tsing',
            });
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/kwaitsing.jpg')}>
            <View style={styles.textContainerBlack}>
              <Text style={styles.sectionTitleWhite}>Kwai Tsing 葵青區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-white.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Tsuen Wan 荃灣區', {place: 'Tsuen Wan'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/tsuenwan.jpg')}>
            <View style={styles.textContainerWhite}>
              <Text style={styles.sectionTitleBlack}>Tsuen Wan 荃灣區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-black.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Tuen Mun 屯門區', {place: 'Tuen Mun'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/tuenmun.jpg')}>
            <View style={styles.textContainerBlack}>
              <Text style={styles.sectionTitleWhite}>Tuen Mun 屯門區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-white.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Yuen Long 元朗區', {place: 'Yuen Long'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/yuenlong.jpg')}>
            <View style={styles.textContainerWhite}>
              <Text style={styles.sectionTitleBlack}>Yuen Long 元朗區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-black.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('North 北區', {place: 'North'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/north.jpg')}>
            <View style={styles.textContainerBlack}>
              <Text style={styles.sectionTitleWhite}>North 北區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-white.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Tai Po 大埔區', {place: 'Tai Po'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/taipo.jpg')}>
            <View style={styles.textContainerWhite}>
              <Text style={styles.sectionTitleBlack}>Tai Po 大埔區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-black.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Sha Tin 沙田區', {place: 'Sha Tin'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/shatin.jpg')}>
            <View style={styles.textContainerBlack}>
              <Text style={styles.sectionTitleWhite}>Sha Tin 沙田區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-white.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Sai Kung 西貢區', {place: 'Sai Kung'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/saikung.jpg')}>
            <View style={styles.textContainerWhite}>
              <Text style={styles.sectionTitleBlack}>Sai Kung 西貢區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-black.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Islands 離島區', {place: 'Islands'});
          }}
          style={styles.sectionContainer}>
          <ImageBackground
            style={styles.imageBackground}
            source={require('../images/islands.jpg')}>
            <View style={styles.textContainerBlack}>
              <Text style={styles.sectionTitleWhite}>Islands 離島區 </Text>
              <Image
                style={styles.arrowIcon}
                source={require('../icons/double-right-30-white.png')}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 0,
    padding: 0,
    marginBottom: 0,
    height: 100,
  },
  imageBackground: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  textContainerBlack: {
    backgroundColor: '#16161680',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleWhite: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  arrowIcon: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 5,
  },
  textContainerWhite: {
    backgroundColor: '#ffffff80',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
});
