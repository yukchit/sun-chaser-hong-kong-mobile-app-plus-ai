import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  SafeAreaView,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from 'store';
import {fetchSuntimes} from '../redux/suntimes/actions';
import {fetchWeatherConditions} from '../redux/weatherConditions/actions';
import {TouchableHighlight} from 'react-native-gesture-handler';
import NotifService from '../NotifService';


export default function WelcomeScreen() {
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();

  const suntimes = useSelector((state: RootState) => state.suntimes);
  const weatherConditions = useSelector(
    (state: RootState) => state.weatherConditions,
  );
  useEffect(() => {
    dispatch(fetchSuntimes());
    dispatch(fetchWeatherConditions());
  }, [dispatch]);

  const notif = new NotifService();


  return (
    <>
      <View style={styles.body}>
        <ImageBackground
          style={styles.background}
          source={require('../images/backgrounds/sunrise.jpg')}>
          <SafeAreaView style={styles.container}>
            <View style={styles.loginTextContainer}>
              <Image
                style={styles.logo}
                source={require('../icons/logo_lightorange.png')}
              />
              <Text style={styles.loginText}>WELCOME TO</Text>
              <Text style={styles.loginText}>SUN-CHASER HONG KONG</Text>
              <View style={styles.textRow}>
                  <Text style={styles.dayTitle}>Today: </Text>
                  <Text style={styles.dayText}>{suntimes.todayResult.date}</Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.suntimeTitle}>Sunrise Time: </Text>
                  <Text style={styles.suntimeText}>{suntimes.todayResult.sunrise}  </Text>
                  <Text style={styles.suntimeTitle}>Sunset Time: </Text>
                  <Text style={styles.suntimeText}>{suntimes.todayResult.sunset}</Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.dayTitle}>Tomorrow: </Text>
                  <Text style={styles.dayText}>{suntimes.tomorrowResult.date}</Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.suntimeTitle}>Sunrise Time: </Text>
                   <Text style={styles.suntimeText}>{suntimes.tomorrowResult.sunrise}  </Text>
                  <Text style={styles.suntimeTitle}>Sunset Time: </Text>
                  <Text style={styles.suntimeText}>{suntimes.tomorrowResult.sunset}</Text>
                </View>

              <Text style={styles.weatherInfoTextTop}>
                {weatherConditions.generalSituation}
              </Text>
              <Text style={styles.weatherPeriodText}>
                {weatherConditions.forecastPeriod}：
              </Text>
              <Text style={styles.weatherInfoText}>
                {weatherConditions.forecastDesc}
              </Text>
            </View>
            
            <View style={styles.loginButtonContainer}>
              <View style={styles.loginButtonSubContainer}>
                <TouchableHighlight
                  style={styles.loginButton}
                  underlayColor="#0fa9bdc5"
                  onPress={() => navigation.navigate('Sign in')}>
                  <Text style={styles.buttonText}>SIGN IN</Text>
                </TouchableHighlight>
              </View>
              <View style={styles.skipButtonContainer}>
                <View style={styles.skipButtonSubContainer}>
                  <TouchableHighlight
                    style={styles.skipButton}
                    underlayColor="#303030c5"
                    onPress={() => navigation.replace('Tabs')}>
                    <Text style={styles.buttonText}>SKIP</Text>
                  </TouchableHighlight>
                </View>
              </View>
              <View style={styles.createAccountButtonContainer}>
                <View style={styles.createAccountButtonSubContainer}>
                  <TouchableHighlight
                    style={styles.createAccountButton}
                    underlayColor="#44444462"
                    onPress={() => navigation.navigate('Create Account')}>
                    <Text style={styles.buttonText}>CREATE ACCOUNT</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    width: '100%',
  },
  loginTextContainer: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    marginBottom: 5,
  },
  loginText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 18,
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  dayTitle:{
    color: 'white',
    fontWeight: 'bold',
    fontSize: 13,
    marginTop: 8,
  },
  dayText: {
    color: 'white',
    fontWeight: '900',
    fontSize: 15,
    marginTop: 8,
  },
  suntimeTitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 13,
  },
  suntimeText: {
    color: 'white',
    fontWeight: '900',
    fontSize: 13,
  },
  weatherInfoTextTop: {
    width: '75%',
    color: 'white',
    fontWeight: 'bold',
    marginTop: 18,
    fontSize: 11,
  },
  weatherPeriodText: {
    width: '75%',
    color: 'white',
    fontWeight: 'bold',
    marginTop: 10,
    fontSize: 11,
  },
  weatherInfoText: {
    width: '75%',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 11,
  },
  loginButtonContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  loginButtonSubContainer: {
    width: '80%',
  },
  loginButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0fa8bd',
    height: 38,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 14,
  },
  skipButtonContainer: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  skipButtonSubContainer: {
    width: '80%',
  },
  skipButton: {
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 4,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    height: 38,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  createAccountButtonContainer: {
    marginTop: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  createAccountButtonSubContainer: {
    width: '80%',
  },
  createAccountButton: {
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 4,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#16161662',
    height: 38,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  modal: {
    alignItems: 'center',
  },
  loginTextBox: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    padding: 5,
    marginTop: 10,
    width: '90%',
  },
  loginButtonBox: {
    backgroundColor: '#ffffff',
    borderRadius: 50,
    padding: 10,
    marginTop: 20,
    width: '70%',
  },
});
