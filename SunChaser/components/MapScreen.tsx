import React, {useRef, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Text,
  Dimensions,
  Alert,
  Platform,
  TouchableHighlight,
  Modal,
} from 'react-native';
import MapView from 'react-native-map-clustering';
import {PROVIDER_GOOGLE, Marker, Geojson} from 'react-native-maps';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../store';
import {fetchMarkers} from '../redux/markers/actions';
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid} from 'react-native';
import {fetchSuntimes} from '../redux/suntimes/actions';
import {fetchWeatherConditions} from '../redux/weatherConditions/actions';

interface ILocation {
  latitude: number;
  longitude: number;
}

export default function MapScreen(props: any) {
  const bottomSheet = useRef<BottomSheet>();
  const fall = new Animated.Value(1);
  const markerIds = useSelector((state: RootState) => state.markers.markerIds);
  const markers = useSelector((state: RootState) =>
    markerIds.map((id) => state.markers.markerById[id]),
  );

  const dispatch = useDispatch();
  const [clickedMarker, setClickedMarker] = useState({} as any);

  useEffect(() => {
    dispatch(fetchMarkers());
  }, [dispatch]);

  const [modalVisible, setModalVisible] = useState(false);

  const suntimes = useSelector((state: RootState) => state.suntimes);
  const weatherConditions = useSelector(
    (state: RootState) => state.weatherConditions,
  );
  useEffect(() => {
    dispatch(fetchSuntimes());
    dispatch(fetchWeatherConditions());
  }, [dispatch]);

  const renderHeader = () => {
    return (
      <View style={styles.BSHeader}>
        <TouchableOpacity style={styles.iconContainer}>
          <Image
            style={{width: 35, height: 15}}
            source={require('../icons/horizontal-line-96-white.png')}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderContent = () => {
    return (
      <ScrollView key={clickedMarker.id}>
        <View style={styles.BSContent}>
          <Text style={styles.clickedMarkerUsername}>
            {clickedMarker.username}
          </Text>
          <View style={styles.clickedMarkerImageContainer}>
            <View style={styles.clickedMarkerImageOverlay}>
              <Image
                style={styles.clickedMarkerImage}
                source={{
                  uri: `https://sunchaserhk.pro/uploads/${clickedMarker.image}`,
                }}
              />
            </View>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Title: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {clickedMarker.title}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Moment: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {(clickedMarker.environment === 'sunrise' && 'Sunrise 日出') ||
                (clickedMarker.environment === 'sunset' && 'Sunset 日落')}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>District: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {(clickedMarker.district === 'Central And Western' &&
                'Central And Western 中西區') ||
                (clickedMarker.district === 'Eastern' && 'Eastern 東區') ||
                (clickedMarker.district === 'Wan Chai' && 'Wan Chai 灣仔區') ||
                (clickedMarker.district === 'Southern' && 'Southern 南區') ||
                (clickedMarker.district === 'Yau Tsim Mong' &&
                  'Yau Tsim Mong 油尖旺區') ||
                (clickedMarker.district === 'Sham Shui Po' &&
                  'Sham Shui Po 深水埗區') ||
                (clickedMarker.district === 'Kowloon City' &&
                  'Kowloon City 九龍城區') ||
                (clickedMarker.district === 'Wong Tai Sin' &&
                  'Wong Tai Sin 黃大仙區') ||
                (clickedMarker.district === 'Kwun Tong' &&
                  'Kwun Tong 觀塘區') ||
                (clickedMarker.district === 'Kwai Tsing' &&
                  'Kwai Tsing 葵青區') ||
                (clickedMarker.district === 'Tsuen Wan' &&
                  'Kwun Tong 荃灣區') ||
                (clickedMarker.district === 'Tuen Mun' && 'Tuen Mun 屯門區') ||
                (clickedMarker.district === 'Yuen Long' &&
                  'Yuen Long 元朗區') ||
                (clickedMarker.district === 'North' && 'North 北區') ||
                (clickedMarker.district === 'Tai Po' && 'Tai Po 大埔區') ||
                (clickedMarker.district === 'Sha Tin' && 'Sha Tin 沙田區') ||
                (clickedMarker.district === 'Sai Kung' && 'Sai Kung 西貢區') ||
                (clickedMarker.district === 'Islands' && 'Islands 離島區') ||
                clickedMarker.district}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Location: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {clickedMarker.location}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Date and Time: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {new Date(clickedMarker.created_at).getFullYear()}-
              {new Date(clickedMarker.created_at).getMonth() + 1}-
              {new Date(clickedMarker.created_at).getDate()}{' '}
              {new Date(clickedMarker.created_at).getHours()}:
              {new Date(clickedMarker.created_at).getMinutes()}:
              {new Date(clickedMarker.created_at).getSeconds()}
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  };

  const [location, setLocation] = useState<ILocation | undefined>(undefined);

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'SUN-CHASER HONG KONG required Location permission',
          message:
            'We required Location permission in order to get device location ' +
            'Please grant us.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Alert.alert("You've access for the location");
        Geolocation.getCurrentPosition(
          (position) => {
            const {latitude, longitude} = position.coords;
            setLocation({
              latitude,
              longitude,
            });
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {enableHighAccuracy: true},
        );
      } else {
        console.log('Location permission denied');
        Alert.alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization('always');
      Geolocation.getCurrentPosition(
        (position) => {
          const {latitude, longitude} = position.coords;
          setLocation({
            latitude,
            longitude,
          });
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true},
      );
    }
    if (Platform.OS === 'android') {
      const onScreenLoad = () => {
        requestLocationPermission();
      };
      onScreenLoad();
    }
  }, []);

  return (
    <View style={styles.body}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.container}>
              <View style={styles.loginTextContainer}>
                <Image
                  style={styles.logo}
                  source={require('../icons/logo_lightorange.png')}
                />
                <Text style={styles.loginText}>SUN-CHASER HONG KONG</Text>
                <View style={styles.textRow}>
                  <Text style={styles.dayTitle}>Today: </Text>
                  <Text style={styles.dayText}>
                    {suntimes.todayResult.date}
                  </Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.suntimeTitle}>Sunrise Time: </Text>
                  <Text style={styles.suntimeText}>
                    {suntimes.todayResult.sunrise}{' '}
                  </Text>
                  <Text style={styles.suntimeTitle}>Sunset Time: </Text>
                  <Text style={styles.suntimeText}>
                    {suntimes.todayResult.sunset}
                  </Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.dayTitle}>Tomorrow: </Text>
                  <Text style={styles.dayText}>
                    {suntimes.tomorrowResult.date}
                  </Text>
                </View>
                <View style={styles.textRow}>
                  <Text style={styles.suntimeTitle}>Sunrise Time: </Text>
                  <Text style={styles.suntimeText}>
                    {suntimes.tomorrowResult.sunrise}{' '}
                  </Text>
                  <Text style={styles.suntimeTitle}>Sunset Time: </Text>
                  <Text style={styles.suntimeText}>
                    {suntimes.tomorrowResult.sunset}
                  </Text>
                </View>

                <Text style={styles.weatherInfoTextTop}>
                  {weatherConditions.generalSituation}
                </Text>
                <Text style={styles.weatherPeriodText}>
                  {weatherConditions.forecastPeriod}：
                </Text>
                <Text style={styles.weatherInfoText}>
                  {weatherConditions.forecastDesc}
                </Text>
              </View>
            </View>

            <TouchableHighlight
              style={{...styles.openButton, backgroundColor: 'orange'}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.textStyle}>Close</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <BottomSheet
        ref={bottomSheet as any}
        snapPoints={['55%', 0]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        callbackNode={fall}
        initialSnap={1}
      />
      <Animated.View
        style={{
          flex: 1,
          opacity: Animated.add(0.3, Animated.multiply(fall, 0.9)),
        }}>
        {location && (
          <MapView
            style={styles.map}
            clusteringEnabled={true}
            provider={PROVIDER_GOOGLE}
            initialRegion={{
              latitude: location.latitude,
              longitude: location.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            showsUserLocation={true}
            showsMyLocationButton={true}
            followsUserLocation={true}
            showsCompass={true}
            customMapStyle={[
              {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
              {
                elementType: 'labels.text.stroke',
                stylers: [{color: '#242f3e'}],
              },
              {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
              {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#263c3f'}],
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#6b9a76'}],
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#38414e'}],
              },
              {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#212a37'}],
              },
              {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#9ca5b3'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#746855'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f3d19c'}],
              },
              {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}],
              },
              {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#17263c'}],
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}],
              },
              {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#17263c'}],
              },
              {
                featureType: 'poi.medical',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.business',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.place_of_worship',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.school',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
            ]}>
            {/* <Geojson
            geojson={require('../hksar_18_district_boundary.json')}
            strokeColor="#b57f1b65"
            strokeWidth={2}
            fillColor="#16161675"
          /> */}
            {markers.map((marker) => (
              <Marker
                onPress={() => {
                  bottomSheet.current?.snapTo(0);
                  setClickedMarker(marker);
                }}
                coordinate={{
                  latitude: marker.latitude,
                  longitude: marker.longitude,
                }}
                image={require('../icons/sunrise_logo_map.png')}
                key={marker.id}></Marker>
            ))}
          </MapView>
        )}
        <View style={styles.weatherInfoContainer}>
          <TouchableOpacity
            style={{
              height: 38,
              width: 38,
              right: 12,
              top: 95,
              backgroundColor: '#ffffffad',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 2,
            }}
            onPress={() => setModalVisible(true)}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../icons/weather-forecast.png')}
            />
          </TouchableOpacity>
        </View>
      </Animated.View>
      <Text style={{display: "none"}}>Mapview for Role User</Text>
    </View>
  );
}

const win = Dimensions.get('window');
const ratio = win.width / 541;

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  map: {
    flex: 1,
    position: 'relative',
  },
  weatherInfoContainer: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  BSHeader: {
    backgroundColor: '#000000c0',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    marginBottom: 0,
  },
  BSContent: {
    marginTop: 0,
    flex: 1,
    minHeight: 400,
    backgroundColor: '#000000c0',
    color: 'white',
    width: '100%',
    alignItems: 'center',
  },
  clickedMarkerUsername: {
    display: 'flex',
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
  },
  clickedMarkerImageContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    maxWidth: '100%',
    width: '95%',
    height: 340 * ratio,
    // paddingTop: "66.66%"
    // * 3:2 display ratio
  },
  clickedMarkerImageOverlay: {
    position: 'absolute',
    backgroundColor: 'black',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
  },
  clickedMarkerImage: {
    position: 'relative',
    // display: "flex",
    // flexDirection: "column",
    maxHeight: '100%',
    maxWidth: '100%',
    aspectRatio: 1,
    resizeMode: 'contain',
    height: '150%',
    width: 'auto',
    padding: 0,
    margin: 0,
  },
  clickedMarkerRowContainer: {
    display: 'flex',
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  clickedMarkerTitle: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
  clickedMarkerTextContent: {
    color: 'white',
    fontSize: 12,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: '#ffffffbd',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '85%',
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 5,
    padding: 5,
    elevation: 2,
    marginTop: 15,
    width: '50%',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  container: {
    width: '100%',
  },
  loginTextContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    marginBottom: 5,
  },
  loginText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'black',
    fontSize: 18,
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  dayTitle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 13,
    marginTop: 8,
  },
  dayText: {
    color: 'black',
    fontWeight: '900',
    fontSize: 13,
    marginTop: 8,
  },
  suntimeTitle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 13,
  },
  suntimeText: {
    color: 'black',
    fontWeight: '900',
    fontSize: 12,
  },
  weatherInfoTextTop: {
    width: '90%',
    color: 'black',
    fontWeight: 'bold',
    marginTop: 15,
    fontSize: 11,
  },
  weatherPeriodText: {
    width: '90%',
    color: 'black',
    fontWeight: 'bold',
    marginTop: 5,
    fontSize: 11,
  },
  weatherInfoText: {
    width: '90%',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 11,
  },
});
