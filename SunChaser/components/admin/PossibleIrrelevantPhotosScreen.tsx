import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Modal,
  Alert,
  TouchableHighlight,
  RefreshControl,
  SafeAreaView,
  AppState,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../store';
import {fetchPendingAndDisapprovedPhotos} from '../../redux/adminPhotos/thunks';
import {updatePhotoStatusThunk} from '../../redux/adminPhotoStatus/thunk';
import config from '../../config';

//@ts-ignore
const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export default function PossibleIrrelevantPhotosScreen(props: any) {
  const pendingAndDisapprovedPhotoIds = useSelector(
    (state: RootState) =>
      state.pendingAndDisapprovedPhotos.pendingAndDisapprovedPhotoIds,
  );
  const pendingAndDisapprovedPhotos = useSelector((state: RootState) =>
    pendingAndDisapprovedPhotoIds.map(
      (id) =>
        state.pendingAndDisapprovedPhotos.pendingAndDisapprovedPhotoById[id],
    ),
  );

  const pendingAndDisapprovedPhotoStatus = useSelector((state: RootState) =>
    pendingAndDisapprovedPhotos.map((thePhoto) => thePhoto.status),
  );

  const pendingAndDisapprovedPhotoId = useSelector((state: RootState) =>
    pendingAndDisapprovedPhotos.map((thePhoto) => thePhoto.id),
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchPendingAndDisapprovedPhotos());
  }, [dispatch]);

  const openModalApprove = (value: number) => {
    setModalApproveVisible(true);
    setModalPhotoId(value);
  };

  const openModalDisapprove = (value: number) => {
    setModalDisapproveVisible(true);
    setModalPhotoId(value);
  };

  const [modalPhotoId, setModalPhotoId] = useState(0);

  const [modalApproveVisible, setModalApproveVisible] = useState(false);

  const [modalDisapproveVisible, setModalDisapproveVisible] = useState(false);

  type PhotoStatus = {
    id: number;
    status: string;
  };

  const onSubmit = ({id, status}: PhotoStatus) => {
    dispatch(updatePhotoStatusThunk(id, status));

    for (let x = 0; x < pendingAndDisapprovedPhotoId.length; x++) {
      if (pendingAndDisapprovedPhotoId[x] === id) {
        () => {
          pendingAndDisapprovedPhotoStatus[x] = status;
        };
        // dispatch(fetchPendingAndDisapprovedPhotos());
        setTimeout(() => setModalApproveVisible(false), 500);
        setTimeout(() => setModalDisapproveVisible(false), 500);
        setTimeout(() => dispatch(fetchPendingAndDisapprovedPhotos()), 1500);
      }
    }
    // setTimeout(() => setModalApproveVisible(false), 1000);
    // setTimeout(() => setModalDisapproveVisible(false), 1000);
    // setTimeout(() => dispatch(fetchPendingAndDisapprovedPhotos()), 1000);
    // setTimeout(() => dispatch(fetchPendingAndDisapprovedPhotos()), 3000);
  };

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(fetchPendingAndDisapprovedPhotos());
    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <SafeAreaView style={{flex: 1, width: '100%'}}>
      <ScrollView
        style={styles.body}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <ImageBackground
          style={styles.background}
          source={require('../../images/backgrounds/sunrise_backup_3.jpg')}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalApproveVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={styles.modalContainer}>
                  <View style={styles.modalBigContainer}>
                    <Image
                      style={styles.logo}
                      source={require('../../icons/logo_lightorange.png')}
                    />
                    <Text style={styles.titleText}>SUN-CHASER HONG KONG</Text>
                    <Text style={styles.modalQuestion}>
                      Are you sure to approve this photo (id:{modalPhotoId})?
                    </Text>
                  </View>
                </View>
                <View style={styles.approvalButtonsRow}>
                  <TouchableHighlight
                    style={{
                      ...styles.openButton,
                      width: '40%',
                      backgroundColor: 'green',
                    }}
                    onPress={() => {
                      onSubmit({id: modalPhotoId, status: 'shown'});
                    }}>
                    <Text style={styles.textStyle}>Confirm</Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={{
                      ...styles.openButton,
                      width: '40%',
                      backgroundColor: 'grey',
                    }}
                    onPress={() => {
                      setModalApproveVisible(!modalApproveVisible);
                    }}>
                    <Text style={styles.textStyle}>Cancel</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </Modal>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalDisapproveVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={styles.modalContainer}>
                  <View style={styles.modalBigContainer}>
                    <Image
                      style={styles.logo}
                      source={require('../../icons/logo_lightorange.png')}
                    />
                    <Text style={styles.titleText}>SUN-CHASER HONG KONG</Text>
                    <Text style={styles.modalQuestion}>
                      Are you sure to DISAPPROVE this photo (id:{modalPhotoId})?
                    </Text>
                  </View>
                </View>
                <View style={styles.approvalButtonsRow}>
                  <TouchableHighlight
                    style={{
                      ...styles.openButton,
                      width: '40%',
                      backgroundColor: 'red',
                    }}
                    onPress={() => {
                      onSubmit({id: modalPhotoId, status: 'disapproved'});
                    }}>
                    <Text style={styles.textStyle}>Confirm</Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={{
                      ...styles.openButton,
                      width: '40%',
                      backgroundColor: 'grey',
                    }}
                    onPress={() => {
                      setModalDisapproveVisible(!modalDisapproveVisible);
                    }}>
                    <Text style={styles.textStyle}>Cancel</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </Modal>
          <View style={styles.screenTitleConatiner}>
            <Text style={styles.screenTitle}> </Text>
          </View>

          {pendingAndDisapprovedPhotos.map((pendingAndDisapprovedPhoto) => (
            <View
              style={styles.photoDataContainer}
              key={pendingAndDisapprovedPhoto.id}>
              <View style={styles.photoDataLittleContainer}>
                <View style={styles.photoHeadingRowContainer}>
                  <View style={styles.photoHeadingRow}>
                    <Text style={styles.photoHeading}>
                      {pendingAndDisapprovedPhoto.username}
                    </Text>
                    <View style={styles.approvalOptionsContainer}>
                      <TouchableOpacity style={styles.optionApproveButton}>
                        <Image
                          style={{width: 20, height: 20}}
                          source={require('../../icons/tick-64.png')}
                        />
                        <Text
                          style={{
                            color: '#009B72',
                            fontSize: 18,
                            fontWeight: 'bold',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          onPress={() =>
                            openModalApprove(pendingAndDisapprovedPhoto.id)
                          }>
                          Approve
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.optionDisapproveButton}>
                        <Image
                          style={{width: 20, height: 20}}
                          source={require('../../icons/cross-96.png')}
                        />
                        <Text
                          style={{
                            color: 'red',
                            fontSize: 18,
                            fontWeight: 'bold',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          onPress={() =>
                            openModalDisapprove(pendingAndDisapprovedPhoto.id)
                          }>
                          Disapprove
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={styles.photoStatusRowConatiner}>
                  <View style={styles.photoStatusRow}>
                    <Text style={{fontSize: 16, fontWeight: '900'}}>
                      Photo Status:{' '}
                    </Text>
                    <Text
                      style={
                        (pendingAndDisapprovedPhoto.status === 'pending' && {
                          fontSize: 16,
                          fontWeight: 'bold',
                          color: 'blue',
                        }) ||
                        (pendingAndDisapprovedPhoto.status ===
                          'disapproved' && {
                          fontSize: 16,
                          fontWeight: 'bold',
                          color: 'red',
                        })
                      }>
                      {(pendingAndDisapprovedPhoto.status === 'pending' &&
                        'Pending') ||
                        (pendingAndDisapprovedPhoto.status === 'disapproved' &&
                          'Disapproved')}
                    </Text>
                  </View>
                </View>
                <View style={styles.actualPhotoConatinerRow}>
                  <View style={styles.actualPhotoConatiner}>
                    <View style={styles.actualPhotoOverlay}>
                      <Image
                        style={styles.actualPhoto}
                        source={{
                          uri: `${config.BACKEND_URL}/uploads/${pendingAndDisapprovedPhoto.image}`,
                        }}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.photoInfoRowConatiner}>
                  <View style={styles.photoInfoRow}>
                    <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                      Title:{' '}
                    </Text>
                    <Text style={{fontSize: 13, fontWeight: '900'}}>
                      {pendingAndDisapprovedPhoto.title}
                    </Text>
                  </View>
                </View>
                <View style={styles.photoInfoRowConatiner}>
                  <View style={styles.photoInfoRow}>
                    <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                      Updated At:{' '}
                    </Text>
                    <Text style={{fontSize: 13, fontWeight: '900'}}>
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getFullYear()}
                      -
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getMonth() + 1}
                      -
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getDate()}{' '}
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getHours()}
                      :
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getMinutes()}
                      :
                      {new Date(
                        pendingAndDisapprovedPhoto.updated_at,
                      ).getSeconds()}
                    </Text>
                  </View>
                </View>
                <View style={styles.photoInfoRowConatiner}>
                  <View style={styles.photoInfoRow}>
                    <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                      Photo Time:{' '}
                    </Text>
                    <Text style={{fontSize: 13, fontWeight: '900'}}>
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getFullYear()}
                      -
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getMonth() + 1}
                      -
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getDate()}{' '}
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getHours()}
                      :
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getMinutes()}
                      :
                      {new Date(
                        pendingAndDisapprovedPhoto.created_at,
                      ).getSeconds()}
                    </Text>
                  </View>
                </View>
                <View style={styles.photoInfoRowConatiner}>
                  <View style={styles.photoInfoRow}>
                    <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                      Moment:{' '}
                    </Text>
                    <Text style={{fontSize: 13, fontWeight: '900'}}>
                      {(pendingAndDisapprovedPhoto.environment === 'sunrise' &&
                        'Sunrise 日出') ||
                        (pendingAndDisapprovedPhoto.environment === 'sunset' &&
                          'Sunset 日落')}
                    </Text>
                  </View>
                </View>
                <View style={styles.photoInfoRowConatiner}>
                  <View style={styles.photoInfoRow}>
                    <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                      District:{' '}
                    </Text>
                    <Text style={{fontSize: 13, fontWeight: '900'}}>
                      {(pendingAndDisapprovedPhoto.district ===
                        'Central And Western' &&
                        'Central And Western 中西區') ||
                        (pendingAndDisapprovedPhoto.district === 'Eastern' &&
                          'Eastern 東區') ||
                        (pendingAndDisapprovedPhoto.district === 'Wan Chai' &&
                          'Wan Chai 灣仔區') ||
                        (pendingAndDisapprovedPhoto.district === 'Southern' &&
                          'Southern 南區') ||
                        (pendingAndDisapprovedPhoto.district ===
                          'Yau Tsim Mong' &&
                          'Yau Tsim Mong 油尖旺區') ||
                        (pendingAndDisapprovedPhoto.district ===
                          'Sham Shui Po' &&
                          'Sham Shui Po 深水埗區') ||
                        (pendingAndDisapprovedPhoto.district ===
                          'Kowloon City' &&
                          'Kowloon City 九龍城區') ||
                        (pendingAndDisapprovedPhoto.district ===
                          'Wong Tai Sin' &&
                          'Wong Tai Sin 黃大仙區') ||
                        (pendingAndDisapprovedPhoto.district === 'Kwun Tong' &&
                          'Kwun Tong 觀塘區') ||
                        (pendingAndDisapprovedPhoto.district === 'Kwai Tsing' &&
                          'Kwai Tsing 葵青區') ||
                        (pendingAndDisapprovedPhoto.district === 'Tsuen Wan' &&
                          'Kwun Tong 荃灣區') ||
                        (pendingAndDisapprovedPhoto.district === 'Tuen Mun' &&
                          'Tuen Mun 屯門區') ||
                        (pendingAndDisapprovedPhoto.district === 'Yuen Long' &&
                          'Yuen Long 元朗區') ||
                        (pendingAndDisapprovedPhoto.district === 'North' &&
                          'North 北區') ||
                        (pendingAndDisapprovedPhoto.district === 'Tai Po' &&
                          'Tai Po 大埔區') ||
                        (pendingAndDisapprovedPhoto.district === 'Sha Tin' &&
                          'Sha Tin 沙田區') ||
                        (pendingAndDisapprovedPhoto.district === 'Sai Kung' &&
                          'Sai Kung 西貢區') ||
                        (pendingAndDisapprovedPhoto.district === 'Islands' &&
                          'Islands 離島區') ||
                        pendingAndDisapprovedPhoto.district}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          ))}
        </ImageBackground>
      </ScrollView>
    </SafeAreaView>
  );
}

const win = Dimensions.get('window');
const ratio = win.width / 541;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    width: '100%',
    alignContent: 'center',
  },
  background: {
    flex: 1,
    width: '100%',
    alignContent: 'center',
    height: '100%',
    minHeight: win.height,
  },
  screenTitleConatiner: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: 40,
    marginBottom: 5,
  },
  screenTitle: {
    justifyContent: 'center',
    alignContent: 'center',
    color: 'red',
    fontSize: 20,
    fontWeight: 'bold',
  },
  photoDataContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  photoDataLittleContainer: {
    width: '95%',
    display: 'flex',
    alignContent: 'center',
    backgroundColor: '#ffffffad',
    borderRadius: 5,
    padding: 5,
  },
  photoHeadingRowContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  photoHeadingRow: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  photoHeading: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  approvalOptionsContainer: {
    width: '65%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  optionApproveButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  optionDisapproveButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 10,
  },
  photoStatusRowConatiner: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  photoStatusRow: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  actualPhotoConatinerRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
  },
  actualPhotoConatiner: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    maxWidth: '100%',
    width: '95%',
    height: 315 * ratio,
    // paddingTop: "66.66%"
    // * 3:2 display ratio
  },
  actualPhotoOverlay: {
    position: 'absolute',
    backgroundColor: 'black',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
  },
  actualPhoto: {
    position: 'relative',
    // display: "flex",
    // flexDirection: "column",
    maxHeight: '100%',
    maxWidth: '100%',
    aspectRatio: 1,
    resizeMode: 'contain',
    height: '150%',
    width: 'auto',
    padding: 0,
    margin: 0,
  },
  photoInfoRowConatiner: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  photoInfoRow: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    height: '70%',
    justifyContent: 'center',
    margin: 20,
    backgroundColor: '#000000da',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '90%',
  },
  modalContainer: {
    width: '100%',
  },
  modalBigContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    marginBottom: 5,
  },
  titleText: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 18,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 5,
    padding: 5,
    elevation: 2,
    marginTop: 15,
    width: '50%',
  },
  modalQuestion: {
    marginTop: 80,
    fontSize: 13,
    color: 'white',
    fontWeight: '900',
  },
  approvalButtonsRow: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
