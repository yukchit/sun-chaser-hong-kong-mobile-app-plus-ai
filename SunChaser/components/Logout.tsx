import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {RootState} from '../store';
import {useDispatch, useSelector} from 'react-redux';
import {logout} from '../redux/auth/actions';

export default function Logout() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  const clickLogout = () => {
    dispatch(logout());
  };

  return (
    <>
      <View style={styles.logoutBar}>
        {isAuthenticated ? (
          <>
            <Text style={styles.word}>Do you want to logout?</Text>
            <TouchableOpacity
              style={styles.logoutButton}
              onPress={() => clickLogout}></TouchableOpacity>
          </>
        ) : (
          ''
        )}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  logoutBar: {
    flex: 1,
    justifyContent: 'center',
  },
  logoutButton: {},
  word: {},
});
