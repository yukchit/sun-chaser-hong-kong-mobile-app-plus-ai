export const DISTRICT = {
  Central_And_Western: {
    initialRegion: {
      latitude: 22.2821,
      longitude: 114.14004,
      latitudeDelta: 0.025,
      longitudeDelta: 0.063,
    },
    coordinates: {
      features: 0,
    },
  },
  Eastern: {
    initialRegion: {
      latitude: 22.2729,
      longitude: 114.2298,
      latitudeDelta: 0.025,
      longitudeDelta: 0.095,
    },
    coordinates: {
      features: 2,
    },
  },
  Wan_Chai: {
    initialRegion: {
      latitude: 22.2768,
      longitude: 114.1829,
      latitudeDelta: 0.025,
      longitudeDelta: 0.079,
    },
    coordinates: {
      features: 1,
    },
  },
  Southern: {
    initialRegion: {
      latitude: 22.2345,
      longitude: 114.2093,
      latitudeDelta: 0.025,
      longitudeDelta: 0.21,
    },
    coordinates: {
      features: 3,
    },
  },
  Yau_Tsim_Mong: {
    initialRegion: {
      latitude: 22.3072,
      longitude: 114.1665,
      latitudeDelta: 0.025,
      longitudeDelta: 0.068,
    },
    coordinates: {
      features: 4,
    },
  },
  Sham_Shui_Po: {
    initialRegion: {
      latitude: 22.3311,
      longitude: 114.1548,
      latitudeDelta: 0.025,
      longitudeDelta: 0.062,
    },
    coordinates: {
      features: 5,
    },
  },
  Kowloon_City: {
    initialRegion: {
      latitude: 22.32186,
      longitude: 114.19101,
      latitudeDelta: 0.025,
      longitudeDelta: 0.099,
    },
    coordinates: {
      features: 6,
    },
  },
  Wong_Tai_Sin: {
    initialRegion: {
      latitude: 22.34401,
      longitude: 114.20317,
      latitudeDelta: 0.025,
      longitudeDelta: 0.054,
    },
    coordinates: {
      features: 7,
    },
  },
  Kwun_Tong: {
    initialRegion: {
      latitude: 22.309,
      longitude: 114.22776,
      latitudeDelta: 0.025,
      longitudeDelta: 0.095,
    },
    coordinates: {
      features: 8,
    },
  },
  Tsuen_Wan: {
    initialRegion: {
      latitude: 22.3667,
      longitude: 114.08257,
      latitudeDelta: 0.025,
      longitudeDelta: 0.19,
    },
    coordinates: {
      features: 9,
    },
  },
  Tuen_Mun: {
    initialRegion: {
      latitude: 22.37876,
      longitude: 113.96194,
      latitudeDelta: 0.025,
      longitudeDelta: 0.22,
    },
    coordinates: {
      features: 10,
    },
  },
  Yuen_Long: {
    initialRegion: {
      latitude: 22.4555,
      longitude: 114.0141,
      latitudeDelta: 0.025,
      longitudeDelta: 0.24,
    },
    coordinates: {
      features: 11,
    },
  },
  North: {
    initialRegion: {
      latitude: 22.5183,
      longitude: 114.2075,
      latitudeDelta: 0.025,
      longitudeDelta: 0.265,
    },
    coordinates: {
      features: 12,
    },
  },
  Tai_Po: {
    initialRegion: {
      latitude: 22.48849,
      longitude: 114.28516,
      latitudeDelta: 0.025,
      longitudeDelta: 0.35,
    },
    coordinates: {
      features: 13,
    },
  },
  Sai_Kung: {
    initialRegion: {
      latitude: 22.33541,
      longitude: 114.32361,
      latitudeDelta: 0.025,
      longitudeDelta: 0.39,
    },
    coordinates: {
      features: 14,
    },
  },
  Sha_Tin: {
    initialRegion: {
      latitude: 22.392,
      longitude: 114.1991,
      latitudeDelta: 0.025,
      longitudeDelta: 0.19,
    },
    coordinates: {
      features: 15,
    },
  },
  Kwai_Tsing: {
    initialRegion: {
      latitude: 22.34098,
      longitude: 114.11176,
      latitudeDelta: 0.025,
      longitudeDelta: 0.16,
    },
    coordinates: {
      features: 16,
    },
  },
  Islands: {
    initialRegion: {
      latitude: 22.22729,
      longitude: 114.0643,
      latitudeDelta: 0.025,
      longitudeDelta: 0.5,
    },
    coordinates: {
      features: 17,
    },
  },
};
