import React, {useEffect, useState, useRef} from 'react';
import {
  ScrollView,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {loadMyPhotosThunk} from '../../redux/photos/thunks';
import {RootState} from '../../store';
import {useSelector, useDispatch} from 'react-redux';
import IPhoto from '../../redux/photos/state';
// import EasternPhotoDetails from './02EasternDetails';
import DistrictPhotoDetails from './DistrictsDetails';

export default function DistrictLongScreen(props: {
  photos: IPhoto[];
  route: any;
}) {
  const districtPhotoIds = useSelector(
    (state: RootState) => state.districtPhotos.photoIds,
  );
  const photos = useSelector((state: RootState) =>
    districtPhotoIds.map((id) => state.districtPhotos.photoById[id]),
  );

  const dispatch = useDispatch();

  const scrollRef = useRef<any>();

  const likeCount = useSelector(
    (state: RootState) => state.districtPhotos.likeCount,
  );
  useEffect(() => {
    console.log('LONG SCREEN COUNT');
  }, [likeCount]);

  useEffect(() => {
    setTimeout(() => {
      scrollRef.current.scrollTo({
        // x: 100,
        y: 635 * districtPhotoIds.indexOf(props.route.params.key),
        animated: true,
      });
    }, 500);
  }, []);

  const [refreshing, setRefreshing] = React.useState(false);

  const wait = (timeout: any) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(loadMyPhotosThunk());
    wait(1000).then(() => setRefreshing(false));
  }, []);

  return (
    <>
      <SafeAreaView
        style={{
          width: '100%',
        }}>
        <ScrollView
          ref={scrollRef}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {photos.map((photo) => (
            <DistrictPhotoDetails photo={photo} key={photo.id} /> //key={route.params.key}
          ))}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}
