import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from 'store';
import IPhoto, {IComment} from '../../redux/photos/state';
import config from '../../config';
import {useForm, Controller} from 'react-hook-form';
import {TouchableOpacity} from 'react-native-gesture-handler'; //android 要import 呢個先用到TouchableOpacity
import {useNavigation} from '@react-navigation/native';
import {
  addCommentThunk,
  loadPhotosCommentsThunk,
  loadPhotosThunk,
} from '../../redux/photos/thunks';
import {StackNavigationProp} from '@react-navigation/stack';
import {
  likePhotosDistricts,
  unLikePhotosDistricts,
} from '../../redux/photos/actions';

const regularHeart = require('../../icons/heart-regular.png');
const solidHeart = require('../../icons/heart-solid.png');

export default function DistrictPhotoDetails(props: {photo: IPhoto}) {
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();

  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const [textInput, setTextInput] = useState(false);

  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  const token = useSelector((state: RootState) => state.auth.token);

  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = (event: {nativeEvent: {lines: string | any[]}}) => {
    setLengthMore(event.nativeEvent.lines.length >= 1);
    // console.log(event.nativeEvent)
  };

  const districtComments: IComment[] = useSelector(
    (state: RootState) => state.districtPhotos.commentsById[props.photo.id],
  );

  const commentCount = useSelector(
    (state: RootState) => state.districtPhotos.commentCount,
  );

  const likeCount = useSelector(
    (state: RootState) => state.districtPhotos.likeCount,
  );

  useEffect(() => {
    dispatch(loadPhotosCommentsThunk(props.photo.id));
  }, [dispatch, commentCount, likeCount]);

  const likePhoto = async () => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/like/${props.photo.id}`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    if (res.status === 200) {
      dispatch(likePhotosDistricts(props.photo.id));
      console.log('200');
    }
  };

  const unlikePhoto = async () => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/unlike/${props.photo.id}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    if (res.status === 200) {
      dispatch(unLikePhotosDistricts(props.photo.id));
      console.log('200');
    }
  };

  type CommentFormData = {
    content: string;
  };

  const {control, handleSubmit, errors} = useForm<CommentFormData>();

  const onSubmit = async (data: any) => {
    dispatch(addCommentThunk(props.photo.id, data));
    Alert.alert(`Your content is submitted`);
    setTextInput(true);
  };

  const [isLoading, setIsLoading] = useState(true);

  //    {isLoading === true ? (
  //   <View>
  //     style={styles.isLoading}
  //     <ActivityIndicator color="#330066" size='large' animating />
  //   </View>
  // )

  return (
    <>
      <View>
        <Text style={styles.photoTitle}>{props.photo.username}</Text>

        <View style={styles.photoImageContainer}>
          <View style={styles.photoImageOverlay}>
            <Image
              source={{
                uri: `${config.BACKEND_URL}/uploads/${props.photo.image}`,
              }}
              style={styles.photoImage}
            />
          </View>
        </View>

        <View style={styles.commentLikeIcon}>
          {isAuthenticated ? (
            <View>
              {props.photo.liked ? (
                <TouchableOpacity onPress={unlikePhoto}>
                  <Image style={styles.tinyLogo} source={solidHeart} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={likePhoto}>
                  <Image style={styles.tinyLogo} source={regularHeart} />
                </TouchableOpacity>
              )}
            </View>
          ) : (
            <TouchableOpacity
              onPress={async () => {
                Alert.alert(
                  'Warning',
                  'You have not signed in. Please sign in and try again',
                );
                navigation.navigate('Sign in');
              }}>
              <Image style={styles.tinyLogo} source={regularHeart} />
            </TouchableOpacity>
          )}

          {isAuthenticated ? (
            <>
              <TouchableOpacity onPress={toggleNumberOfLines}>
                <Image
                  style={styles.tinyLogo}
                  source={require('../../icons/comment-regular.png')}
                />
              </TouchableOpacity>
            </>
          ) : (
            <TouchableOpacity
              onPress={async () => {
                Alert.alert(
                  'Warning',
                  'You have not signed in. Please sign in and try again',
                );
                navigation.navigate('Sign in');
              }}>
              <Image
                style={styles.tinyLogo}
                source={require('../../icons/comment-regular.png')}
              />
            </TouchableOpacity>
          )}
        </View>

        <View>
          <Text style={styles.likeComment}>
            {' '}
            {props.photo.total_likes} likes {props.photo.total_comments}{' '}
            Comments{' '}
          </Text>
        </View>

        <Text
          //@ts-ignore
          onTextLayout={onTextLayout}
          numberOfLines={textShown ? undefined : 2}
          style={styles.photoDetailsContainer}>
          <Text>
            <Text style={styles.photoUser}>Title: </Text>
            {props.photo.title}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoLocation}>Location: </Text>{' '}
            {props.photo.location}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDescription}>Description: </Text>{' '}
            {props.photo.description} {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDate}>Date and Time: </Text>{' '}
            {new Date(props.photo.updated_at).getFullYear()}-
            {new Date(props.photo.updated_at).getMonth() + 1}-
            {new Date(props.photo.updated_at).getDate()}
            {'  '}
            {new Date(props.photo.updated_at).getHours()}:
            {new Date(props.photo.updated_at).getMinutes()}:
            {new Date(props.photo.updated_at).getSeconds()}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoEnvironment}>Sunrise/Sunset: </Text>{' '}
            {props.photo.environment} {'\n'}
          </Text>{' '}
          <Text>{'\n'}</Text>
          <Text>
            Description: {props.photo.description} {'\n'}
          </Text>
          <Text>
            Location: {props.photo.location}
            {'\n'}
          </Text>
          <Text>
            Date and Time: {new Date(props.photo.updated_at).getFullYear()}-
            {new Date(props.photo.updated_at).getMonth() + 1}-
            {new Date(props.photo.updated_at).getDate()}
            {'  '}
            {new Date(props.photo.updated_at).getHours()}:
            {new Date(props.photo.updated_at).getMinutes()}:
            {new Date(props.photo.updated_at).getSeconds()}
            {'\n'}
          </Text>
          {districtComments &&
            districtComments.map((comment, i) => (
              <Text key={i}>
                {comment.username}: {`${comment.content}\n`}
              </Text>
            ))}
        </Text>

        {lengthMore ? (
          <Text
            onPress={toggleNumberOfLines}
            style={{lineHeight: 20, marginLeft: 20, marginBottom: 10}}>
            {textShown ? <Text>...Read less</Text> : <Text>...Read more</Text>}
          </Text>
        ) : null}

        {textShown ? (
          <View style={styles.commentArea}>
            <View style={styles.textAreaContainer}>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.textArea}
                    multiline={true}
                    numberOfLines={10}
                    placeholder="Add a Comment"
                    placeholderTextColor="grey"
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={textInput === false ? value : ''}
                    clearButtonMode="always"
                  />
                )}
                name="content"
                rules={{required: true}}
                defaultValue=""
              />

              {errors.content && (
                <Text style={styles.errorMsg}>Comment is required.</Text>
              )}
            </View>

            <View>
              {isAuthenticated ? (
                <TouchableOpacity
                  style={styles.commentButton}
                  onPress={handleSubmit(onSubmit)}>
                  <Text style={styles.commentButtonText}> Post</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.commentButton}
                  onPress={async () => {
                    Alert.alert(
                      'Warning',
                      'You have not signed in. Please sign in and try again',
                    );
                    navigation.navigate('Sign in');
                  }}>
                  <Text style={styles.commentButtonText}> Post</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        ) : null}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  photoTitle: {
    color: 'black',
    fontSize: 25,
    paddingTop: 15,
    paddingBottom: 10,
    fontWeight: 'bold',
    paddingLeft: 5,
  },
  username: {
    color: 'black',
    fontSize: 30,
    paddingTop: 10,
  },
  photoImageContainer: {
    display: 'flex',
    position: 'relative',
    maxWidth: '100%',
    width: 400,
    height: 400,
  },
  photoImageOverlay: {
    backgroundColor: 'black',
  },
  photoImage: {
    position: 'relative',
    maxHeight: '100%',
    maxWidth: '100%',
    aspectRatio: 1,
    resizeMode: 'contain',
    width: 'auto',
    padding: 0,
    margin: 0,
  },
  commentArea: {
    flexDirection: 'row',
    maxHeight: '5%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentButton: {
    display: 'flex',
    width: 60,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 25,
    backgroundColor: '#DDDDDD',
    height: 54,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  commentButtonText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  tinyLogo: {
    width: 27,
    height: 27,
    marginRight: 20,
  },
  likeComment: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingLeft: 16,
    paddingBottom: 20,
  },
  commentLikeIcon: {
    display: 'flex',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingTop: 15,
  },
  photoDetails: {
    display: 'flex',
  },
  photoDetailsContainer: {
    lineHeight: 30,
    flex: 1,
    flexWrap: 'wrap',
    paddingLeft: 20,
  },
  textArea: {
    height: 40,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 10,
    textAlignVertical: 'top',
  },
  textAreaContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    width: '90%',
    flex: 5,
    paddingLeft: 10,
    marginLeft: 15,
    borderRadius: 10,
    padding: 5,
  },
  errorMsg: {
    fontSize: 20,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    fontWeight: 'bold',
    backgroundColor: '#ff00007a',
    borderRadius: 2,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    width: '90%',
  },
  isLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoUser: {
    fontWeight: 'bold',
    fontSize: 15,
    fontFamily: 'Arial',
  },
  photoLocation: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  photoDescription: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  photoDate: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  photoComment: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  photoEnvironment: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  photoLatitude: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  photoLongitude: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
