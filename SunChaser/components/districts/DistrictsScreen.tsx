import React, {useRef, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Text,
  Dimensions,
  Alert,
  Platform,
  FlatList,
  SafeAreaView,
  ToastAndroid,
  RefreshControl,
} from 'react-native';
import MapView from 'react-native-map-clustering';
import {PROVIDER_GOOGLE, Marker, Polygon} from 'react-native-maps';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../store';
import {fetchMarkers} from '../../redux/markers/actions';
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import config from '../../config';
import {TouchableHighlight} from 'react-native-gesture-handler';
import IPhoto from '../../redux/photos/state';
import {loadDistrictPhotosThunk} from '../../redux/photos/thunks';
import {DISTRICT} from './districts';

const hkGeoJson = require('../../hksar_18_district_boundary.json');

interface ILocation {
  latitude: number;
  longitude: number;
}

export default function DistrictsScreen(props: {photos: IPhoto[]; route: any}) {
  const bottomSheet = useRef<BottomSheet>();
  const fall = new Animated.Value(1);
  const markerIds = useSelector((state: RootState) => state.markers.markerIds);
  const markers = useSelector((state: RootState) =>
    markerIds.map((id) => state.markers.markerById[id]),
  );
  const districtPhotoIds = useSelector(
    (state: RootState) => state.districtPhotos.photoIds,
  );
  const photos = useSelector((state: RootState) =>
    districtPhotoIds.map((id) => state.districtPhotos.photoById[id]),
  );

  const district: string = props.route.params.place;
  const regex = / /gi;
  const districtTemp = district.replace(regex, '_');

  const dispatch = useDispatch();
  const [clickedMarker, setClickedMarker] = useState({} as any);

  const navigation = useNavigation();

  const likeCount = useSelector(
    (state: RootState) => state.districtPhotos.likeCount,
  );
  const commentCount = useSelector(
    (state: RootState) => state.districtPhotos.commentCount,
  );

  useEffect(() => {
    dispatch(loadDistrictPhotosThunk(district));
  }, [likeCount, commentCount]);

  useEffect(() => {
    dispatch(fetchMarkers());
  }, [dispatch]);

  const [refreshing, setRefreshing] = React.useState(false);

  const [selectedId, setSelectedId] = useState(0);

  const wait = (timeout: any) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    // dispatch(loadDistrictPhotosThunk(district));
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const scrollRef = useRef();

  const renderHeader = () => {
    return (
      <View style={styles.BSHeader}>
        <TouchableOpacity style={styles.iconContainer}>
          <Image
            style={{width: 35, height: 15}}
            source={require('../../icons/horizontal-line-96-white.png')}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderContent = () => {
    return (
      <ScrollView key={clickedMarker.id}>
        <View style={styles.BSContent}>
          <Text style={styles.clickedMarkerUsername}>
            {clickedMarker.username}
          </Text>
          <View style={styles.clickedMarkerImageContainer}>
            <View style={styles.clickedMarkerImageOverlay}>
              <Image
                style={styles.clickedMarkerImage}
                source={{
                  uri: `https://sunchaserhk.pro/uploads/${clickedMarker.image}`,
                }}
              />
            </View>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Title: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {clickedMarker.title}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Moment: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {(clickedMarker.environment === 'sunrise' && 'Sunrise 日出') ||
                (clickedMarker.environment === 'sunset' && 'Sunset 日落')}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>District: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {(clickedMarker.district === 'Central And Western' &&
                'Central And Western 中西區') ||
                (clickedMarker.district === 'Eastern' && 'Eastern 東區') ||
                (clickedMarker.district === 'Wan Chai' && 'Wan Chai 灣仔區') ||
                (clickedMarker.district === 'Southern' && 'Southern 南區') ||
                (clickedMarker.district === 'Yau Tsim Mong' &&
                  'Yau Tsim Mong 油尖旺區') ||
                (clickedMarker.district === 'Sham Shui Po' &&
                  'Sham Shui Po 深水埗區') ||
                (clickedMarker.district === 'Kowloon City' &&
                  'Kowloon City 九龍城區') ||
                (clickedMarker.district === 'Wong Tai Sin' &&
                  'Wong Tai Sin 黃大仙區') ||
                (clickedMarker.district === 'Kwun Tong' &&
                  'Kwun Tong 觀塘區') ||
                (clickedMarker.district === 'Kwai Tsing' &&
                  'Kwai Tsing 葵青區') ||
                (clickedMarker.district === 'Tsuen Wan' &&
                  'Kwun Tong 荃灣區') ||
                (clickedMarker.district === 'Tuen Mun' && 'Tuen Mun 屯門區') ||
                (clickedMarker.district === 'Yuen Long' &&
                  'Yuen Long 元朗區') ||
                (clickedMarker.district === 'North' && 'North 北區') ||
                (clickedMarker.district === 'Tai Po' && 'Tai Po 大埔區') ||
                (clickedMarker.district === 'Sha Tin' && 'Sha Tin 沙田區') ||
                (clickedMarker.district === 'Sai Kung' && 'Sai Kung 西貢區') ||
                (clickedMarker.district === 'Islands' && 'Islands 離島區') ||
                clickedMarker.district}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Location: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {clickedMarker.location}
            </Text>
          </View>
          <View style={styles.clickedMarkerRowContainer}>
            <Text style={styles.clickedMarkerTitle}>Date and Time: </Text>
            <Text style={styles.clickedMarkerTextContent}>
              {new Date(clickedMarker.created_at).getFullYear()}-
              {new Date(clickedMarker.created_at).getMonth() + 1}-
              {new Date(clickedMarker.created_at).getDate()}{' '}
              {new Date(clickedMarker.created_at).getHours()}:
              {new Date(clickedMarker.created_at).getMinutes()}:
              {new Date(clickedMarker.created_at).getSeconds()}
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  };

  const [location, setLocation] = useState<ILocation | undefined>(undefined);

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'SUN-CHASER HONG KONG required Location permission',
          message:
            'We required Location permission in order to get device location ' +
            'Please grant us.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Alert.alert("You've access for the location");
        Geolocation.getCurrentPosition(
          (position) => {
            const {latitude, longitude} = position.coords;
            setLocation({
              latitude,
              longitude,
            });
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {enableHighAccuracy: true},
        );
      } else {
        console.log('Location permission denied');
        Alert.alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization('always');
      Geolocation.getCurrentPosition(
        (position) => {
          const {latitude, longitude} = position.coords;
          console.log('mapScreen lat lon', latitude, longitude);
          setLocation({
            latitude,
            longitude,
          });
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true},
      );
    }
    if (Platform.OS === 'android') {
      const onScreenLoad = () => {
        requestLocationPermission();
      };
      onScreenLoad();
    }
  }, []);

  return (
    <View style={styles.body}>
      <BottomSheet
        ref={bottomSheet as any}
        snapPoints={['50%', 0]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        callbackNode={fall}
        initialSnap={1}
      />
      <Animated.View
        style={{
          flex: 1,
          opacity: Animated.add(0.3, Animated.multiply(fall, 0.9)),
        }}>
        {location && (
          <MapView
            style={styles.map}
            clusteringEnabled={true}
            provider={PROVIDER_GOOGLE}
            //@ts-ignore
            initialRegion={DISTRICT[`${districtTemp}`].initialRegion}
            // region={{
            //     latitude: location.latitude,
            //     longitude: location.longitude,
            //     latitudeDelta: 0.0159,
            //     longitudeDelta: 0.053,
            // }}
            showsUserLocation={true}
            showsMyLocationButton={true}
            followsUserLocation={true}
            showsCompass={true}
            customMapStyle={[
              {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
              {
                elementType: 'labels.text.stroke',
                stylers: [{color: '#242f3e'}],
              },
              {
                elementType: 'labels.text.fill',
                stylers: [{color: '#746855'}],
              },
              {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#263c3f'}],
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#6b9a76'}],
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#38414e'}],
              },
              {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#212a37'}],
              },
              {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#9ca5b3'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#746855'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}],
              },
              {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f3d19c'}],
              },
              {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}],
              },
              {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}],
              },
              {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#17263c'}],
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}],
              },
              {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#17263c'}],
              },
              {
                featureType: 'poi.medical',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.business',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.place_of_worship',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
              {
                featureType: 'poi.school',
                stylers: [
                  {
                    visibility: 'off',
                  },
                ],
              },
            ]}>
            {/* <Geojson
            geojson={require('../../hksar_18_district_boundary.json')}
            strokeColor="#b57f1b65"
            strokeWidth={2}
            fillColor="#16161675"
          /> */}

            <Polygon
              coordinates={hkGeoJson.features[
                //@ts-ignore
                DISTRICT[`${districtTemp}`].coordinates.features
              ].geometry.coordinates[0].map(
                //@ts-ignore
                (eachPoint) => {
                  return {
                    longitude: eachPoint[0],
                    latitude: eachPoint[1],
                  };
                },
              )}
              strokeColor="#b57f1b65"
              strokeWidth={2}
              fillColor="#16161675"
            />

            {markers.map((marker) => (
              <Marker
                onPress={() => {
                  bottomSheet.current?.snapTo(0);
                  setClickedMarker(marker);
                }}
                coordinate={{
                  latitude: marker.latitude,
                  longitude: marker.longitude,
                }}
                image={require('../../icons/sunrise_logo_map.png')}
                key={marker.id}></Marker>
            ))}
          </MapView>
        )}
        <SafeAreaView
          style={{
            width: '100%',
            flex: 1,
          }}>
          <ScrollView style={styles.districtGallery}>
            <FlatList
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
              ref={(ref) => scrollRef}
              style={styles.scrollView}
              keyExtractor={(item) => String(item.id)}
              // data={districtPhotos}
              data={photos}
              horizontal={false}
              numColumns={3}
              extraData={selectedId}
              renderItem={({item, index}) => (
                <TouchableHighlight
                  style={styles.photoIcon}
                  onPress={() => {
                    navigation.navigate('Posts', {
                      key: item.id,
                    });
                    console.log('itemid', item.id);
                    // ToastAndroid.show(item.title, ToastAndroid.SHORT);
                  }}>
                  <Image
                    source={{
                      uri: `${config.BACKEND_URL}/uploads/${item.image}`,
                    }}
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                    }}
                  />
                </TouchableHighlight>
              )}
            />
          </ScrollView>
        </SafeAreaView>
      </Animated.View>
    </View>
  );
}

const win = Dimensions.get('window');
const ratio = win.width / 541;

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  BSHeader: {
    backgroundColor: '#000000c0',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    marginBottom: 0,
  },
  BSContent: {
    marginTop: 0,
    flex: 1,
    minHeight: 500,
    backgroundColor: '#000000c0',
    color: 'white',
    width: '100%',
    alignItems: 'center',
  },
  clickedMarkerUsername: {
    display: 'flex',
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
  },
  clickedMarkerImageContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    maxWidth: '100%',
    width: '95%',
    height: 340 * ratio,
    // paddingTop: "66.66%"
    // * 3:2 display ratio
  },
  clickedMarkerImageOverlay: {
    position: 'absolute',
    backgroundColor: 'black',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
  },
  clickedMarkerImage: {
    position: 'relative',
    // display: "flex",
    // flexDirection: "column",
    maxHeight: '100%',
    maxWidth: '100%',
    aspectRatio: 1,
    resizeMode: 'contain',
    height: '150%',
    width: 'auto',
    padding: 0,
    margin: 0,
  },
  clickedMarkerRowContainer: {
    display: 'flex',
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  clickedMarkerTitle: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
  clickedMarkerTextContent: {
    color: 'white',
    fontSize: 12,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  districtGallery: {
    flex: 1,
    width: '100%',
    marginTop: 0,
    padding: 0,
    marginBottom: 0,
    minHeight: 100,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  photoTitle: {
    color: 'black',
    fontSize: 5,
    paddingTop: 40,
  },
  photoIcon: {
    width: 0.33333 * win.width,
    maxHeight: 0.33333 * win.width,
    borderWidth: 1,
    borderColor: '#cccccc',
    minHeight: 130,
    flex: 1,
  },
  scrollView: {
    flex: 1,
    width: '100%',
  },
  renderSeparator: {
    height: 1,
    width: '100%',
    backgroundColor: 'black',
  },
});
