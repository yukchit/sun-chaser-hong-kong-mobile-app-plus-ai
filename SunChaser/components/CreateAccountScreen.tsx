import React, {useEffect} from 'react';
import {
  Text,
  TextInput,
  Alert,
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {ScrollView, TouchableHighlight} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {createUser} from '../redux/user/actions';
import {RootState} from '../store';
import {useNavigation} from '@react-navigation/native';

type FormData = {
  email: string;
  password: string;
  password_repeat: string;
  username: string;
};

export default function CreateAccountScreen() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {control, handleSubmit, errors, getValues} = useForm<FormData>();
  const isCreated = useSelector((state: RootState) => state.createUser.success);

  const onSubmit = ({email, password, username}: FormData) => {
    dispatch(createUser(email, password, username));
    console.log('After dispatch in createAccScreen', isCreated);
  };
  useEffect(() => {
    if (isCreated !== null) {
      if (!isCreated) {
        Alert.alert('Email already registered', '', [
          {
            text: 'Please retry',
          },
        ]);
      }
      if (isCreated) {
        Alert.alert('Account created successfully', '', [
          {text: 'OK', onPress: () => navigation.navigate('Sign in')},
        ]);
      }
    }
    return () => {};
  }, [isCreated]);
  console.log('bool', isCreated);
  return (
    <>
      <View style={styles.body}>
        <ImageBackground
          style={styles.background}
          source={require('../images/backgrounds/sunrise.jpg')}>
          <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollViewContainer}>
              <View style={styles.logoContainer}>
                <Image
                  style={styles.logo}
                  source={require('../icons/logo_lightorange.png')}
                />
              </View>
              <KeyboardAvoidingView style={styles.formContainer}>
                <Text style={styles.label}>E-mail (for Login):</Text>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Enter e-mail address'}
                      textContentType={'emailAddress'}
                      keyboardType={'email-address'}
                      keyboardAppearance={'dark'}
                      autoCompleteType={'email'}
                    />
                  )}
                  name="email"
                  rules={{
                    required: true,
                    pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  }}
                  defaultValue=""
                />
                {errors.email?.type === 'required' && (
                  <Text style={styles.errorMsg}>Your e-mail is required.</Text>
                )}
                {errors.email?.type === 'pattern' && (
                  <Text style={styles.errorMsg}>
                    Please enter a valid Email Address.
                  </Text>
                )}

                <Text style={styles.label}>Password:</Text>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Enter password'}
                      secureTextEntry={true}
                      textContentType={'password'}
                      keyboardAppearance={'dark'}
                    />
                  )}
                  name="password"
                  rules={{
                    required: true,
                    pattern: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}/,
                  }}
                  defaultValue=""
                />
                {errors.password?.type === 'required' && (
                  <Text style={styles.errorMsg}>
                    Your password is required.
                  </Text>
                )}
                {errors.password?.type === 'pattern' && (
                  <Text style={styles.errorMsg}>
                    Must contain at least one number and one uppercase and
                    lowercase letter, and at least 5 or more characters.
                  </Text>
                )}

                <Text style={styles.label}>Confirm Password:</Text>
                <Controller
                  control={control}
                  name="password_repeat"
                  rules={{
                    required: true,
                    validate: (value) => {
                      if (value === getValues()['password']) {
                        return true;
                      } else {
                        return false;
                      }
                    },
                    pattern: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}/,
                  }}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Re-Enter password'}
                      secureTextEntry={true}
                      textContentType={'password'}
                      keyboardAppearance={'dark'}
                    />
                  )}
                  defaultValue=""
                />
                {errors.password_repeat?.type === 'required' && (
                  <Text style={styles.errorMsg}>
                    Your password is required.
                  </Text>
                )}
                {errors.password_repeat && (
                  <Text style={styles.errorMsg}>
                    The passwords do not match
                  </Text>
                )}

                <Text style={styles.label}>Display Name:</Text>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={(value) => onChange(value)}
                      value={value}
                      placeholder={'Enter display name'}
                      keyboardAppearance={'dark'}
                    />
                  )}
                  name="username"
                  type="text"
                  rules={{required: true, maxLength: 20, pattern: /^(\d|\w)+$/}}
                  defaultValue=""
                />
                {errors.username?.type === 'required' && (
                  <Text style={styles.errorMsg}>
                    Your display name is required.
                  </Text>
                )}
                {errors.username?.type === 'maxLength' && (
                  <Text style={styles.errorMsg}>Display Name maxLength 20</Text>
                )}
                {errors.username?.type === 'pattern' && (
                  <Text style={styles.errorMsg}>
                    Please enter your display name WITHOUT spaces and special
                    characters.
                  </Text>
                )}

                <View style={styles.submitButtonContainer}>
                  <View style={styles.submitButtonSubContainer}>
                    <TouchableHighlight
                      style={styles.submitButton}
                      underlayColor="#0fa9bdc5"
                      onPress={handleSubmit(onSubmit)}>
                      <Text style={styles.submitButtonText}>SUBMIT</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </KeyboardAvoidingView>
            </ScrollView>
          </SafeAreaView>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
  },
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    padding: 8,
    width: '100%',
  },
  scrollViewContainer: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  logo: {
    width: 60,
    height: 60,
    marginBottom: 5,
    marginTop: 40,
  },
  formContainer: {
    flex: 4,
    justifyContent: 'flex-start',
    width: '100%',
  },
  label: {
    marginTop: 5,
    marginBottom: 0,
    marginLeft: 20,
    color: 'white',
    fontSize: 13,
    fontWeight: 'bold',
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    padding: 10,
    borderRadius: 4,
    marginTop: 2,
    marginLeft: 20,
    marginRight: 20,
  },
  errorMsg: {
    fontSize: 10,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    fontWeight: 'bold',
    backgroundColor: '#ff00007a',
    borderRadius: 2,
    padding: 2,
  },
  submitButtonContainer: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  submitButtonSubContainer: {
    width: '90%',
  },
  submitButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0fa8bd',
    height: 38,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  submitButtonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 14,
  },
});
