import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default function KowloonScreen(props: any) {
  return (
    <View style={styles.body}>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Yau Tsim Mong 油尖旺區', {
            place: 'Yau Tsim Mong',
          });
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/yautsimmong.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>
              Yau Tsim Mong 油尖旺區{' '}
            </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Sham Shui Po 深水埗區', {
            place: 'Sham Shui Po',
          });
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/shamshuipo.jpg')}>
          <View style={styles.textContainerWhite}>
            <Text style={styles.sectionTitleBlack}>Sham Shui Po 深水埗區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-black.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Kowloon City 九龍城區', {
            place: 'Kowloon City',
          });
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/kowlooncity.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>Kowloon City 九龍城區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Wong Tai Sin 黃大仙區', {
            place: 'Wong Tai Sin',
          });
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/wongtaisin.jpg')}>
          <View style={styles.textContainerWhite}>
            <Text style={styles.sectionTitleBlack}>Wong Tai Sin 黃大仙區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-black.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Kwun Tong 觀塘區', {place: 'Kwun Tong'});
        }}
        style={styles.sectionContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={require('../images/kwuntong.jpg')}>
          <View style={styles.textContainerBlack}>
            <Text style={styles.sectionTitleWhite}>Kwun Tong 觀塘區 </Text>
            <Image
              style={styles.arrowIcon}
              source={require('../icons/double-right-30-white.png')}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  sectionContainer: {
    marginTop: 0,
    padding: 0,
    marginBottom: 0,
    flex: 1,
  },
  imageBackground: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  textContainerBlack: {
    backgroundColor: '#16161680',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleWhite: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  arrowIcon: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 5,
  },
  textContainerWhite: {
    backgroundColor: '#ffffff80',
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  sectionTitleBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
});
