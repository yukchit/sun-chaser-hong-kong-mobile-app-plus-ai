import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  createRef,
} from 'react';
import {
  ScrollView,
  RefreshControl,
  SafeAreaView,
  View,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import {loadMyPhotosThunk} from '../../redux/photos/thunks';
import {RootState} from '../../store';
import {useSelector, useDispatch} from 'react-redux';
import IPhoto from '../../redux/photos/state';
import ProfilePhotoDetails from './ProfilePhotoDetails';

// @ts-ignore
// route
export default function ProfileLongScreen(props: {photos: IPhoto[]; route}) {
  // console.log("param key", route.params.key);
  const profilePhotoIds = useSelector(
    (state: RootState) => state.profilePhotos.photoIds,
  );
  const profilePhotos = useSelector((state: RootState) =>
    profilePhotoIds.map((id) => state.profilePhotos.photoById[id]),
  );

  const dispatch = useDispatch();

  const scrollRef = useRef<any>();

  useEffect(() => {
    dispatch(loadMyPhotosThunk());
  }, [dispatch]);

  useEffect(() => {
    setTimeout(() => {
      scrollRef.current.scrollTo({
        // x: 100,
        y: 631 * profilePhotoIds.indexOf(props.route.params.key),
        animated: true,
      });
    }, 500);
  }, []);

  const [refreshing, setRefreshing] = React.useState(false);

  const wait = (timeout: any) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(loadMyPhotosThunk());
    wait(1000).then(() => setRefreshing(false));
  }, []);

  return (
    <>
      <SafeAreaView
        style={{
          width: '100%',
        }}>
        <ScrollView
          ref={scrollRef}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {profilePhotos.map((photo) => (
            <ProfilePhotoDetails photo={photo} key={photo.id} /> //key={route.params.key}
          ))}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}
