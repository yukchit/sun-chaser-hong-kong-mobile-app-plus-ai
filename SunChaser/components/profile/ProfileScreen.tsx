import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableHighlight,
  SafeAreaView,
  RefreshControl,
  FlatList,
  ToastAndroid,
  Platform,
  ScrollView,
} from 'react-native';
import {RootState} from '../../store';
import {loadMyPhotosThunk} from '../../redux/photos/thunks';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import config from '../../config';

export default function ProfileScreen() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const profilePhotoIds = useSelector(
    (state: RootState) => state.profilePhotos.photoIds,
  );
  const photos = useSelector((state: RootState) =>
    profilePhotoIds.map((id) => state.profilePhotos.photoById[id]),
  );

  const [refreshing, setRefreshing] = React.useState(false);

  const wait = (timeout: any) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(loadMyPhotosThunk());
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    dispatch(loadMyPhotosThunk());
    console.log('profile screen useEffect');
  }, [dispatch]);

  const [selectedId, setSelectedId] = useState(0);

  const scrollRef = useRef();

  return (
    <>
      <SafeAreaView
        style={{
          width: '100%',
        }}>
        <ScrollView>
          <FlatList
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            ref={(ref) => scrollRef}
            keyExtractor={(item) => String(item.id)}
            data={photos}
            horizontal={false}
            numColumns={3}
            extraData={selectedId}
            renderItem={({item, index}) => (
              <TouchableHighlight
                style={styles.photoIcon}
                onPress={() => {
                  navigation.navigate('My Photos', {key: item.id});
                  console.log('itemid', item.id);
                  Platform.OS === 'android' &&
                    ToastAndroid.show(item.title, ToastAndroid.SHORT);
                }}>
                <Image
                  source={{
                    uri: `${config.BACKEND_URL}/uploads/${item.image}`,
                  }}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                  }}
                />
              </TouchableHighlight>
            )}
          />
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  photoTitle: {
    color: 'black',
    fontSize: 5,
    paddingTop: 40,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  photoIcon: {
    width: '33.333%',
    height: '100%',
    borderWidth: 1,
    borderColor: '#cccccc',
    minHeight: 130,
  },
  renderSeparator: {
    height: 1,
    width: '100%',
    backgroundColor: 'black',
  },
  isLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
