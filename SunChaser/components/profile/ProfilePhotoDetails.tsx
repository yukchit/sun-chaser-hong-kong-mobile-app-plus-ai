import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, TextInput, Alert} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from 'store';
import IPhoto, {IComment} from '../../redux/photos/state';
import config from '../../config';
import {useForm, Controller} from 'react-hook-form';
import {TouchableOpacity} from 'react-native-gesture-handler'; //android 要import 呢個先用到TouchableOpacity
import {useNavigation} from '@react-navigation/native';
import {
  likePhotosProfileThunk,
  addCommentThunk,
  unLikePhotosProfileThunk,
  loadPhotosCommentsThunk,
  loadMyPhotosThunk,
} from '../../redux/photos/thunks';
import {StackNavigationProp} from '@react-navigation/stack';

const regularHeart = require('../../icons/heart-regular.png');
const solidHeart = require('../../icons/heart-solid.png');

export default function ProfilePhotoDetails(props: {photo: IPhoto}) {
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();

  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const [textInput, setTextInput] = useState(false);

  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = (event: {nativeEvent: {lines: string | any[]}}) => {
    setLengthMore(event.nativeEvent.lines.length >= 1);
  };

  const profileComments: IComment[] = useSelector(
    (state: RootState) => state.profilePhotos.commentsById[props.photo.id],
  );

  const commentCount = useSelector(
    (state: RootState) => state.profilePhotos.commentCount,
  );

  const likeCount = useSelector(
    (state: RootState) => state.profilePhotos.likeCount,
  );

  useEffect(() => {
    dispatch(loadPhotosCommentsThunk(props.photo.id));
  }, [dispatch, commentCount, profileComments, likeCount]);

  const likePhoto = () => {
    dispatch(likePhotosProfileThunk(props.photo.id));
    dispatch(loadMyPhotosThunk());
  };
  const unlikePhoto = () => {
    dispatch(unLikePhotosProfileThunk(props.photo.id));
    dispatch(loadMyPhotosThunk());
  };

  type CommentFormData = {
    content: string;
  };

  const {control, handleSubmit, errors} = useForm<CommentFormData>();

  const onSubmit = async (data: any) => {
    dispatch(addCommentThunk(props.photo.id, data));
    Alert.alert(`Your content is submitted`);
    setTextInput(true);
  };

  const [isLoading, setIsLoading] = useState(true);

  //    {isLoading === true ? (
  //   <View>
  //     style={styles.isLoading}
  //     <ActivityIndicator color="#330066" size='large' animating />
  //   </View>
  // )

  return (
    <>
      <View>
        <Text style={styles.photoTitle}>{props.photo.title}</Text>

        <View style={styles.photoImageContainer}>
          <View style={styles.photoImageOverlay}>
            <Image
              source={{
                uri: `${config.BACKEND_URL}/uploads/${props.photo.image}`,
              }}
              style={styles.photoImage}
            />
          </View>
        </View>

        <View style={styles.commentLikeIcon}>
          {isAuthenticated && (
            <View>
              {props.photo.liked ? (
                <TouchableOpacity onPress={unlikePhoto}>
                  <Image style={styles.tinyLogo} source={solidHeart} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={likePhoto}>
                  <Image style={styles.tinyLogo} source={regularHeart} />
                </TouchableOpacity>
              )}
            </View>
          )}

          {isAuthenticated && (
            <>
              <TouchableOpacity onPress={toggleNumberOfLines}>
                <Image
                  style={styles.tinyLogo}
                  source={require('../../icons/comment-regular.png')}
                />
              </TouchableOpacity>
            </>
          )}
        </View>

        <View>
          <Text style={styles.likeComment}>
            {' '}
            {props.photo.total_likes} likes {props.photo.total_comments}{' '}
            Comments{' '}
          </Text>
        </View>

        <Text
          //@ts-ignore
          onTextLayout={onTextLayout}
          numberOfLines={textShown ? undefined : 2}
          style={styles.photoDetailsContainer}>
          <Text>
            <Text style={styles.photoDetailsText}>Title: </Text>
            {props.photo.title}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDetailsText}>District: </Text>
            {props.photo.district}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDetailsText}>Location: </Text>{' '}
            {props.photo.location}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDetailsText}>Description: </Text>{' '}
            {props.photo.description} {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDetailsText}>Date and Time: </Text>{' '}
            {new Date(props.photo.updated_at).getFullYear()}-
            {new Date(props.photo.updated_at).getMonth() + 1}-
            {new Date(props.photo.updated_at).getDate()}
            {'  '}
            {new Date(props.photo.updated_at).getHours()}:
            {new Date(props.photo.updated_at).getMinutes()}:
            {new Date(props.photo.updated_at).getSeconds()}
            {'\n'}
          </Text>
          <Text>
            <Text style={styles.photoDetailsText}>Sunrise/Sunset: </Text>{' '}
            {props.photo.environment} {'\n'}
          </Text>

          {profileComments &&
            profileComments.map((comment, i) => (
              <Text key={i}>
                {comment.username}: {`${comment.content}\n`}
              </Text>
            ))}
        </Text>

        {lengthMore ? (
          <Text
            onPress={toggleNumberOfLines}
            style={{lineHeight: 20, marginLeft: 20, marginBottom: 10}}>
            {textShown ? <Text>...Read less</Text> : <Text>...Read more</Text>}
          </Text>
        ) : null}

        {textShown ? (
          <View style={styles.commentArea}>
            <View style={styles.textAreaContainer}>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.textArea}
                    multiline={true}
                    numberOfLines={10}
                    placeholder="Add a Comment"
                    placeholderTextColor="grey"
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={textInput === false ? value : ''}
                    clearButtonMode="always"
                  />
                )}
                name="content"
                rules={{required: true}}
                defaultValue=""
              />

              {errors.content && (
                <Text style={styles.errorMsg}>Comment is required.</Text>
              )}
            </View>

            <View>
              {isAuthenticated ? (
                <TouchableOpacity
                  style={styles.commentButton}
                  onPress={handleSubmit(onSubmit)}>
                  <Text style={styles.commentButtonText}> Post</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.commentButton}
                  onPress={async () => {
                    Alert.alert(
                      'Warning',
                      'You have not signed in. Please sign in and try again',
                    );
                    navigation.navigate('Sign in');
                  }}>
                  <Text style={styles.commentButtonText}> Post</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        ) : null}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  photoTitle: {
    color: 'black',
    fontSize: 25,
    paddingTop: 15,
    paddingBottom: 10,
    fontWeight: 'bold',
    paddingLeft: 5,
  },
  username: {
    color: 'black',
    fontSize: 30,
    paddingTop: 10,
  },
  photoImageContainer: {
    width: '100%',
    height: 415,
  },
  photoImageOverlay: {
    backgroundColor: 'black',
    width: '100%',
  },
  photoImage: {
    aspectRatio: 1,
    resizeMode: 'contain',
    width: '100%',
  },
  commentButton: {
    display: 'flex',
    width: 60,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 25,
    backgroundColor: '#DDDDDD',
    height: 54,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  commentButtonText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  commentArea: {
    flexDirection: 'row',
    maxHeight: '5%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tinyLogo: {
    width: 27,
    height: 27,
    marginRight: 20,
  },
  likeComment: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingLeft: 16,
  },
  commentLikeIcon: {
    display: 'flex',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  photoDetails: {
    display: 'flex',
  },
  photoDetailsContainer: {
    lineHeight: 30,
    flex: 1,
    flexWrap: 'wrap',
    paddingLeft: 20,
  },
  textArea: {
    height: 40,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 10,
    textAlignVertical: 'top',
  },
  textAreaContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    width: '90%',
    flex: 5,
    paddingLeft: 10,
    marginLeft: 15,
    borderRadius: 10,
    padding: 5,
  },
  errorMsg: {
    fontSize: 20,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    fontWeight: 'bold',
    backgroundColor: '#ff00007a',
    borderRadius: 2,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    width: '90%',
  },
  readMore: {
    paddingLeft: 10,
  },
  photoDetailsText: {
    fontWeight: 'bold',
    fontSize: 15,
  },
});
