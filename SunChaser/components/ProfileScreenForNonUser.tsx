import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Modal,
  Alert,
  TouchableHighlight,
  RefreshControl,
  SafeAreaView
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import {useNavigation} from '@react-navigation/native';




export default function ProfileScreenForNonUser(props: any){
    const navigation = useNavigation();


    return(
      <SafeAreaView style={{flex:1, width:'100%'}}>
        <ScrollView style={styles.body}>
          <ImageBackground
            style={styles.background}
            source={require('../images/backgrounds/sunrise_backup_4.jpg')}>
                <View style={styles.container}>
                    <View style={styles.littleContainer}>
                        <Image
                        style={styles.logo}
                        source={require('../icons/logo_lightorange.png')}
                        />
                        <Text style={styles.title}>SUN-CHASER HONG KONG</Text>
                        <Text style={styles.text}>Please sign in to use this function.</Text>

                        <View style={styles.buttonsRow}>
                        <TouchableHighlight
                            style={{ ...styles.openButton, width: '40%', backgroundColor: "orange", justifyContent:"center" }}
                            onPress={() => {
                                navigation.navigate("Sign in");
                            }}
                        >
                            <Text style={styles.textStyle}>Login</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            style={{ ...styles.openButton, width: '40%', backgroundColor: "green", justifyContent:"center" }}
                            onPress={() => {
                                navigation.navigate("Create Account");
                            }}
                        >
                            <Text style={styles.textStyle}>Create Account</Text>
                        </TouchableHighlight>
                        </View>
                        <TouchableHighlight
                            style={{ ...styles.openButton, width: '90%', backgroundColor: "grey", justifyContent:"center" }}
                            onPress={() => {
                                navigation.goBack();
                            }}>
                            <Text style={styles.textStyle}>BACK</Text>
                        </TouchableHighlight>

                    </View>
                    
                </View>
          </ImageBackground>      
        </ScrollView>
        </SafeAreaView>
    )
}


const win = Dimensions.get('window');
const ratio = win.width / 541;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    width: '100%',
    alignContent: 'center',
  },
  background: {
    flex: 1,
    width: '100%',
    alignContent: 'center',
    height: '100%',
    minHeight: win.height,
  },
  container: {
    flex: 1,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  littleContainer: {
    height: '50%',
    width: '70%',
    backgroundColor: "#ffffffbd",
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
      height: 50,
      width: 50
  },
  title: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 50,
  },
  text: {
      color: 'black',
      fontSize: 15,
      fontWeight: 'bold'
  },
  buttonsRow: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 5,
    padding: 5,
    elevation: 2,
    marginTop: 15,
    width: '50%'
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    justifyContent: 'center'
  },
})