import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image, SafeAreaView} from 'react-native';
import {useNavigation, StackActions} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from 'store';
import {restoreLogin} from '../redux/auth/actions';
import {DotIndicator} from 'react-native-indicators';

export default function LoadingScreen() {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  useEffect(() => {
    if (isAuthenticated) {
      navigation.dispatch(StackActions.replace('Tabs'));
    } else if (isAuthenticated === false) {
      navigation.dispatch(StackActions.replace('Welcome'));
    } else {
      dispatch(restoreLogin());
    }
  }, [isAuthenticated]);

  return (
    <View style={styles.body}>
      <SafeAreaView style={styles.safeAreaViewContainer}>
        <Image
          style={styles.logo}
          source={require('../icons/logo_white.png')}
        />
        <View style={styles.loadingIconContainer}>
          <DotIndicator color="white" size={15} />
        </View>
        <Text style={styles.loadingText}>LOADING...</Text>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#FFB229',
  },
  safeAreaViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  logo: {
    width: 80,
    height: 80,
    marginBottom: 5,
  },
  loadingIconContainer: {
    marginTop: 15,
    height: 15,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  loadingText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
  },
});
