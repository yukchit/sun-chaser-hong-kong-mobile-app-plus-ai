import React from 'react';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { render } from '@testing-library/react-native';

import { Tabs } from '../App';

import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store'
import thunk from 'redux-thunk'

import { Provider } from 'react-redux';



// Silence the warning https://github.com/facebook/react-native/issues/11094#issuecomment-263240420
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const mockStore = configureMockStore([thunk]);


describe('Testing react navigation as Admin', () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      auth: {
        "isAuthenticated": true, user: { role: 'admin', id: 1, email: 'testing@test.com' }
      },
      markers: {
        markerIds: [1],
        markerById: {
          [1]: {
            id: 1,
            image: '1.jpg',
            title: 'title_1',
            description: 'description_1',
            location: 'location_1',
            district: 'Islands',
            latitude: 22.2466,
            longitude: 22.2466,
            created_at: "2020-06-22T09:59:54.000Z",
            username: "user 1",
            status: 'shown',
            environment: 'sunrise'
          },
        }
      },
      suntimes: {
        todayResult: {
          date: '2020-06-25',
          sunrise: '05:45',
          sunset: '18:30'
        },
        tomorrowResult: {
          date: '2020-06-26',
          sunrise: '05:46',
          sunset: '18:31'
        }
      },
      weatherConditions: {
        generalSituation: "",
        forecastPeriod: "",
        forecastDesc: "",
      },
      galleryPhotos: {
        photoIds: [],
        photoById: {},
        commentsById: {},
        commentCount: 0,
        likeCount: 0,
        galleryPhotoCount: 0,
      },
      profilePhotos: {
        photoIds: [],
        photoById: {},
        commentsById: {},
        commentCount: 0,
        likeCount: 0,
        profilePhotoCount: 0,
      },
      districtPhotos: {
        photoIds: [],
        photoById: {},
        commentsById: {},
        commentCount: 0,
        likeCount: 0,
        districtPhotoCount: 0,
      },
      createUser: {
        success: null,
      },
      photos: {
        photoIds: [],
        photoById: {},
        photoStatus: 'shown',
        addPhotoCount: 0,
      },
      pendingAndDisapprovedPhotos: {
        pendingAndDisapprovedPhotoIds: [],
        pendingAndDisapprovedPhotoById: {},
      },
      photoStatus: {
        id: 0,
        status: '',
      },
    })
  });


  test('page contains the Text', async () => {
    const component = (
        <NavigationContainer>
          <Provider store={store}>
            <Tabs />
          </Provider> 
         </NavigationContainer>
    );

    const { findByText } = render(component);

    const appStoreListText = findByText(/Possible Irrelevant Photo(s)/i);

    expect(appStoreListText).toBeTruthy();
  }, 10000);

  
});