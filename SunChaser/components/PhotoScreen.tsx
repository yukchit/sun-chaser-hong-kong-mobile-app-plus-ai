import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  TextInput,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  PermissionsAndroid,
  Platform,
  TouchableOpacity as TouchableOpacityRN,
  Alert,
  Dimensions,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {
  TouchableHighlight,
  ScrollView,
  TouchableOpacity as TouchableOpacityRNGH,
} from 'react-native-gesture-handler';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import ImagePicker from 'react-native-image-crop-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
//@ts-ignore
import RadioForm from 'react-native-simple-radio-button';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {point, polygon, booleanPointInPolygon} from '@turf/turf';
import {addPhotosThunk} from '../redux/photos/thunks';

const hkGeoJson = require('../hksar_18_district_boundary.json');

const radio_props = [
  {label: 'Sunrise', value: 'sunrise'},
  {label: 'Sunset', value: 'sunset'},
];

interface ILocation {
  latitude: number;
  longitude: number;
}

export default function PhotoScreen() {
  const bottomSheet = useRef<BottomSheet>();
  const fall = new Animated.Value(1);
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();

  const showDateTimePicker = () => {
    setDateTimePickerVisibility(true);
  };

  const hideDateTimePicker = () => {
    setDateTimePickerVisibility(false);
  };

  const handleConfirm = (date: Date) => {
    hideDateTimePicker();
    setDateTime(date);
    setShowingDateTime(date);
  };

  const selectPhotoMoment = (value: string) => {
    setPhotoMoment(value);
  };

  const [mapDistrict, setMapDistrict] = useState('');
  const [photoMoment, setPhotoMoment] = useState('sunrise');
  const [dateTime, setDateTime] = useState(new Date());
  const [showingDateTime, setShowingDateTime] = useState(new Date());
  const [imageFile, setImage] = useState(
    'https://sunchaserhk.pro/icons/add-photo-130.png',
  );
  const [isDateTimePickerVisible, setDateTimePickerVisibility] = useState(
    false,
  );

  const [location, setLocation] = useState<ILocation | undefined>(undefined);

  const [mapLatitude, setMapLatitude] = useState(22.37206);

  const [mapLongitude, setMapLongitude] = useState(114.10789);

  const [mapRegion, setMapRegion] = useState({
    latitude: mapLatitude,
    longitude: mapLongitude,
    latitudeDelta: 0.0228,
    longitudeDelta: 0.017,
  });

  const [isMapMaximized, setIsMapMaximized] = useState(false);

  const [enlargeMap, isEnlargeMap] = useState({height: 150});

  const maxMinMap = () => {
    setIsMapMaximized(!isMapMaximized);
    if (isMapMaximized) {
      isEnlargeMap(styles.fullScreen);
    } else if (!isMapMaximized) {
      isEnlargeMap({height: 150});
    }
  };

  var onChangeValue = (value: {
    latitude: number;
    longitude: number;
    latitudeDelta: number;
    longitudeDelta: number;
  }) => {
    setMapRegion(value);

    if (turfResult01CentralAndWestern === true) {
      setMapDistrict('Central And Western');
    } else if (turfResult02Eastern === true) {
      setMapDistrict('Eastern');
    } else if (turfResult03WanChai === true) {
      setMapDistrict('Wan Chai');
    } else if (turfResult04Southern === true) {
      setMapDistrict('Southern');
    } else if (turfResult05YauTsimMong === true) {
      setMapDistrict('Yau Tsim Mong');
    } else if (turfResult06ShamShuiPo === true) {
      setMapDistrict('Sham Shui Po');
    } else if (turfResult07KowloonCity === true) {
      setMapDistrict('Kowloon City');
    } else if (turfResult08WongTaiSin === true) {
      setMapDistrict('Wong Tai Sin');
    } else if (turfResult09KwunTong === true) {
      setMapDistrict('Kwun Tong');
    } else if (turfResult10KwaiTsing === true) {
      setMapDistrict('Kwai Tsing');
    } else if (turfResult11TsuenWan === true) {
      setMapDistrict('Tsuen Wan');
    } else if (turfResult12TuenMun === true) {
      setMapDistrict('Tuen Mun');
    } else if (turfResult13YuenLong === true) {
      setMapDistrict('Yuen Long');
    } else if (turfResult14North === true) {
      setMapDistrict('North');
    } else if (turfResult15TaiPo === true) {
      setMapDistrict('Tai Po');
    } else if (turfResult16ShaTin === true) {
      setMapDistrict('Sha Tin');
    } else if (turfResult17SaiKung === true) {
      setMapDistrict('Sai Kung');
    } else if (turfResult18Islands === true) {
      setMapDistrict('Islands');
    } else {
      setMapDistrict("Please put the map's pin in Hong Kong's Area");
    }
  };

  const pinnedPt = point([mapRegion.longitude, mapRegion.latitude]);

  const poly01CentralAndWestern = polygon(
    hkGeoJson.features[0].geometry.coordinates,
  );
  const poly02Eastern = polygon(hkGeoJson.features[2].geometry.coordinates);
  const poly03WanChai = polygon(hkGeoJson.features[1].geometry.coordinates);
  const poly04Southern = polygon(hkGeoJson.features[3].geometry.coordinates);
  const poly05YauTsimMong = polygon(hkGeoJson.features[4].geometry.coordinates);
  const poly06ShamShuiPo = polygon(hkGeoJson.features[5].geometry.coordinates);
  const poly07KowloonCity = polygon(hkGeoJson.features[6].geometry.coordinates);
  const poly08WongTaiSin = polygon(hkGeoJson.features[7].geometry.coordinates);
  const poly09KwunTong = polygon(hkGeoJson.features[8].geometry.coordinates);
  const poly10KwaiTsing = polygon(hkGeoJson.features[16].geometry.coordinates);
  const poly11TsuenWan = polygon(hkGeoJson.features[9].geometry.coordinates);
  const poly12TuenMun = polygon(hkGeoJson.features[10].geometry.coordinates);
  const poly13YuenLong = polygon(hkGeoJson.features[11].geometry.coordinates);
  const poly14North = polygon(hkGeoJson.features[12].geometry.coordinates);
  const poly15TaiPo = polygon(hkGeoJson.features[13].geometry.coordinates);
  const poly16ShaTin = polygon(hkGeoJson.features[15].geometry.coordinates);
  const poly17SaiKung = polygon(hkGeoJson.features[14].geometry.coordinates);
  const poly18Islands = polygon(hkGeoJson.features[17].geometry.coordinates);

  const turfResult01CentralAndWestern = booleanPointInPolygon(
    pinnedPt,
    poly01CentralAndWestern,
  );
  const turfResult02Eastern = booleanPointInPolygon(pinnedPt, poly02Eastern);
  const turfResult03WanChai = booleanPointInPolygon(pinnedPt, poly03WanChai);
  const turfResult04Southern = booleanPointInPolygon(pinnedPt, poly04Southern);
  const turfResult05YauTsimMong = booleanPointInPolygon(
    pinnedPt,
    poly05YauTsimMong,
  );
  const turfResult06ShamShuiPo = booleanPointInPolygon(
    pinnedPt,
    poly06ShamShuiPo,
  );
  const turfResult07KowloonCity = booleanPointInPolygon(
    pinnedPt,
    poly07KowloonCity,
  );
  const turfResult08WongTaiSin = booleanPointInPolygon(
    pinnedPt,
    poly08WongTaiSin,
  );
  const turfResult09KwunTong = booleanPointInPolygon(pinnedPt, poly09KwunTong);
  const turfResult10KwaiTsing = booleanPointInPolygon(
    pinnedPt,
    poly10KwaiTsing,
  );
  const turfResult11TsuenWan = booleanPointInPolygon(pinnedPt, poly11TsuenWan);
  const turfResult12TuenMun = booleanPointInPolygon(pinnedPt, poly12TuenMun);
  const turfResult13YuenLong = booleanPointInPolygon(pinnedPt, poly13YuenLong);
  const turfResult14North = booleanPointInPolygon(pinnedPt, poly14North);
  const turfResult15TaiPo = booleanPointInPolygon(pinnedPt, poly15TaiPo);
  const turfResult16ShaTin = booleanPointInPolygon(pinnedPt, poly16ShaTin);
  const turfResult17SaiKung = booleanPointInPolygon(pinnedPt, poly17SaiKung);
  const turfResult18Islands = booleanPointInPolygon(pinnedPt, poly18Islands);

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'SUN-CHASER HONG KONG required Location permission',
          message:
            'We required Location permission in order to get device location ' +
            'Please grant us.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Alert.alert("You've access for the location");
        Geolocation.getCurrentPosition(
          (position) => {
            const {latitude, longitude} = position.coords;
            setLocation({
              latitude,
              longitude,
            });
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {enableHighAccuracy: true},
        );
      } else {
        console.log('Location permission denied');
        Alert.alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization('always');
      Geolocation.getCurrentPosition(
        (position) => {
          const {latitude, longitude} = position.coords;
          console.log('mapScreen lat lon', latitude, longitude);
          setLocation({
            latitude,
            longitude,
          });
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true},
      );
    }
    if (Platform.OS === 'android') {
      const onScreenLoad = () => {
        requestLocationPermission();
      };
      onScreenLoad();
    }
  }, []);

  const requestCameraPermission = async () => {
    try {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA &&
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'SUN-CHASER HONG KONG required Camera Permission',
            message:
              'SUN-CHASER HONG KONG needs access to your camera ' +
              'so you can take awesome pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
          let image = {} as Image;
          ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: false,
            mediaType: 'photo',
            compressImageMaxWidth: 640,
            compressImageMaxHeight: 640,
            freeStyleCropEnabled: true,
            multiple: false,
            // includeBase64: true,
            avoidEmptySpaceAroundImage: true,
            writeTempFile: false,
            // includeExif: true,
            cropperActiveWidgetColor: '#4242426e',
          })
            .then((image) => {
              console.log(image);
              bottomSheet.current?.snapTo(1);
              // storeUploadedData(item, image);
              //@ts-ignore
              setImage(image.path);
            })
            .catch((err) => {
              console.log('openCamera catch' + err.toString());
            });
        } else {
          console.log('Camera permission denied');
        }
      }
      if (Platform.OS === 'ios') {
        let image = {} as Image;
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
          mediaType: 'photo',
          compressImageMaxWidth: 640,
          compressImageMaxHeight: 640,
          freeStyleCropEnabled: true,
          multiple: false,
          // includeBase64: true,
          avoidEmptySpaceAroundImage: true,
          writeTempFile: false,
          // includeExif: true,
          cropperActiveWidgetColor: '#4242426e',
        })
          .then((image) => {
            console.log(image);
            bottomSheet.current?.snapTo(1);
            // storeUploadedData(item, image);
            //@ts-ignore
            setImage(image.path);
          })
          .catch((err) => {
            console.log('openCamera catch' + err.toString());
          });
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const requestLibraryPermission = async () => {
    try {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title:
              'SUN-CHASER HONG KONG required READ_EXTERNAL_STORAGE Permission',
            message:
              'SUN-CHASER HONG KONG needs access to your picture library ' +
              'so you can choose your awesome picture.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
          ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: false,
            mediaType: 'photo',
            compressImageMaxWidth: 640,
            compressImageMaxHeight: 640,
            freeStyleCropEnabled: true,
            multiple: false,
            // includeBase64: true,
            avoidEmptySpaceAroundImage: true,
            // path: image,
            writeTempFile: false,
            // includeExif: true,
          })
            .then((image) => {
              console.log(image);
              bottomSheet.current?.snapTo(1);
              //@ts-ignore
              setImage(image.path);
            })
            .catch((err) => {
              console.log('openCamera catch' + err.toString());
            });
        } else {
          console.log('READ_EXTERNAL_STORAGE denied');
        }
      }
      if (Platform.OS === 'ios') {
        console.log('IOS');
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
          mediaType: 'photo',
          compressImageMaxWidth: 640,
          compressImageMaxHeight: 640,
          freeStyleCropEnabled: true,
          multiple: false,
          // includeBase64: true,
          avoidEmptySpaceAroundImage: true,
          // path: image,
          writeTempFile: false,
          // includeExif: true,
        })
          .then((image) => {
            console.log(image);
            bottomSheet.current?.snapTo(1);
            //@ts-ignore
            setImage(image.path);
          })
          .catch((err) => {
            console.log('openCamera catch' + err.toString());
          });
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const renderHeader = () => {
    return (
      <View style={styles.BSHeader}>
        {(Platform.OS === 'ios' && (
          <TouchableOpacityRN style={styles.iconContainer}>
            <Image
              style={{width: 95, height: 25, marginBottom: 5}}
              source={require('../icons/horizontal-line-96-white.png')}
            />
          </TouchableOpacityRN>
        )) ||
          (Platform.OS === 'android' && (
            <TouchableOpacityRNGH style={styles.iconContainer}>
              <Image
                style={{width: 95, height: 25, marginBottom: 5}}
                source={require('../icons/horizontal-line-96-white.png')}
              />
            </TouchableOpacityRNGH>
          ))}
      </View>
    );
  };

  const renderContent = () => {
    return (
      <View style={styles.BSContent}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.BSTitle}>Upload Photo</Text>
          <Text style={styles.BSSubTitle}>
            Choose Your Sunrise/Sunset Picture
          </Text>
        </View>
        <View style={styles.BSButtonContainer}>
          {(Platform.OS === 'ios' && (
            <TouchableOpacityRN
              style={styles.BSButton}
              onPress={requestCameraPermission}>
              <Text style={styles.BSButtonTitle}>Take Photo</Text>
            </TouchableOpacityRN>
          )) ||
            (Platform.OS === 'android' && (
              <TouchableOpacityRNGH
                style={styles.BSButton}
                onPress={requestCameraPermission}>
                <Text style={styles.BSButtonTitle}>Take Photo</Text>
              </TouchableOpacityRNGH>
            ))}
        </View>
        <View style={styles.BSButtonContainer}>
          {(Platform.OS === 'ios' && (
            <TouchableOpacityRN
              style={styles.BSButton}
              onPress={requestLibraryPermission}>
              <Text style={styles.BSButtonTitle}>Choose From Library</Text>
            </TouchableOpacityRN>
          )) ||
            (Platform.OS === 'android' && (
              <TouchableOpacityRNGH
                style={styles.BSButton}
                onPress={requestLibraryPermission}>
                <Text style={styles.BSButtonTitle}>Choose From Library</Text>
              </TouchableOpacityRNGH>
            ))}
        </View>
        <View style={styles.BSButtonContainer}>
          {(Platform.OS === 'ios' && (
            <TouchableOpacityRN
              style={styles.BSButton}
              onPress={() => {
                reset(defaultValues);
                navigation.goBack();
              }}>
              <Text style={styles.BSButtonTitle}>Cancel + Go Back</Text>
            </TouchableOpacityRN>
          )) ||
            (Platform.OS === 'android' && (
              <TouchableOpacityRNGH
                style={styles.BSButton}
                onPress={() => {
                  reset(defaultValues);
                  navigation.goBack();
                }}>
                <Text style={styles.BSButtonTitle}>Cancel + Go Back</Text>
              </TouchableOpacityRNGH>
            ))}
        </View>
      </View>
    );
  };

  type HookFormData = {
    image: string;
    title: string;
    description: string;
    created_at: string;
    environment: string;
    latitude: string;
    longitude: string;
    district: string;
    location: string;
  };

  const defaultValues = {
    title: '',
    description: '',
    location: '',
  };

  const {control, handleSubmit, errors, setError, register, reset} = useForm<
    HookFormData
  >();

  const onSubmit = ({title, description, location}: HookFormData) => {
    dispatch(
      addPhotosThunk(
        imageFile,
        title,
        description,
        dateTime.toISOString(),
        photoMoment,
        mapRegion.latitude,
        mapRegion.longitude,
        mapDistrict,
        location,
      ),
    );
    reset(defaultValues);
    setImage('https://sunchaserhk.pro/icons/add-photo-130.png');
    navigation.goBack();
  };

  return (
    <>
      <View style={styles.body}>
        <ImageBackground
          style={styles.background}
          source={require('../images/backgrounds/sunrise_backup_2.jpg')}>
          <View style={styles.container}>
            <BottomSheet
              ref={bottomSheet as any}
              snapPoints={['45%', 0]}
              renderContent={renderContent}
              renderHeader={renderHeader}
              callbackNode={fall}
              initialSnap={0}
            />
            <Animated.View
              style={{
                flex: 1,
                opacity: Animated.add(0.3, Animated.multiply(fall, 0.9)),
              }}>
              <ScrollView style={{flex: 1}}>
                <TouchableOpacityRN
                  onPress={() => {
                    bottomSheet.current?.snapTo(0);
                  }}
                  style={{alignItems: 'center', marginTop: 50}}>
                  <View
                    style={{
                      height: 100,
                      width: 100,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ImageBackground
                      source={{
                        uri: imageFile,
                      }}
                      style={{height: 100, width: 100}}
                      imageStyle={{borderRadius: 15}}></ImageBackground>
                  </View>
                </TouchableOpacityRN>
                <KeyboardAvoidingView style={styles.formContainer}>
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.actualInput}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        editable={false}
                        value={(value = imageFile)}
                      />
                    )}
                    name="image"
                    rules={{
                      required: true,
                    }}
                    defaultValue={imageFile}
                  />
                  {errors.created_at?.type === 'required' && (
                    <Text style={styles.errorMsg}>Image File is required.</Text>
                  )}

                  <Text style={styles.label}>Title:</Text>
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        value={value}
                        placeholder={'Enter Title'}
                        keyboardAppearance={'dark'}
                        ref={register}
                      />
                    )}
                    name="title"
                    rules={{
                      required: true,
                    }}
                    defaultValue=""
                  />
                  {errors.title?.type === 'required' && (
                    <Text style={styles.errorMsg}>Title is required.</Text>
                  )}

                  <Text style={styles.label}>Description: (Optional)</Text>
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        value={value}
                        placeholder={'Enter description'}
                        keyboardAppearance={'dark'}
                        ref={register}
                      />
                    )}
                    name="description"
                    rules={{required: false}}
                    defaultValue=""
                  />

                  <Text style={styles.label}>
                    Photo Time: (Press to change the date and time)
                  </Text>
                  <View style={styles.BSButtonContainer}>
                    <TouchableOpacityRN
                      style={styles.DateTimeButton}
                      onPress={showDateTimePicker}>
                      <Text style={styles.DateTimeButtonTitle}>
                        {showingDateTime.toString()}
                      </Text>
                    </TouchableOpacityRN>
                    <DateTimePickerModal
                      isVisible={isDateTimePickerVisible}
                      mode="datetime"
                      onConfirm={handleConfirm}
                      onCancel={hideDateTimePicker}
                      maximumDate={new Date()}
                    />
                  </View>
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.actualInput}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        editable={false}
                        value={(value = dateTime.toISOString())}
                      />
                    )}
                    name="created_at"
                    rules={{
                      required: true,
                    }}
                    defaultValue={dateTime.toISOString()}
                  />
                  {errors.created_at?.type === 'required' && (
                    <Text style={styles.errorMsg}>Photo Time is required.</Text>
                  )}

                  <Text style={styles.label}>Moment:</Text>
                  <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#FFFFFF'}
                    labelColor={'#FFFFFF'}
                    animation={true}
                    radioStyle={{
                      paddingRight: 20,
                      marginTop: 5,
                      color: '#000000',
                    }}
                    labelStyle={{
                      marginRight: 10,
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                      fontSize: 13,
                    }}
                    selectedButtonColor={'#ffcc6c'}
                    // selectedLabelColor={'#ffcc6c'}
                    //@ts-ignore
                    onPress={selectPhotoMoment}
                  />
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.actualInput}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        editable={false}
                        value={(value = photoMoment)}
                      />
                    )}
                    name="environment"
                    rules={{
                      required: true,
                    }}
                    defaultValue="sunrise"
                  />
                  {errors.environment?.type === 'required' && (
                    <Text style={styles.errorMsg}>
                      Photo Moment is required.
                    </Text>
                  )}

                  <Text style={styles.label}>
                    Location: (Please move the pin to the location)
                  </Text>
                  <View style={styles.mapContainer}>
                    {/* <TouchableOpacityRN style={mapSize} onPress={maximizeMapSize}> */}
                    {location && (
                      <MapView
                        style={enlargeMap}
                        provider={PROVIDER_GOOGLE}
                        initialRegion={{
                          latitude: location.latitude,
                          longitude: location.longitude,
                          latitudeDelta: 0.0228,
                          longitudeDelta: 0.017,
                        }}
                        region={mapRegion}
                        onRegionChangeComplete={onChangeValue}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        followsUserLocation={true}
                        showsCompass={true}
                        customMapStyle={[
                          {
                            elementType: 'geometry',
                            stylers: [{color: '#242f3e'}],
                          },
                          {
                            elementType: 'labels.text.stroke',
                            stylers: [{color: '#242f3e'}],
                          },
                          {
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#746855'}],
                          },
                          {
                            featureType: 'administrative.locality',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}],
                          },
                          {
                            featureType: 'poi',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}],
                          },
                          {
                            featureType: 'poi.park',
                            elementType: 'geometry',
                            stylers: [{color: '#263c3f'}],
                          },
                          {
                            featureType: 'poi.park',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#6b9a76'}],
                          },
                          {
                            featureType: 'road',
                            elementType: 'geometry',
                            stylers: [{color: '#38414e'}],
                          },
                          {
                            featureType: 'road',
                            elementType: 'geometry.stroke',
                            stylers: [{color: '#212a37'}],
                          },
                          {
                            featureType: 'road',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#9ca5b3'}],
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'geometry',
                            stylers: [{color: '#746855'}],
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'geometry.stroke',
                            stylers: [{color: '#1f2835'}],
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#f3d19c'}],
                          },
                          {
                            featureType: 'transit',
                            elementType: 'geometry',
                            stylers: [{color: '#2f3948'}],
                          },
                          {
                            featureType: 'transit.station',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}],
                          },
                          {
                            featureType: 'water',
                            elementType: 'geometry',
                            stylers: [{color: '#17263c'}],
                          },
                          {
                            featureType: 'water',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#515c6d'}],
                          },
                          {
                            featureType: 'water',
                            elementType: 'labels.text.stroke',
                            stylers: [{color: '#17263c'}],
                          },
                          {
                            featureType: 'poi.medical',
                            stylers: [
                              {
                                visibility: 'off',
                              },
                            ],
                          },
                          {
                            featureType: 'poi.business',
                            stylers: [
                              {
                                visibility: 'off',
                              },
                            ],
                          },
                          {
                            featureType: 'poi.place_of_worship',
                            stylers: [
                              {
                                visibility: 'off',
                              },
                            ],
                          },
                          {
                            featureType: 'poi.school',
                            stylers: [
                              {
                                visibility: 'off',
                              },
                            ],
                          },
                        ]}>
                        {/* <Geojson
                            geojson={require('../hksar_18_district_boundary.json')}
                            strokeColor="#b57f1b65"
                            strokeWidth={2}
                            fillColor="#16161675"
                          /> */}
                      </MapView>
                    )}
                    {/* </TouchableOpacityRN> */}
                    <View
                      style={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        marginLeft: -24,
                        marginTop: -48,
                      }}>
                      <Image
                        style={{height: 48, width: 48}}
                        source={require('../icons/map-pin-96.png')}
                      />
                    </View>
                    {!isMapMaximized ? (
                      <TouchableOpacityRN
                        style={styles.maxMinButton}
                        onPress={maxMinMap}>
                        <Image
                          style={{height: '80%', width: '80%'}}
                          source={require('../icons/collapse-50.png')}
                        />
                      </TouchableOpacityRN>
                    ) : (
                      <TouchableOpacityRN
                        style={styles.maxMinButton}
                        onPress={maxMinMap}>
                        <Image
                          style={{height: '80%', width: '80%'}}
                          source={require('../icons/expand-50.png')}
                        />
                      </TouchableOpacityRN>
                    )}
                  </View>
                  <View style={styles.locationInputsContainer}>
                    <View style={styles.latLongInputsContainer}>
                      <Text style={styles.latlongInputsTitle}>Latitude:</Text>
                      <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                          <TextInput
                            style={styles.latLongDisInput}
                            onBlur={onBlur}
                            onChangeText={(value) => onChange(value)}
                            editable={false}
                            value={(value = mapRegion.latitude.toString())}
                          />
                        )}
                        name="latitude"
                        rules={{
                          required: true,
                        }}
                        defaultValue={mapRegion.latitude}
                      />
                      {errors.latitude?.type === 'required' && (
                        <Text style={styles.errorMsg}>
                          Location's latutude and longitude are required.
                        </Text>
                      )}
                    </View>
                    <View style={styles.latLongInputsContainer}>
                      <Text style={styles.latlongInputsTitle}>Longitude:</Text>
                      <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                          <TextInput
                            style={styles.latLongDisInput}
                            onBlur={onBlur}
                            onChangeText={(value) => onChange(value)}
                            editable={false}
                            value={(value = mapRegion.longitude.toString())}
                          />
                        )}
                        name="longitude"
                        rules={{
                          required: true,
                        }}
                        defaultValue={mapRegion.longitude}
                      />
                      {errors.longitude?.type === 'required' && (
                        <Text style={styles.errorMsg}>
                          Location's latutude and longitude are required.
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={styles.locationInputsContainer}>
                    <View style={styles.latLongInputsContainer}>
                      <Text style={styles.latlongInputsTitle}>District:</Text>
                      <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                          <TextInput
                            style={styles.latLongDisInput}
                            onBlur={onBlur}
                            onChangeText={(value) => onChange(value)}
                            editable={false}
                            value={(value = mapDistrict)}
                          />
                        )}
                        name="district"
                        rules={{
                          required: true,
                        }}
                        defaultValue="Please put the Map's pin in Hong Kong's area"
                      />
                    </View>
                  </View>
                  {errors.district?.type === 'required' && (
                    <Text style={styles.errorMsg}>
                      Please put the Map's pin in Hong Kong's area
                    </Text>
                  )}

                  <Text style={styles.label}>
                    Location in Details: (Optional)
                  </Text>
                  <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                      <TextInput
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={(value) => onChange(value)}
                        value={value}
                        placeholder={'Enter Location in Details'}
                        keyboardAppearance={'dark'}
                      />
                    )}
                    name="location"
                    rules={{required: false}}
                    defaultValue=""
                  />

                  <View style={styles.loginButtonContainer}>
                    <TouchableHighlight
                      style={styles.loginButton}
                      underlayColor="#0fa9bdc5"
                      onPress={handleSubmit(onSubmit)}>
                      <Text style={styles.loginButtonText}>SUBMIT</Text>
                    </TouchableHighlight>
                  </View>
                  {errors.environment?.type === 'required' && (
                    <Text style={styles.errorMsg}>
                      Photo Moment is required.
                    </Text>
                  )}

                  <View style={styles.horizontalLineContainer}>
                    <Image
                      style={styles.horizontalLine}
                      source={require('../icons/horizontal-line-96-white.png')}
                    />
                    <Text style={styles.or}>Or</Text>
                    <Image
                      style={styles.horizontalLine}
                      source={require('../icons/horizontal-line-96-white.png')}
                    />
                  </View>

                  <View style={styles.loginButtonContainer}>
                    <TouchableOpacityRN
                      style={styles.goBackButton}
                      onPress={() => {
                        navigation.goBack();
                      }}>
                      <Text style={styles.loginButtonText}>BACK</Text>
                    </TouchableOpacityRN>
                  </View>
                </KeyboardAvoidingView>
              </ScrollView>
            </Animated.View>
          </View>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },
  BSHeader: {
    backgroundColor: '#000000c0',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    marginBottom: 0,
    width: '100%',
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  BSContent: {
    marginTop: 0,
    flex: 1,
    backgroundColor: '#000000c0',
    color: 'white',
    width: '100%',
    marginBottom: 0,
    minHeight: 1000,
    alignItems: 'center',
  },
  BSTitle: {
    fontSize: 26,
    fontWeight: 'bold',
    color: 'white',
  },
  BSSubTitle: {
    fontSize: 13,
    fontWeight: '800',
    color: 'white',
    marginBottom: 10,
  },
  BSButtonContainer: {
    width: '90%',
  },
  BSButton: {
    backgroundColor: 'orange',
    height: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    borderRadius: 5,
  },
  BSButtonTitle: {
    color: 'white',
    fontSize: 20,
  },
  DateTimeButton: {
    backgroundColor: 'white',
    height: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    marginBottom: 0,
    borderRadius: 5,
  },
  DateTimeButtonTitle: {
    color: 'black',
    fontSize: 12,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },
  actualInput: {
    color: 'black',
    fontSize: 9,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
    display: 'none',
  },
  latLongDisInput: {
    color: 'black',
    fontSize: 9,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },
  mapContainer: {
    flex: 1,
    width: '95%',
    position: 'relative',
  },
  map: {
    height: 250,
  },
  locationInputsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '95%',
    backgroundColor: 'white',
  },
  latLongInputsContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  latlongInputsTitle: {
    color: 'black',
    fontSize: 9,
    marginRight: 0,
  },
  background: {
    flex: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 8,
    marginTop: 30,
    width: '100%',
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  logo: {
    width: 60,
    height: 60,
    marginBottom: 5,
    marginTop: 40,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  label: {
    marginTop: 5,
    marginBottom: 0,
    marginLeft: 20,
    color: 'white',
    fontSize: 13,
    fontWeight: 'bold',
    width: '95%',
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    padding: 10,
    borderRadius: 4,
    marginTop: 2,
    marginLeft: 20,
    marginRight: 20,
    width: '90%',
    color: 'black',
  },
  errorMsg: {
    fontSize: 10,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    fontWeight: 'bold',
    backgroundColor: '#ff00007a',
    borderRadius: 2,
    padding: 2,
    width: '90%',
  },
  loginButtonContainer: {
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    width: '90%',
  },
  loginButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1bb9ce',
    width: '100%',
    height: 38,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
  },
  loginButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    display: 'flex',
  },
  horizontalLineContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  horizontalLine: {
    width: '49%',
    height: 1,
  },
  or: {
    fontSize: 15,
    fontWeight: '600',
    color: 'white',
  },
  loginGoogleButtonContainer: {
    padding: 0,
    marginTop: 5,
    width: '90%',
  },
  goBackButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'orange',
    width: '100%',
    height: 38,
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.8,
    elevation: 8,
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 10},
    marginBottom: 50,
  },
  maxMinButton: {
    position: 'absolute',
    top: 10,
    left: '5%',
    backgroundColor: '#ffffffad',
    borderRadius: 2,
    height: 35,
    width: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fullScreen: {
    height: 0.65 * Dimensions.get('window').height,
    // width: Dimensions.get('window').width,
    alignSelf: 'stretch',
    width: '100%',
  },
});
