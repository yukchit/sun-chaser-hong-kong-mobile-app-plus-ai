import {CreateUserActions} from './actions';

export interface CreateUserState {
  success: boolean | null;
}

const initialState: CreateUserState = {
  success: null,
};

export function createUserReducer(
  state: CreateUserState = initialState,
  action: CreateUserActions,
): CreateUserState {
  switch (action.type) {
    case '@@user/CREATE_USER_SUCCESS':
      return {success: true};
    case '@@user/CREATE_USER_FAILED':
      return {success: false};
    case '@@user/CREATE_USER_RESET':
      return {success: null};
    default:
      return state;
  }
}
