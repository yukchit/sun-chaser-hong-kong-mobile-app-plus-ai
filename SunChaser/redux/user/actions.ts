import {ThunkDispatch, RootState} from '../../store';
import config from '../../config';

export function createUserSuccess() {
  return {
    type: '@@user/CREATE_USER_SUCCESS' as '@@user/CREATE_USER_SUCCESS',
  };
}

export function createUserFailed() {
  return {
    type: '@@user/CREATE_USER_FAILED' as '@@user/CREATE_USER_FAILED',
  };
}

export function createUserReset() {
  return {
    type: '@@user/CREATE_USER_RESET' as '@@user/CREATE_USER_RESET',
  };
}

export type CreateUserActions = ReturnType<
  typeof createUserSuccess | typeof createUserFailed | typeof createUserReset
>;

export function createUser(email: string, password: string, username: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${config.BACKEND_URL}/createUser`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
          username,
        }),
      });
      const json = await res.json();
      console.log('json in action', json);
      if (json.updated) {
        dispatch(createUserSuccess());
        dispatch(createUserReset());
      }
      if (json.message) {
        dispatch(createUserFailed());
        dispatch(createUserReset());
      }
    } catch (e) {
      console.error(e);
    }
  };
}
