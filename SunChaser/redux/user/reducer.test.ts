import {createUserReducer} from './reducer';
import { createUserSuccess, createUserFailed, createUserReset } from './actions';

describe('user reducer', () => {
    it('initial state', () => {
        const state = createUserReducer(undefined, {type: '@@INIT'} as any);

        expect(state).toEqual({
            success: null,
          });
    });

    it('should create user successfully', () => {
        const state = createUserReducer({
            success: null,
          }, createUserSuccess());

        expect(state).toEqual({success: true});
    });

    it('should fail to create user', () => {
        const state = createUserReducer({
            success: null,
          }, createUserFailed());

        expect(state).toEqual({success: false});
    });

    it('should reset the creating user process', () => {
        const state = createUserReducer({
            success: null,
          }, createUserReset());

        expect(state).toEqual({
            success: null,
          });
    });

})