import {updatePhotoStatus} from './actions';
import config from '../../config';
import {ThunkDispatch, RootState} from '../../store';

export function updatePhotoStatusThunk(id: number, status: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/statusChangedByAdmin`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id,
          status,
        }),
      },
    );
    const json = await res.json();
    console.log('JSON', json);
    dispatch(updatePhotoStatus(json));

    if (res.status == 400) {
      console.log(json.error);
    } else if (res.status == 500) {
      console.log('internal server error');
    }
  };
}
