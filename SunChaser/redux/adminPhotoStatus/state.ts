export interface PhotoStatusState {
  id: number;
  status: string;
}

export const initialState: PhotoStatusState = {
  id: 0,
  status: '',
};
