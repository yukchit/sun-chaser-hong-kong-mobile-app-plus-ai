export interface PhotoStatus {
  id: number;
  status: string;
}

export function updatePhotoStatus(photoStatus: PhotoStatus) {
  return {
    type: '@@photoStatus/UPDATE_PHOTO_STATUS' as '@@photoStatus/UPDATE_PHOTO_STATUS',
    photoStatus,
  };
}

export type PhotoStatusActions = ReturnType<typeof updatePhotoStatus>;
