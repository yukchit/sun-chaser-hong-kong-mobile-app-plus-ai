import {PhotoStatusActions} from './actions';
import {initialState, PhotoStatusState} from './state';

export const photoStatusReducer = (
  state: PhotoStatusState = initialState,
  action: PhotoStatusActions,
): PhotoStatusState => {
  switch (action.type) {
    case '@@photoStatus/UPDATE_PHOTO_STATUS':
      return {
        id: action.photoStatus.id,
        status: action.photoStatus.status,
      };

    default:
      return state;
  }
};
