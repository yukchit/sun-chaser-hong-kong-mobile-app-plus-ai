import {photoStatusReducer} from './reducers';
import { updatePhotoStatus } from './actions';

describe('photo status reducer', () => {
    it('intial state', () => {
        const state = photoStatusReducer(undefined, {type:'@@INIT'} as any);

        expect(state).toEqual({
            id: 0,
            status: '',
        })
    })

    it('update photo status successfully', () => {
        const state = photoStatusReducer({
            id: 2,
            status: 'pending',
        }, updatePhotoStatus({id: 2, status: 'shown'}));

        expect(state).toEqual({id: 2, status: 'shown'});
    })




})