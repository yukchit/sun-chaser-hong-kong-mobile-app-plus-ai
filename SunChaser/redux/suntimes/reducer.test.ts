import {suntimesReducer} from './reducers';
import { loadedSuntimes } from './actions';

describe('suntimes reducer', () => {
    it('initial state', () => {
        const state = suntimesReducer(undefined, {type: '@@INIT'} as any); 

        expect(state).toEqual({
            todayResult: {date: '', sunrise: '', sunset: ''},
            tomorrowResult: {date: '', sunrise: '', sunset: ''},
          });

    })

    it('should load suntimes correctly', () => {
        const state = suntimesReducer({
            todayResult: { date: '', sunrise: '', sunset: '' },
            tomorrowResult: { date: '', sunrise: '', sunset: '' },
        }, loadedSuntimes({
            todayResult: {
                date: '2020-02-02',
                sunrise: '05:45',
                sunset: '18:30'
            },
            tomorrowResult: {
                date: '2020-02-03',
                sunrise: '05:46',
                sunset: '18:31',
            }
        }));

        expect(state).toEqual(
            {
                "todayResult": {
                    "date": "2020-02-02",
                    "sunrise": "05:45",
                    "sunset": "18:30"
                },
                "tomorrowResult": {
                    "date": "2020-02-03",
                    "sunrise": "05:46",
                    "sunset": "18:31"
                }
            });
    });


})