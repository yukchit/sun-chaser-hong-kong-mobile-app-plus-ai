import config from '../../config';
import {ThunkDispatch} from '../../store';

export interface Suntime {
  todayResult: {date: string; sunrise: string; sunset: string};
  tomorrowResult: {
    date: string;
    sunrise: string;
    sunset: string;
  };
}

export function loadedSuntimes(suntimes: Suntime) {
  return {
    type: '@@suntimes/LOAD_SUNTIMES' as '@@suntimes/LOAD_SUNTIMES',
    suntimes: suntimes,
  };
}

export type SuntimesActions = ReturnType<typeof loadedSuntimes>;

// thunk actions

export function fetchSuntimes() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${config.BACKEND_URL}/suntimes`);
    const json = await res.json();
    dispatch(loadedSuntimes(json));
  };
}
