import {Suntime, SuntimesActions} from './actions';

export interface SuntimesState {
  todayResult: {date: string; sunrise: string; sunset: string};
  tomorrowResult: {
    date: string;
    sunrise: string;
    sunset: string;
  };
}

const initialState: SuntimesState = {
  todayResult: {date: '', sunrise: '', sunset: ''},
  tomorrowResult: {date: '', sunrise: '', sunset: ''},
};

export const suntimesReducer = (
  state: SuntimesState = initialState,
  action: SuntimesActions,
): SuntimesState => {
  switch (action.type) {
    case '@@suntimes/LOAD_SUNTIMES':
      return {
        ...state,
        todayResult: action.suntimes.todayResult,
        tomorrowResult: action.suntimes.tomorrowResult,
        // date: action.suntimes.date,
        // sunriseTime: action.suntimes.sunriseTime,
        // sunsetTime: action.suntimes.sunsetTime,
        // nextDate: action.suntimes.nextDate,
        // nextDaySunriseTime: action.suntimes.nextDaySunriseTime,
        // nextDaySunsetTime: action.suntimes.nextDaySunsetTime,
      };
    default:
      return state;
  }
};
