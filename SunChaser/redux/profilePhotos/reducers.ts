import {
  PhotosActions,
  PROFILE_LIKE_PHOTOS,
  LOAD_USER_PHOTOS,
  PROFILE_UNLIKE_PHOTOS,
  LOAD_PHOTOS_COMMENTS,
  ADD_COMMENTS,
} from '../photos/actions';
import {initialState, ProfilePhotoState} from './state';
import IPhoto, {IComment} from '../photos/state';

export function profilePhotoReducer(
  oldState: ProfilePhotoState = initialState,
  action: PhotosActions,
): ProfilePhotoState {
  switch (action.type) {
    case LOAD_USER_PHOTOS:
      const newUserPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      for (const photo of action.photos) {
        //@ts-ignore
        photo.liked = photo['liked'] ? true : false;
        newUserPhotoById[photo.id] = photo;
      }
      return {
        ...oldState,
        photoIds: action.photos.map((photo) => photo.id),
        photoById: newUserPhotoById,
      };
    case LOAD_PHOTOS_COMMENTS: {
      const newCommentsById: {
        [id: string]: IComment[];
      } = {...oldState.commentsById};
      newCommentsById[action.photoId] = action.comments;
      return {
        ...oldState,
        commentsById: newCommentsById,
      };
    }

    case ADD_COMMENTS:
      return {
        ...oldState,
        commentCount: oldState.commentCount + 1,
      };

    case PROFILE_LIKE_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      newPhotoById[action.photoId].liked = true;
      return {
        ...oldState,
        photoById: newPhotoById,
        likeCount: oldState.likeCount + 1,
      };
    }

    case PROFILE_UNLIKE_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      newPhotoById[action.photoId].liked = false;
      return {
        ...oldState,
        photoById: newPhotoById,
        likeCount: oldState.likeCount - 1,
      };
    }
    default:
      return oldState;
  }
}
