import IPhoto, {IComment} from '../photos/state';

export interface ProfilePhotoState {
  photoIds: number[];
  photoById: {
    [id: string]: IPhoto;
  };
  commentsById: {
    [id: string]: IComment[];
  };
  commentCount: number;
  likeCount: number;
  profilePhotoCount: number;
}

export const initialState: ProfilePhotoState = {
  photoIds: [],
  photoById: {},
  commentsById: {},
  commentCount: 0,
  likeCount: 0,
  profilePhotoCount: 0,
};
