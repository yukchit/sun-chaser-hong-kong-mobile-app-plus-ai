import {profilePhotoReducer} from './reducers';
import { loadUserPhotos, loadPhotosComments, addComment, likePhotosProfile, unLikePhotosProfile } from '../photos/actions';

describe('profile photos reducer', () => {
    it('initial state', () => {
        const state = profilePhotoReducer(undefined, { type: '@@INIT' } as any);

        expect(state).toEqual({
            photoIds: [],
            photoById: {},
            commentsById: {},
            commentCount: 0,
            likeCount: 0,
            profilePhotoCount: 0,
        })
    }
    );

    it("should load user's photos correctly", () => {
        const state = profilePhotoReducer({
            photoIds: [],
            photoById: {},
            commentsById: {},
            commentCount: 0,
            likeCount: 0,
            profilePhotoCount: 0,
        },loadUserPhotos(
            [{
                id: 3,
                image: '3.jpg',
                title: 'title_3',
                description: 'description_3',
                location: 'location_3',
                district: 'Eastern',
                latitude: 22.2606,
                longitude: 114.2499,
                created_at: "2019-12-19T10:13:54.000Z",
                username: 'user 3',
                status: 'shown',
                environment: 'sunset',
                updated_at: "2019-12-19T10:13:54.000Z",
                total_likes: '3',
                total_comments: '5',
                content: 'testing',
                liked: false,
                commented: 'testing',
                galleryPhotoCount: 2,
              },{
                id: 8,
                image: '8.jpg',
                title: 'title_8',
                description: 'description_8',
                location: 'location_8',
                district: 'Central And Western',
                latitude: 22.2863,
                longitude: 114.1321,
                created_at: "2019-11-19T10:13:54.000Z",
                username: 'user 3',
                status: 'shown',
                environment: 'sunset',
                updated_at: "2019-11-19T10:13:54.000Z",
                total_likes: '2',
                total_comments: '54',
                content: 'testing',
                liked: false,
                commented: 'testing',
                galleryPhotoCount: 2,
              }]
        ));

        expect(state.photoIds).toHaveLength(2);
        expect(state.photoIds).toEqual([3, 8]);
        expect(state.photoById[3]).toEqual(
            {"commented": "testing", 
            "content": "testing", 
            "created_at": "2019-12-19T10:13:54.000Z", 
            "description": "description_3", 
            "district": "Eastern", 
            "environment": "sunset", 
            "galleryPhotoCount": 2, 
            "id": 3, 
            "image": "3.jpg", 
            "latitude": 22.2606, 
            "liked": false, 
            "location": "location_3", 
            "longitude": 114.2499, 
            "status": "shown", 
            "title": "title_3", 
            "total_comments": "5", 
            "total_likes": "3", 
            "updated_at": "2019-12-19T10:13:54.000Z", 
            "username": "user 3"})
    });

    it("should load user's photos' comments correctly", () => {
        const state = profilePhotoReducer({
            photoIds: [],
            photoById: {},
            commentsById: {},
            commentCount: 0,
            likeCount: 0,
            profilePhotoCount: 0,
        }, loadPhotosComments(
            [{
                id: '8',
                content: 'content_testing',
                created_at: "2020-01-29T10:13:54.000Z",
                updated_at: "2020-01-29T10:13:54.000Z",
                username: 'user b',
                user_id: 2,
                photo_id: 5,
              } , {
                id: '12',
                content: 'content_testing',
                created_at: "2020-01-30T10:13:54.000Z",
                updated_at: "2020-01-30T10:13:54.000Z",
                username: 'user d',
                user_id: 4,
                photo_id: 5,
              }],
              5
        ));

        expect(state).toHaveProperty('commentsById');
        expect(state.commentsById[5]).toHaveLength(2);
    });

    it("should add photo's comment correctly", () => {
        const state = profilePhotoReducer({
            "commentCount": 0,
                "commentsById":
            {
                "8":
                [{
                    "content": "good",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "9",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User A"
                },
                {
                    "content": "great",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "20",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User B"
                }]
            },
            "profilePhotoCount": 0,
                "likeCount": 0,
                    "photoById": { },
            "photoIds": []
        }, addComment(8));

        expect(state.commentCount).toEqual(1);
    });

    it('should add like count correctly', () => {
        const state = profilePhotoReducer({
            "commentCount": 0,
            "commentsById":
            {
                "5":
                    [{
                        "content": "good",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "9",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User A"
                    },
                    {
                        "content": "great",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "20",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User B"
                    }]
            },
            "profilePhotoCount": 0,
            "likeCount": 0,
            "photoById": {
                "5": {
                    "commented": "testing",
                    "content": "testing",
                    "created_at": "2020-06-19T10:13:54.000Z",
                    "description": "description5",
                    "district": "Tai Po",
                    "environment": "sunset",
                    "galleryPhotoCount": 2,
                    "id": 5,
                    "image": "5.jpg",
                    "latitude": 22.4593,
                    "liked": false,
                    "location": "location5",
                    "longitude": 114.2026,
                    "status": "shown",
                    "title": "title5",
                    "total_comments": "5",
                    "total_likes": "5",
                    "updated_at": "2020-06-19T10:13:54.000Z",
                    "username": "user a"
                }
            },
            "photoIds": []
        }, likePhotosProfile(5));

        expect(state.likeCount).toEqual(1);
    });

    it('should unlike photo correctly', () => {
        const state = profilePhotoReducer({
            "commentCount": 0,
            "commentsById":
            {
                "5":
                    [{
                        "content": "good",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "9",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User A"
                    },
                    {
                        "content": "great",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "20",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User B"
                    }]
            },
            "profilePhotoCount": 0,
            "likeCount": 9,
            "photoById": {
                "5": {
                    "commented": "testing",
                    "content": "testing",
                    "created_at": "2020-06-19T10:13:54.000Z",
                    "description": "description5",
                    "district": "Tai Po",
                    "environment": "sunset",
                    "galleryPhotoCount": 2,
                    "id": 5,
                    "image": "5.jpg",
                    "latitude": 22.4593,
                    "liked": true,
                    "location": "location5",
                    "longitude": 114.2026,
                    "status": "shown",
                    "title": "title5",
                    "total_comments": "5",
                    "total_likes": "5",
                    "updated_at": "2020-06-19T10:13:54.000Z",
                    "username": "user a"
                }
            },
            "photoIds": []
        }, unLikePhotosProfile(5));

        expect(state.likeCount).toEqual(8);
    });

})