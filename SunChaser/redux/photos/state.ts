export default interface IPhoto {
  id: number;
  image: string;
  title: string;
  description: string;
  location: string;
  district: string;
  latitude: number;
  longitude: number;
  created_at: string;
  username: string;
  status: string;
  environment: string;
  updated_at: string;
  total_likes: string | null;
  total_comments: string | null;
  content: string;
  liked: boolean | null;
  commented: string | null;
  galleryPhotoCount: number;
}

export interface IComment {
  id: string;
  content: string;
  created_at: string;
  updated_at: string;
  username: string;
  user_id: number;
  photo_id: number;
}

export interface PhotoState {
  photoIds: number[];
  photoById: {
    [id: string]: IPhoto;
  };
  photoStatus: string;
  addPhotoCount: number;
}

export const initialState: PhotoState = {
  photoIds: [],
  photoById: {},
  photoStatus: 'shown',
  addPhotoCount: 0,
};
