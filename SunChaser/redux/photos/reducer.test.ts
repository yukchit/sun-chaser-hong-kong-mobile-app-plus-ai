import {photoReducer} from './reducers';
import { addPhotos } from './actions';

describe('photos reducer', () => {
    it('initial state', () => {
        const state = photoReducer(undefined, {types: '@@INIT'} as any);

        expect(state).toEqual({
            photoIds: [],
            photoById: {},
            photoStatus: 'shown',
            addPhotoCount: 0,
          });
    });

    it('should add photo correctly', () => {
        const state = photoReducer({
            photoIds: [],
            photoById: {},
            photoStatus: 'shown',
            addPhotoCount: 0,
          }, addPhotos({
            id: 27,
            image: '27.jpg',
            title: 'title_27',
            description: 'description_27',
            location: 'location_27',
            district: 'Wan Chai',
            latitude: 22.2790,
            longitude: 114.1775,
            created_at: "2020-01-19T10:13:54.000Z",
            username: 'user 27',
            status: 'shown',
            environment: 'sunset',
            updated_at: "2020-01-19T10:13:54.000Z",
            total_likes: '0',
            total_comments: '0',
            content: 'testing',
            liked: false,
            commented: 'testing',
            galleryPhotoCount: 1,
          }));

          expect(state).toEqual({"addPhotoCount": 1, "photoById": {}, "photoIds": [], "photoStatus": "shown"});
    });


    

})