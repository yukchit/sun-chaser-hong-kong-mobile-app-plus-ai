import {PhotosActions} from './actions';
import {initialState, PhotoState} from './state';
import {ADD_PHOTOS, DELETE_PHOTOS} from './actions';

export function photoReducer(
  oldState: PhotoState = initialState,
  action: PhotosActions,
): PhotoState {
  switch (action.type) {
    case ADD_PHOTOS:
      return {
        ...oldState,
        addPhotoCount: oldState.addPhotoCount + 1,
      };

    case DELETE_PHOTOS:
      return {
        ...oldState,
      };

    default:
      return oldState;
  }
}
