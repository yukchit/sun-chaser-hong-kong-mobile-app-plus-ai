import IPhoto, {IComment} from './state';

export const LOAD_PHOTOS = '@@photos/LOAD_PHOTOS';
export const LOAD_USER_PHOTOS = '@@photos/LOAD_USER_PHOTOS';
export const LOAD_DISTRICT_PHOTOS = '@@photos/LOAD_DISTRICT_PHOTOS';
export const ADD_PHOTOS = '@@photos/ADD_PHOTOS';
export const DELETE_PHOTOS = '@@photos/DELETE_PHOTOS';
export const ADD_COMMENTS = '@@photos/ADD_COMMENTS';
export const LOAD_PHOTOS_COMMENTS = '@@/photos/LOAD_PHOTOS_COMMENTS';
export const GALLERY_LIKE_PHOTOS = '@@galleryPhotos/LIKE_PHOTOS';
export const PROFILE_LIKE_PHOTOS = '@@profilePhotos/LIKE_PHOTOS';
export const GALLERY_UNLIKE_PHOTOS = '@@galleryPhotos/UNLIKE_PHOTOS';
export const PROFILE_UNLIKE_PHOTOS = '@@profilePhotos/UNLIKE_PHOTOS';
export const DISTRICTS_LIKE_PHOTOS = '@@districtsPhotos/LIKE_PHOTOS';
export const DISTRICTS_UNLIKE_PHOTOS = '@@districtsPhotos/UNLIKE_PHOTOS';

export function loadPhotos(
  photos: IPhoto[],
  current_page: number,
  total: number,
) {
  return {
    type: '@@photos/LOAD_PHOTOS' as '@@photos/LOAD_PHOTOS',
    photos,
    current_page,
    total,
  };
}

export function loadUserPhotos(photos: IPhoto[]) {
  return {
    type: '@@photos/LOAD_USER_PHOTOS' as '@@photos/LOAD_USER_PHOTOS',
    photos,
  };
}

export function loadDistrictPhotos(photos: IPhoto[]) {
  return {
    type: '@@photos/LOAD_DISTRICT_PHOTOS' as '@@photos/LOAD_DISTRICT_PHOTOS',
    photos,
  };
}

export function loadPhotosComments(comments: IComment[], photoId: number) {
  return {
    type: '@@/photos/LOAD_PHOTOS_COMMENTS' as '@@/photos/LOAD_PHOTOS_COMMENTS',
    comments,
    photoId,
  };
}

export function addPhotos(photo: IPhoto) {
  return {
    type: '@@photos/ADD_PHOTOS' as '@@photos/ADD_PHOTOS',
    photo,
  };
}

export function deletePhotos(photo: IPhoto) {
  return {
    type: '@@photos/DELETE_PHOTOS' as '@@photos/DELETE_PHOTOS',
    photo,
  };
}

export function addComment(photoId: number) {
  return {
    type: '@@photos/ADD_COMMENTS' as '@@photos/ADD_COMMENTS',
    photoId,
  };
}

export function likePhotosGallery(photoId: number) {
  return {
    type: '@@galleryPhotos/LIKE_PHOTOS' as '@@galleryPhotos/LIKE_PHOTOS',
    photoId,
  };
}

export function likePhotosProfile(photoId: number) {
  return {
    type: '@@profilePhotos/LIKE_PHOTOS' as '@@profilePhotos/LIKE_PHOTOS',
    photoId,
  };
}

export function unLikePhotosGallery(photoId: number) {
  return {
    type: '@@galleryPhotos/UNLIKE_PHOTOS' as '@@galleryPhotos/UNLIKE_PHOTOS',
    photoId,
  };
}

export function unLikePhotosProfile(photoId: number) {
  return {
    type: '@@profilePhotos/UNLIKE_PHOTOS' as '@@profilePhotos/UNLIKE_PHOTOS',
    photoId,
  };
}

export function likePhotosDistricts(photoId: number) {
  return {
    type: '@@districtsPhotos/LIKE_PHOTOS' as '@@districtsPhotos/LIKE_PHOTOS',
    photoId,
  };
}

export function unLikePhotosDistricts(photoId: number) {
  return {
    type: '@@districtsPhotos/UNLIKE_PHOTOS' as '@@districtsPhotos/UNLIKE_PHOTOS',
    photoId,
  };
}

export type PhotosActions = ReturnType<
  | typeof loadPhotos
  | typeof loadUserPhotos
  | typeof loadDistrictPhotos
  | typeof loadPhotosComments
  | typeof addPhotos
  | typeof deletePhotos
  | typeof likePhotosGallery
  | typeof likePhotosProfile
  | typeof unLikePhotosGallery
  | typeof unLikePhotosProfile
  | typeof addComment
  | typeof likePhotosDistricts
  | typeof unLikePhotosDistricts
>;
