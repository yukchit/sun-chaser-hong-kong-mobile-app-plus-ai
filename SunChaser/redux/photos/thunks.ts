import {
  loadPhotos,
  addPhotos,
  deletePhotos,
  loadUserPhotos,
  addComment,
  unLikePhotosGallery,
  loadPhotosComments,
  unLikePhotosProfile,
  likePhotosGallery,
  likePhotosProfile,
  loadDistrictPhotos,
} from './actions';
import {ThunkDispatch, RootState} from '../../store';
import config from '../../config';

export function loadPhotosThunk(current_page: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const resComments = await fetch(
      `${config.BACKEND_URL}/photos/totalComments`,
    );
    const comments = await resComments.json();

    const photos = [];

    if (!getState().auth.token) {
      const res = await fetch(`${config.BACKEND_URL}/photos`);
      const json = await res.json();
      for (let i = 0; i < comments.length; i++) {
        const obj = {...json.photos[i], ...comments[i]};
        photos.push(obj);
      }
      dispatch(loadPhotos(photos, current_page, json.total));
    } else {
      const res = await fetch(`${config.BACKEND_URL}/photos`, {
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      });
      const json = await res.json();
      for (let i = 0; i < comments.length; i++) {
        const obj = {...json.photos[i], ...comments[i]};
        photos.push(obj);
      }
      dispatch(loadPhotos(photos, current_page, json.total));
    }
  };
}

export function loadMyPhotosThunk() {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    if (getState().auth.token) {
      const res = await fetch(
        `${config.BACKEND_URL}/photos/user/${getState().auth.user?.id}`,
      );
      const json = await res.json();

      const resComments = await fetch(
        `${config.BACKEND_URL}/photos/myTotalComments/${
          getState().auth.user?.id
        }`,
      );
      const comments = await resComments.json();

      const photos = [];

      for (let i = 0; i < comments.length; i++) {
        const obj = {...json[i], ...comments[i]};
        photos.push(obj);
      }

      dispatch(loadUserPhotos(photos));
    }
  };
}

export function loadDistrictPhotosThunk(district: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const photos = [];
    const token = getState().auth.token;

    const resComments = await fetch(
      `${config.BACKEND_URL}/photos/districtTotalComments/${district}`,
    );
    const comments = await resComments.json();

    if (!token) {
      const res = await fetch(
        `${config.BACKEND_URL}/photos/district/${district}`,
      );
      const json = await res.json();

      console.log('photo json', json);

      for (let i = 0; i < comments.length; i++) {
        const obj = {...json[i], ...comments[i]};
        photos.push(obj);
      }
    } else if (token) {
      const res = await fetch(
        `${config.BACKEND_URL}/photos/district/${district}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      const json = await res.json();

      console.log('photo json', json);

      for (let i = 0; i < comments.length; i++) {
        const obj = {...json[i], ...comments[i]};
        photos.push(obj);
      }
    }
    dispatch(loadDistrictPhotos(photos));
  };
}

export function loadPhotosCommentsThunk(photoId: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(`${config.BACKEND_URL}/photos/comments/${photoId}`);
    const json = await res.json();

    dispatch(loadPhotosComments(json, photoId));
  };
}

export function addPhotosThunk(
  image: string,
  title: string,
  description: string,
  created_at: string,
  environment: string,
  latitude: number,
  longitude: number,
  district: string,
  location: string,
) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const formData = new FormData();

    formData.append('image', {uri: image, name: 'image', type: 'image/jpg'});
    formData.append('title', title);
    formData.append('description', description);
    formData.append('created_at', created_at);
    formData.append('environment', environment);
    formData.append('latitude', latitude);
    formData.append('longitude', longitude);
    formData.append('district', district);
    formData.append('location', location);

    console.log('FORM', formData);

    const res = await fetch(`${config.BACKEND_URL}/photos/addPhotoRN`, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${getState().auth.token}`,
      },
    });
    const json = await res.json();
    dispatch(addPhotos(json));

    if (res.status == 400) {
      console.log(json.error);
    } else if (res.status == 500) {
      console.log('internal server error');
    }
  };
}

export function deletePhotosThunk() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${config.BACKEND_URL}/photos/:id`);
    const json = res.json();

    dispatch(deletePhotos(await json));
  };
}

export function likePhotosGalleryThunk(photoId: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/like/${photoId}`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      },
    );
    if (res.status === 200) {
      dispatch(likePhotosGallery(photoId));
    } else {
      // ...
    }
  };
}

export function likePhotosProfileThunk(photoId: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/like/${photoId}`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      },
    );
    if (res.status === 200) {
      dispatch(likePhotosProfile(photoId));
    } else {
      // ...
    }
  };
}

export function addCommentThunk(photoId: number, content: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/comment/${photoId}`,
      {
        method: 'POST',
        body: JSON.stringify({
          content,
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${getState().auth.token}`,
        },
      },
    );
    const json = await res.json();
    dispatch(addComment(json));
    if (res.status == 400) {
      console.log(json.error);
    } else if (res.status == 500) {
      console.log('internal server error');
    }
  };
}

export function unLikePhotosGalleryThunk(photoId: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/unlike/${photoId}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      },
    );
    if (res.status === 200) {
      console.log('Photo Id in delete like thunk', photoId);
      dispatch(unLikePhotosGallery(photoId));
    } else {
      // ...
    }
  };
}

export function unLikePhotosProfileThunk(photoId: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/commentAndLike/RN/unlike/${photoId}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      },
    );
    if (res.status === 200) {
      console.log('Photo Id in delete like thunk', photoId);
      dispatch(unLikePhotosProfile(photoId));
    } else {
      // ...
    }
  };
}
