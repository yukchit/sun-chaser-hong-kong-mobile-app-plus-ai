import react from 'react';

// import { CommentAndLikesActions } from "./actions";
// import IComment, {
//   initialState,
//   CommentState,
//   commentInitialState,
//   LikedPhotoState,
//   ILikedPhoto,
// } from "./state";
// import {
//   LOAD_COMMENTS,
//   LOAD_LIKED_PHOTOS,
//   ADD_COMMENTS,
//   DELETE_COMMENTS,
//   DELETE_LIKED_PHOTOS 
// } from "./actions";

// export function likedPhotoReducer(
//   oldState: LikedPhotoState = initialState,
//   action: CommentAndLikesActions
// ): LikedPhotoState {
//   switch (action.type) {
//     case LOAD_LIKED_PHOTOS: {
//       const newLikedPhotoById: {
//         [id: string]: ILikedPhoto;
//       } = { ...oldState.likedPhotoById };
//       const newLikedPhotoIds = oldState.likedPhotoIds.slice();
//       let i = 0;
//       for (const likedPhoto of action.likedPhotos) {
//         newLikedPhotoById[likedPhoto.id] = likedPhoto;

//         i++;
//       }
//       return {
//         ...oldState,
//         likedPhotoIds: newLikedPhotoIds,
//         likedPhotoById: newLikedPhotoById,
//       };
//     }

//     case DELETE_LIKED_PHOTOS:
//       return {
//         ...oldState,
//         likedPhotoIds: [],
//         likedPhotoById: {},
//       };

//     default:
//       return oldState;
//   }
// }

// export function commentReducer(
//   commentOldState: CommentState = commentInitialState,
//   action: CommentAndLikesActions
// ): CommentState {
//   switch (action.type) {
//     case LOAD_COMMENTS: {
//       const newCommentById: {
//         [id: string]: IComment;
//       } = { ...commentOldState.commentById };
//       const newCommentIds = commentOldState.commentIds.slice();
//       let i = 0;
//       for (const comment of action.comments) {
//         newCommentById[comment.id] = comment;

//         i++;
//       }
//       return {
//         ...commentOldState,
//         commentIds: newCommentIds,
//         commentById: newCommentById,
//       };
//     }

//     case ADD_COMMENTS:
//       return {
//         ...commentOldState,
//         commentIds: [],
//         commentById: {},
//       };

//     case DELETE_COMMENTS:
//       return {
//         ...commentOldState,
//         commentIds: [],
//         commentById: {},
//       };

//     default:
//       return commentOldState;
//   }
// }
