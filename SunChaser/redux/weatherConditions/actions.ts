import config from '../../config';
import {ThunkDispatch} from '../../store';

export interface WeatherCondition {
    generalSituation: string;
    forecastPeriod: string;
    forecastDesc: string;
}

export function loadedWeatherConditions(weatherConditions: WeatherCondition) {
  return {
    type: '@@weatherConditions/LOAD_WEATHERCONDITIONS' as '@@weatherConditions/LOAD_WEATHERCONDITIONS',
    weatherConditions: weatherConditions,
  };
}

export type WeatherConditionsActions = ReturnType<typeof loadedWeatherConditions>;

// thunk actions

export function fetchWeatherConditions() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${config.BACKEND_URL}/weather`);
    const json = await res.json();

    dispatch(loadedWeatherConditions(json));
  };
}
