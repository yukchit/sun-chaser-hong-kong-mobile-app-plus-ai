import {WeatherCondition, WeatherConditionsActions} from './actions';

export interface WeatherConditionsState {
    generalSituation: string;
    forecastPeriod: string;
    forecastDesc: string;
}

const initialState: WeatherConditionsState = {
    generalSituation: "",
    forecastPeriod: "",
    forecastDesc: "",
};

export const weatherConditionsReducer = (
  state: WeatherConditionsState = initialState,
  action: WeatherConditionsActions,
): WeatherConditionsState => {
  switch (action.type) {
    case '@@weatherConditions/LOAD_WEATHERCONDITIONS':
      return {
          ...state,
          generalSituation: action.weatherConditions.generalSituation,
          forecastPeriod: action.weatherConditions.forecastPeriod,
          forecastDesc: action.weatherConditions.forecastDesc,
      }
  }
  return state;
};
