import {weatherConditionsReducer} from './reducers';
import { loadedWeatherConditions } from './actions';

describe("weather conditions' reducer", () => {
    it('initial state', () => {
        const state = weatherConditionsReducer(undefined, {type: '@@INIT'} as any);

        expect(state).toEqual({
            generalSituation: "",
            forecastPeriod: "",
            forecastDesc: "",
        });
    });

    it('should load weather conditions correctly', () => {
        const state = weatherConditionsReducer({
            generalSituation: "",
            forecastPeriod: "",
            forecastDesc: "",
        }, loadedWeatherConditions({
            generalSituation: 'testing_general_situation',
            forecastPeriod: 'testing_forecast_period',
            forecastDesc: 'testing_forecast_desc'
        }));

        expect(state).toEqual({
            generalSituation: 'testing_general_situation',
            forecastPeriod: 'testing_forecast_period',
            forecastDesc: 'testing_forecast_desc'
        });
    })


})