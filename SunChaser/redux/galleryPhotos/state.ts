import IPhoto, {IComment} from '../photos/state';

export interface GalleryPhotoState {
  photoIds: number[];
  photoById: {
    [id: string]: IPhoto;
  };
  commentsById: {
    [id: string]: IComment[];
  };
  commentCount: number;
  likeCount: number;
  galleryPhotoCount: number;
}

export const initialState: GalleryPhotoState = {
  photoIds: [],
  photoById: {},
  commentsById: {},
  commentCount: 0,
  likeCount: 0,
  galleryPhotoCount: 0,
};

export const perPage = 10;
