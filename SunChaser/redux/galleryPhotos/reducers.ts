import {
  PhotosActions,
  GALLERY_LIKE_PHOTOS,
  LOAD_PHOTOS,
  GALLERY_UNLIKE_PHOTOS,
  LOAD_PHOTOS_COMMENTS,
  ADD_COMMENTS,
} from '../photos/actions';
import {initialState, GalleryPhotoState, perPage} from './state';
import IPhoto, {IComment} from '../photos/state';

export function galleryPhotoReducer(
  oldState: GalleryPhotoState = initialState,
  action: PhotosActions,
): GalleryPhotoState {
  switch (action.type) {
    case LOAD_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      const newPhotoIds = oldState.photoIds.slice();
      let i = 0;
      for (const photo of action.photos) {
        //@ts-ignore
        photo.liked = photo['liked'] ? true : false;
        newPhotoById[photo.id] = photo;
        newPhotoIds[perPage * (action.current_page - 1) + i] = photo.id;
        i++;
      }
      return {
        ...oldState,
        photoIds: newPhotoIds,
        photoById: newPhotoById,
        galleryPhotoCount: action.total,
      };
    }

    case LOAD_PHOTOS_COMMENTS: {
      const newCommentsById: {
        [id: string]: IComment[];
      } = {...oldState.commentsById};
      newCommentsById[action.photoId] = action.comments;
      return {
        ...oldState,
        commentsById: newCommentsById,
      };
    }

    case ADD_COMMENTS:
      return {
        ...oldState,
        commentCount: oldState.commentCount + 1,
      };

    case GALLERY_LIKE_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      newPhotoById[action.photoId].liked = true;
      return {
        ...oldState,
        photoById: newPhotoById,
        likeCount: oldState.likeCount + 1,
      };
    }

    case GALLERY_UNLIKE_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      newPhotoById[action.photoId].liked = false;
      return {
        ...oldState,
        photoById: newPhotoById,
        likeCount: oldState.likeCount - 1,
      };
    }
    default:
      return oldState;
  }
}
