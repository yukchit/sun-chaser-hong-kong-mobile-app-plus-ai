import { galleryPhotoReducer } from './reducers';
import { loadPhotos, loadPhotosComments, addComment, likePhotosGallery, unLikePhotosGallery } from '../photos/actions';
import { exp } from 'react-native-reanimated';


describe('gallery photo reducer', () => {
    it('intial state', () => {
        const state = galleryPhotoReducer(undefined, { type: '@@INIT' } as any);

        expect(state).toEqual({
            photoIds: [],
            photoById: {},
            commentsById: {},
            commentCount: 0,
            likeCount: 0,
            galleryPhotoCount: 0,
        })
    });

    it('should load photos correctly', () => {
        const state = galleryPhotoReducer(undefined, loadPhotos(
            [{
                id: 5,
                image: '5.jpg',
                title: 'title5',
                description: 'description5',
                location: 'location5',
                district: 'Tai Po',
                latitude: 22.4593,
                longitude: 114.2026,
                created_at: "2020-06-19T10:13:54.000Z",
                username: 'user a',
                status: 'shown',
                environment: 'sunset',
                updated_at: "2020-06-19T10:13:54.000Z",
                total_likes: '5',
                total_comments: '5',
                content: 'testing',
                liked: true,
                commented: 'testing',
                galleryPhotoCount: 2,
            }, {
                id: 8,
                image: '8.jpg',
                title: 'title8',
                description: 'description8',
                location: 'location8',
                district: 'Sha Tin',
                latitude: 22.3930,
                longitude: 114.1995,
                created_at: "2020-05-19T10:13:54.000Z",
                username: 'user b',
                status: 'shown',
                environment: 'sunrise',
                updated_at: "2020-05-19T10:13:54.000Z",
                total_likes: '5',
                total_comments: '5',
                content: 'testing',
                liked: true,
                commented: 'testing',
                galleryPhotoCount: 2,
            }],
            1,
            1
        ));

        expect(state.commentCount).toEqual(0);
        expect(state).toHaveProperty("commentsById");
        expect(state.galleryPhotoCount).toEqual(1);
        expect(state.likeCount).toEqual(0);
        expect(state.photoIds).toEqual([5, 8]);
        expect(state.photoById[5]).toEqual(
            {
                "commented": "testing",
                "content": "testing",
                "created_at": "2020-06-19T10:13:54.000Z",
                "description": "description5",
                "district": "Tai Po",
                "environment": "sunset",
                "galleryPhotoCount": 2,
                "id": 5,
                "image": "5.jpg",
                "latitude": 22.4593,
                "liked": true,
                "location": "location5",
                "longitude": 114.2026,
                "status": "shown",
                "title": "title5",
                "total_comments": "5",
                "total_likes": "5",
                "updated_at": "2020-06-19T10:13:54.000Z",
                "username": "user a"
            }
        )
    });

    it("should load photos' comments correctly", () => {
        const state = galleryPhotoReducer(undefined, loadPhotosComments(
            [{
                id: '9',
                content: 'good',
                created_at: "2020-05-22T10:13:54.000Z",
                updated_at: "2020-05-22T10:13:54.000Z",
                username: "Testing User A",
                user_id: 12,
                photo_id: 50
            },
            {
                id: '20',
                content: 'great',
                created_at: "2020-05-22T10:13:54.000Z",
                updated_at: "2020-05-22T10:13:54.000Z",
                username: "Testing User B",
                user_id: 12,
                photo_id: 50
            },
            ],
            8
        ))

        expect(state.commentCount).toEqual(0);
        expect(state.galleryPhotoCount).toEqual(0);
        expect(state.likeCount).toEqual(0);
        expect(state.commentsById[8]).toEqual(
            expect.arrayContaining(
                [{
                    id: '20',
                    content: 'great',
                    created_at: "2020-05-22T10:13:54.000Z",
                    updated_at: "2020-05-22T10:13:54.000Z",
                    username: "Testing User B",
                    user_id: 12,
                    photo_id: 50
                }]
            ))
    });

    it('should add comment count correctly', () => {
        const state = galleryPhotoReducer({
            "commentCount": 0,
                "commentsById":
            {
                "8":
                [{
                    "content": "good",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "9",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User A"
                },
                {
                    "content": "great",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "20",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User B"
                }]
            },
            "galleryPhotoCount": 0,
                "likeCount": 0,
                    "photoById": { },
            "photoIds": []
        }, addComment(8));

        expect(state.commentCount).toEqual(1);
    });

    it('should add like count correctly', () => {
        const state = galleryPhotoReducer({
            "commentCount": 0,
            "commentsById":
            {
                "5":
                    [{
                        "content": "good",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "9",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User A"
                    },
                    {
                        "content": "great",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "20",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User B"
                    }]
            },
            "galleryPhotoCount": 0,
            "likeCount": 0,
            "photoById": {
                "5": {
                    "commented": "testing",
                    "content": "testing",
                    "created_at": "2020-06-19T10:13:54.000Z",
                    "description": "description5",
                    "district": "Tai Po",
                    "environment": "sunset",
                    "galleryPhotoCount": 2,
                    "id": 5,
                    "image": "5.jpg",
                    "latitude": 22.4593,
                    "liked": false,
                    "location": "location5",
                    "longitude": 114.2026,
                    "status": "shown",
                    "title": "title5",
                    "total_comments": "5",
                    "total_likes": "5",
                    "updated_at": "2020-06-19T10:13:54.000Z",
                    "username": "user a"
                }
            },
            "photoIds": []
        }, likePhotosGallery(5));

        expect(state.likeCount).toEqual(1);
    });

    it('should unlike photo correctly', () => {
        const state = galleryPhotoReducer({
            "commentCount": 0,
            "commentsById":
            {
                "5":
                    [{
                        "content": "good",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "9",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User A"
                    },
                    {
                        "content": "great",
                        "created_at": "2020-05-22T10:13:54.000Z",
                        "id": "20",
                        "photo_id": 50,
                        "updated_at": "2020-05-22T10:13:54.000Z",
                        "user_id": 12,
                        "username": "Testing User B"
                    }]
            },
            "galleryPhotoCount": 0,
            "likeCount": 9,
            "photoById": {
                "5": {
                    "commented": "testing",
                    "content": "testing",
                    "created_at": "2020-06-19T10:13:54.000Z",
                    "description": "description5",
                    "district": "Tai Po",
                    "environment": "sunset",
                    "galleryPhotoCount": 2,
                    "id": 5,
                    "image": "5.jpg",
                    "latitude": 22.4593,
                    "liked": true,
                    "location": "location5",
                    "longitude": 114.2026,
                    "status": "shown",
                    "title": "title5",
                    "total_comments": "5",
                    "total_likes": "5",
                    "updated_at": "2020-06-19T10:13:54.000Z",
                    "username": "user a"
                }
            },
            "photoIds": []
        }, unLikePhotosGallery(5));

        expect(state.likeCount).toEqual(8);
    });


})