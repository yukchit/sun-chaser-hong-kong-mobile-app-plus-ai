import {districtPhotoReducer} from './reducers';
import { loadDistrictPhotos, loadPhotosComments, addComment, likePhotosDistricts, unLikePhotosDistricts } from '../photos/actions';

describe('district photo reducer', () => {
    it('initial state', () => {
        const state = districtPhotoReducer(undefined, { type: '@@INIT' } as any);

        expect(state).toEqual({
            photoIds: [],
            photoById: {},
            commentsById: {},
            commentCount: 0,
            likeCount: 0,
            districtPhotoCount: 0,
        });
    });

    it('should load district photos successfully', () => {
        const state = districtPhotoReducer(undefined, loadDistrictPhotos([
            {
            id: 60,
            image: '60.jpg',
            title: '60_title',
            description: '60_description',
            location: 'Ha Pak Nai',
            district: 'Yuen Long',
            latitude: 22.4268,
            longitude: 113.939,
            created_at: "2020-03-19T10:13:54.000Z",
            username: 'user003',
            status: 'shown',
            environment: 'sunset',
            updated_at: "2020-03-19T10:13:54.000Z",
            total_likes: '3',
            total_comments: '5',
            content: 'testing_content',
            liked: true,
            commented: 'testing',
            galleryPhotoCount: 2,
          },{
            id: 66,
            image: '66.jpg',
            title: '66_title',
            description: '66_description',
            location: 'Ha Pak Nai',
            district: 'Yuen Long',
            latitude: 22.4268,
            longitude: 113.939,
            created_at: "2020-03-19T10:13:54.000Z",
            username: 'user005',
            status: 'shown',
            environment: 'sunset',
            updated_at: "2020-03-19T10:13:54.000Z",
            total_likes: '5',
            total_comments: '7',
            content: 'testing_content',
            liked: true,
            commented: 'testing',
            galleryPhotoCount: 2,
          },{
            id: 88,
            image: '88.jpg',
            title: '88_title',
            description: '88_description',
            location: 'Ha Pak Nai',
            district: 'Yuen Long',
            latitude: 22.4268,
            longitude: 113.939,
            created_at: "2020-03-19T10:13:54.000Z",
            username: 'user005',
            status: 'shown',
            environment: 'sunset',
            updated_at: "2020-03-19T10:13:54.000Z",
            total_likes: '7',
            total_comments: '9',
            content: 'testing_content',
            liked: true,
            commented: 'testing',
            galleryPhotoCount: 2,
          }]));

          
          expect(state.photoIds).toHaveLength(3);
          expect(state.photoById[66]).toEqual({
            id: 66,
            image: '66.jpg',
            title: '66_title',
            description: '66_description',
            location: 'Ha Pak Nai',
            district: 'Yuen Long',
            latitude: 22.4268,
            longitude: 113.939,
            created_at: "2020-03-19T10:13:54.000Z",
            username: 'user005',
            status: 'shown',
            environment: 'sunset',
            updated_at: "2020-03-19T10:13:54.000Z",
            total_likes: '5',
            total_comments: '7',
            content: 'testing_content',
            liked: true,
            commented: 'testing',
            galleryPhotoCount: 2,
          });
          expect(state).toHaveProperty("commentCount");
          expect(state).toHaveProperty("commentsById");
          expect(state).toHaveProperty("districtPhotoCount");
          expect(state).toHaveProperty("likeCount");
    });


    it("shoule load photos' comments correctly", () => {
        const state = districtPhotoReducer(undefined, loadPhotosComments(
            [{
                id: '9',
                content: 'good',
                created_at: "2020-05-22T10:13:54.000Z",
                updated_at: "2020-05-22T10:13:54.000Z",
                username: "Testing User A",
                user_id: 12,
                photo_id: 50
            },
            {
                id: '20',
                content: 'great',
                created_at: "2020-05-22T10:13:54.000Z",
                updated_at: "2020-05-22T10:13:54.000Z",
                username: "Testing User B",
                user_id: 12,
                photo_id: 50
            },
            ],
            8
        ))

        expect(state.commentCount).toEqual(0);
        expect(state.districtPhotoCount).toEqual(0);
        expect(state.likeCount).toEqual(0);
        expect(state.commentsById[8]).toEqual(
            expect.arrayContaining(
                [{
                    id: '20',
                    content: 'great',
                    created_at: "2020-05-22T10:13:54.000Z",
                    updated_at: "2020-05-22T10:13:54.000Z",
                    username: "Testing User B",
                    user_id: 12,
                    photo_id: 50
                }]
            ))
    });

    it('should add comment count correctly', () => {
        const state = districtPhotoReducer({
            "commentCount": 0,
                "commentsById":
            {
                "8":
                [{
                    "content": "good",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "9",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User A"
                },
                {
                    "content": "great",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "20",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User B"
                }]
            },
            "districtPhotoCount": 0,
                "likeCount": 0,
                    "photoById": { },
            "photoIds": []
        }, addComment(8));

        expect(state.commentCount).toEqual(1);
    });

    it('should add like count correctly', () => {
        const state = districtPhotoReducer({
            "commentCount": 0,
                "commentsById":
            {
                "8":
                [{
                    "content": "good",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "9",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User A"
                },
                {
                    "content": "great",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "20",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User B"
                }]
            },
            "districtPhotoCount": 0,
                "likeCount": 0,
                    "photoById": { },
            "photoIds": []
        }, likePhotosDistricts(8));

        expect(state.likeCount).toEqual(1);
    });

    it('should unlike photo correctly', () => {
        const state = districtPhotoReducer({
            "commentCount": 0,
                "commentsById":
            {
                "8":
                [{
                    "content": "good",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "9",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User A"
                },
                {
                    "content": "great",
                    "created_at": "2020-05-22T10:13:54.000Z",
                    "id": "20",
                    "photo_id": 50,
                    "updated_at": "2020-05-22T10:13:54.000Z",
                    "user_id": 12,
                    "username": "Testing User B"
                }]
            },
            "districtPhotoCount": 0,
                "likeCount": 5,
                    "photoById": { },
            "photoIds": []
        }, unLikePhotosDistricts(8));

        expect(state.likeCount).toEqual(4);
    })

})
