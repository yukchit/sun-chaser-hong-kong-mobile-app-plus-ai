import IPhoto, {IComment} from '../photos/state';

export interface DistrictPhotoState {
  photoIds: number[];
  photoById: {
    [id: string]: IPhoto;
  };
  commentsById: {
    [id: string]: IComment[];
  };
  commentCount: number;
  likeCount: number;
  districtPhotoCount: number;
}

export const initialState: DistrictPhotoState = {
  photoIds: [],
  photoById: {},
  commentsById: {},
  commentCount: 0,
  likeCount: 0,
  districtPhotoCount: 0,
};
