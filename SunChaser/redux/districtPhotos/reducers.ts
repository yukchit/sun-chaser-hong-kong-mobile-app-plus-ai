import {
  PhotosActions,
  LOAD_PHOTOS_COMMENTS,
  ADD_COMMENTS,
  DISTRICTS_LIKE_PHOTOS,
  DISTRICTS_UNLIKE_PHOTOS,
  LOAD_DISTRICT_PHOTOS,
} from '../photos/actions';
import {initialState, DistrictPhotoState} from './state';
import IPhoto, {IComment} from '../photos/state';

export function districtPhotoReducer(
  oldState: DistrictPhotoState = initialState,
  action: PhotosActions,
): DistrictPhotoState {
  switch (action.type) {
    case LOAD_DISTRICT_PHOTOS: {
      const newPhotoById: {
        [id: string]: IPhoto;
      } = {...oldState.photoById};
      for (const photo of action.photos) {
        //@ts-ignore
        photo.liked = photo['liked'] ? true : false;
        newPhotoById[photo.id] = photo;
      }
      return {
        ...oldState,
        photoIds: action.photos.map((photo) => photo.id),
        photoById: newPhotoById,
      };
    }

    case LOAD_PHOTOS_COMMENTS: {
      const newCommentsById: {
        [id: string]: IComment[];
      } = {...oldState.commentsById};
      newCommentsById[action.photoId] = action.comments;
      return {
        ...oldState,
        commentsById: newCommentsById,
      };
    }

    case ADD_COMMENTS:
      return {
        ...oldState,
        commentCount: oldState.commentCount + 1,
      };

    case DISTRICTS_LIKE_PHOTOS: {
      return {
        ...oldState,
        likeCount: oldState.likeCount + 1,
      };
    }

    case DISTRICTS_UNLIKE_PHOTOS: {
      return {
        ...oldState,
        likeCount: oldState.likeCount - 1,
      };
    }
    default:
      return oldState;
  }
}
