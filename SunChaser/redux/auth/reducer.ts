import {User, AuthActions} from './actions';

export interface AuthState {
  isAuthenticated: boolean | null;
  token: string | null;
  user: User | null;
}

const initialState: AuthState = {
  isAuthenticated: null,
  token: null,
  user: null,
};

export function authReducer(
  state: AuthState = initialState,
  action: AuthActions,
): AuthState {
  switch (action.type) {
    case '@@auth/LOGIN_SUCCESS':
      return {
        isAuthenticated: true,
        token: action.token,
        user: action.user,
      };
    case '@@auth/LOGIN_FAILED':
    case '@@auth/LOGOUT':
      return {
        isAuthenticated: false,
        token: null,
        user: null,
      };
    default:
      return state;
  }
}
