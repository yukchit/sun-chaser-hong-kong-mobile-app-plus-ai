import {authReducer} from "./reducer";

describe('auth reducer', () => {
    it('intial state', () => {
        const state = authReducer(undefined, {type:'@@INIT'} as any);

        expect(state).toEqual({
            isAuthenticated: null,
            token: null,
            user: null,
        })
    });

    it('login successfully', () => {
        const state = authReducer(undefined, {type: '@@auth/LOGIN_SUCCESS'} as any);

        expect(state).toHaveProperty('isAuthenticated', true);
        expect(state).toHaveProperty('token');
        expect(state).toHaveProperty('user');
    });

    it('login failed', () => {
        const state = authReducer(undefined, {type: '@@auth/LOGIN_FAILED'} as any);

        expect(state).toEqual({
            isAuthenticated: false,
            token: null,
            user: null,
        })
    });

    it('logout', () => {
        const state = authReducer({
            isAuthenticated: true,
            token: "testingToken",
            user: {
                id: 1,
                email: "testing@email.com",
                role: "user"
              },
        }, {type:'@@auth/LOGOUT'} as any);

        expect(state).toEqual({
            isAuthenticated: false,
            token: null,
            user: null,
        })
    });
})