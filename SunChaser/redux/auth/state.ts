import {User, AuthActions} from './actions';


export interface AuthState {
    isAuthenticated: boolean | null;
    token: string | null;
    user: User | null;
  }
  
  export const initialState: AuthState = {
    isAuthenticated: false,
    token: null,
    user: null,
  };