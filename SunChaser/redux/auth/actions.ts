import {ThunkDispatch, RootState} from '../../store';
import AsyncStorage from '@react-native-community/async-storage';
import config from '../../config';

export interface User {
  id: number;
  email: string;
  role: string;
}

export function loginSuccess(token: string, user: User) {
  return {
    type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
    token,
    user,
  };
}

export function loginFailed(error: string) {
  return {
    type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
    error,
  };
}

export function logoutSuccess() {
  return {
    type: '@@auth/LOGOUT' as '@@auth/LOGOUT',
  };
}

export function clearError() {
  return {
    type: '@@auth/CLEAR_ERROR' as '@@auth/CLEAR_ERROR',
  };
}

export type AuthActions = ReturnType<
  | typeof loginSuccess
  | typeof loginFailed
  | typeof logoutSuccess
  | typeof clearError
>;

export function logout() {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    await AsyncStorage.removeItem('token');
    dispatch(logoutSuccess());
  };
}

export function login(email: string, password: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${config.BACKEND_URL}/loginRN`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
        }),
      });

      const json = await res.json();
      console.log('login JSON', json);
      if (res.status !== 200) {
        return dispatch(loginFailed(json.error));
      }
      if (!json.token) {
        return dispatch(loginFailed('Network Error'));
      }
      await AsyncStorage.setItem('token', json.token);
      dispatch(loginSuccess(json.token, json.user));
    } catch (e) {
      console.error(e);
      dispatch(loginFailed('Unknown Error'));
    }
  };
}

export function restoreLogin() {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log('TOKEN', token);
      if (token == null) {
        dispatch(logout());
        return;
      }
      const res = await fetch(`${config.BACKEND_URL}/userRN`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const json = await res.json();
      if (json.id) {
        dispatch(loginSuccess(token, json));
      } else {
        dispatch(logout());
      }
    } catch (e) {
      console.error(e);
      dispatch(logout());
    }
  };
}

export function loginGoogle(accessToken: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${config.BACKEND_URL}/loginGoogleRN`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          accessToken,
        }),
      });
      const json = await res.json();
      console.log('login google JSON', json);

      if (res.status != 200) {
        return dispatch(loginFailed(json.error));
      }
      if (!json.token) {
        return dispatch(loginFailed('Network error'));
      }
      await AsyncStorage.setItem('token', json.token);
      dispatch(loginSuccess(json.token, json.user));
    } catch (e) {
      console.error(e);
      dispatch(loginFailed('Unknown error'));
    }
  };
}
