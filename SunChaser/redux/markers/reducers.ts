import {Marker, MarkersActions} from './actions';

export interface MarkersState {
  markerIds: number[];
  markerById: {
    [id: string]: Marker;
  };
}

const initialState: MarkersState = {
  markerIds: [],
  markerById: {},
};

export const markersReducer = (
  state: MarkersState = initialState,
  action: MarkersActions,
): MarkersState => {
  switch (action.type) {
    case '@@markers/LOAD_MARKERS':
      const newMarkerById: {
        [id: string]: Marker;
      } = {};
      for (const marker of action.markers) {
        newMarkerById[marker.id] = marker;
      }
      return {
        markerIds: action.markers.map((marker) => marker.id),
        markerById: newMarkerById,
      };
  }
  return state;
};
