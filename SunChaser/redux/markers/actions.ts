import config from '../../config';
import {ThunkDispatch} from '../../store';

export interface Marker {
  id: number;
  image: string;
  title: string;
  description: string;
  location: string;
  district: string;
  latitude: number;
  longitude: number;
  created_at: string;
  username: string;
  status: string;
  environment: string;
}

export function loadedMarkers(markers: Marker[]) {
  return {
    type: '@@markers/LOAD_MARKERS' as '@@markers/LOAD_MARKERS',
    markers,
  };
}

export type MarkersActions = ReturnType<typeof loadedMarkers>;

// thunk actions

export function fetchMarkers() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${config.BACKEND_URL}/googleMapMarkers`);
    const json = await res.json();
    dispatch(loadedMarkers(json));
  };
}
