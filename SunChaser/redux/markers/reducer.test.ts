import {markersReducer} from './reducers';
import { loadedMarkers } from './actions';

describe('google map markers reducer', () => {
    it('initial state', () => {
        const state = markersReducer(undefined, { type: '@@INIT' } as any);

        expect(state).toEqual(
            {
                markerIds: [],
                markerById: {}
            }
        )
    });

    it('should load map markers correctly', () => {
        const state = markersReducer({
                markerIds: [],
                markerById: {},
            }, loadedMarkers([{
                id: 1,
                image: '1.jpg',
                title: 'title_1',
                description: 'description_1',
                location: 'location_1',
                district: 'Tuen Mun',
                latitude: 22.3698,
                longitude: 113.9919,
                created_at: "2020-04-19T10:13:54.000Z",
                username: 'user_1',
                status: 'shown',
                environment: 'sunset',
              },{
                id: 2,
                image: '2.jpg',
                title: 'title_2',
                description: 'description_2',
                location: 'location_2',
                district: 'North',
                latitude: 22.5240,
                longitude: 114.2179,
                created_at: "2020-03-19T10:13:54.000Z",
                username: 'user_2',
                status: 'shown',
                environment: 'sunrise',
              },{
                id: 3,
                image: '2.jpg',
                title: 'title_3',
                description: 'description_3',
                location: 'location_3',
                district: 'Sai Kung',
                latitude: 22.3096,
                longitude: 114.3035,
                created_at: "2020-02-19T10:13:54.000Z",
                username: 'user_3',
                status: 'shown',
                environment: 'sunrise',
              }]));

        expect(state.markerIds).toHaveLength(3);
        expect(state.markerById[2]).toEqual({
            id: 2,
            image: '2.jpg',
            title: 'title_2',
            description: 'description_2',
            location: 'location_2',
            district: 'North',
            latitude: 22.5240,
            longitude: 114.2179,
            created_at: "2020-03-19T10:13:54.000Z",
            username: 'user_2',
            status: 'shown',
            environment: 'sunrise',
          })
    });

})