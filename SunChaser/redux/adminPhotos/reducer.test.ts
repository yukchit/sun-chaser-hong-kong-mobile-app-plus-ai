import {pendingAndDisapprovedPhotosReducer} from './reducers';
import { loadedPendingAndDisapprovedPhotos } from './actions';

describe('adminPhotos', () => {
    it('intial state', () => {
        const state = pendingAndDisapprovedPhotosReducer(undefined, {type: '@@INIT'} as any);

        expect(state).toEqual({
            pendingAndDisapprovedPhotoIds: [],
            pendingAndDisapprovedPhotoById: {},
        })
    });

    it('load pending and disapproved photos successfully', () => {
        const state = pendingAndDisapprovedPhotosReducer({
            pendingAndDisapprovedPhotoIds: [],
            pendingAndDisapprovedPhotoById: {},
        }, loadedPendingAndDisapprovedPhotos([
            {
                id: 50,
                image: '50.jpg',
                title: 'image50',
                description: '50description',
                location: 'Mei Foo',
                district: 'Shum Shui Po',
                latitude: 22.3365,
                longitude: 114.141,
                created_at: "2020-06-22T09:59:54.000Z",
                updated_at: "2020-06-22T09:59:54.000Z",
                username: "user01",
                status: "pending",
                environment: "sunset"
            },
            {
                id: 55,
                image: '55.jpg',
                title: 'image55',
                description: '55description',
                location: 'Belvedere Garden',
                district: 'Tsuen Wan',
                latitude: 22.3683,
                longitude: 114.098,
                created_at: "2020-07-22T09:59:54.000Z",
                updated_at: "2020-07-22T09:59:54.000Z",
                username: "user02",
                status: "pending",
                environment: "sunrise"
            }]));

        expect(state).toHaveProperty('pendingAndDisapprovedPhotoIds');
        expect(state).toHaveProperty('pendingAndDisapprovedPhotoById');
        expect(state.pendingAndDisapprovedPhotoIds).toHaveLength(2);
    })
})