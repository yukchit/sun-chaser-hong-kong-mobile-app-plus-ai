import {loadedPendingAndDisapprovedPhotos} from './actions';
import config from '../../config';
import {ThunkDispatch, RootState} from '../../store';

export function fetchPendingAndDisapprovedPhotos() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(
      `${config.BACKEND_URL}/photos/pendingAndDisapprovedPhotos`,
    );
    const json = await res.json();
    dispatch(loadedPendingAndDisapprovedPhotos(json));
  };
}
