import {PendingAndDisapprovedPhoto} from './actions';

export interface PendingAndDisapprovedPhotosState {
  pendingAndDisapprovedPhotoIds: number[];
  pendingAndDisapprovedPhotoById: {
    [id: string]: PendingAndDisapprovedPhoto;
  };
}

export const initialState: PendingAndDisapprovedPhotosState = {
  pendingAndDisapprovedPhotoIds: [],
  pendingAndDisapprovedPhotoById: {},
};
