import config from '../../config';
import {ThunkDispatch} from '../../store';

export interface PendingAndDisapprovedPhoto {
  id: number;
  image: string;
  title: string;
  description: string;
  location: string;
  district: string;
  latitude: number;
  longitude: number;
  created_at: string;
  updated_at: string;
  username: string;
  status: string;
  environment: string;
}

export function loadedPendingAndDisapprovedPhotos(
  pendingAndDisapprovedPhotos: PendingAndDisapprovedPhoto[],
) {
  return {
    type: '@@pendingAndDisapprovedPhotos/LOAD_PENDINGANDDISAPPROVEDPHOTOS' as '@@pendingAndDisapprovedPhotos/LOAD_PENDINGANDDISAPPROVEDPHOTOS',
    pendingAndDisapprovedPhotos,
  };
}

export type PendingAndDisapprovedPhotosActions = ReturnType<
  typeof loadedPendingAndDisapprovedPhotos
>;
