import {
  PendingAndDisapprovedPhoto,
  PendingAndDisapprovedPhotosActions,
} from './actions';
import {PendingAndDisapprovedPhotosState, initialState} from './state';

export const pendingAndDisapprovedPhotosReducer = (
  state: PendingAndDisapprovedPhotosState = initialState,
  action: PendingAndDisapprovedPhotosActions,
): PendingAndDisapprovedPhotosState => {
  switch (action.type) {
    case '@@pendingAndDisapprovedPhotos/LOAD_PENDINGANDDISAPPROVEDPHOTOS':
      const newPendingAndDisapprovedPhotoById: {
        [id: string]: PendingAndDisapprovedPhoto;
      } = {};
      for (const pendingAndDisapprovedPhoto of action.pendingAndDisapprovedPhotos) {
        newPendingAndDisapprovedPhotoById[
          pendingAndDisapprovedPhoto.id
        ] = pendingAndDisapprovedPhoto;
      }
      return {
        pendingAndDisapprovedPhotoIds: action.pendingAndDisapprovedPhotos.map(
          (pendingAndDisapprovedPhoto) => pendingAndDisapprovedPhoto.id,
        ),
        pendingAndDisapprovedPhotoById: newPendingAndDisapprovedPhotoById,
      };
  }
  return state;
};
